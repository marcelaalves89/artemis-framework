import numpy as np
import math

from custos.radio_transporte.tco_transporte import TcoTransporte
from entidade.central_office import CentralOffice
from rede_de_transporte.rede_transporte import RedeTransporte
from cenario_geografico.grafo import Grafo


class CenarioRedeTransporte:

    def __init__(self, tempo_analise):
        self.tempo_analise = tempo_analise
        self.tco_transporte = dict()
        self.co = CentralOffice()
        self.capex_co = dict(infraestrutura=np.zeros(self.tempo_analise),
                             equipamentos=np.zeros(self.tempo_analise),
                             instalacao=np.zeros(self.tempo_analise))
        self.opex_co = dict(energia=np.zeros(self.tempo_analise),
                            manutencao=np.zeros(self.tempo_analise),
                            aluguel=np.zeros(self.tempo_analise),
                            falhas=np.zeros(self.tempo_analise))


    def carrega_rede_transporte(self, municipio):
        for key in municipio.cenarios:
            for agrupamento in municipio.cenarios.get(key).agrupamentos_territoriais:
                rede_transporte= RedeTransporte(agrupamento, self.tempo_analise)
                rede_transporte.calcula_dimensionamento_rede_transporte()

            self.tco_transporte[key] = TcoTransporte(municipio.cenarios[key], self.tempo_analise)
            self.tco_transporte[key].get_tco()

        if len(municipio.vizinhos) > 0 and len(municipio.estradas) > 0:
            distancia_sem_fio, distancia_com_fio = self.calcula_transporte_agrupamentos_municipio(municipio)

    def calcula_transporte_agrupamentos_municipio(self, municipio):
        g = Grafo()
        grafo_agrupamento_arvore_minima = g.gera_arvore_minima_conexao_com_fio(municipio.vizinhos,municipio, False, False)
        distancia_com_fio = 0
        distancia_sem_fio = 0
        for (u, v, peso_com_fio) in grafo_agrupamento_arvore_minima.edges(data=True):
            print(peso_com_fio)
            distancia_com_fio += peso_com_fio['weight']
        print('Com fio')
        print(distancia_com_fio)

        grafo_agrupamento_arvore_minima_distancia_simples = g.gera_arvore_minima_conexao_sem_fio( municipio.vizinhos, municipio, False, False)
        for (u, v, peso_sem_fio) in grafo_agrupamento_arvore_minima_distancia_simples.edges(data=True):
            print(peso_sem_fio)
            distancia_sem_fio +=peso_sem_fio['weight']
        print('Sem fio')
        print(distancia_sem_fio)

        return distancia_sem_fio, distancia_com_fio


