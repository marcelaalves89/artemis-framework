import numpy as np
from util.util import Util
from cenario_geografico.grafo import Grafo


class RedeTransporte:

    def __init__(self, agrupamento, tempo_analise):
        self.agrupamento = agrupamento
        self.tempo_analise = tempo_analise
        self.pontos_agrupamento = self.agrupa_pontos_ruas_agrupamento()
        self.lista_geral_bs_agrupamento = self.bs_agrupamento()
        # Equipamentos Fibra (PON)
        self.agrupamento.qtd_fibra_instalada_macro_only = np.zeros(self.tempo_analise)
        self.agrupamento.qtd_modem_pon_macro_only = np.zeros(self.tempo_analise)
        self.agrupamento.qtd_fibra_instalada_hetnet = np.zeros(self.tempo_analise)
        self.agrupamento.qtd_modem_pon_hetnet = np.zeros(self.tempo_analise)

        # Equipamentos MicroWave (MW)
        self.agrupamento.qtd_antena_mw_macro = np.zeros(self.tempo_analise)
        self.agrupamento.qtd_antena_mw_hetnet = np.zeros(self.tempo_analise)


    def calcula_dimensionamento_rede_transporte(self):
        #implementação antiga
        # self.agrupamento.qtd_fibra_instalada_macro_only, self.agrupamento.qtd_modem_pon_macro_only = \
        #     self._calcula_dimensionamento_rede_transporte_fibra(self.agrupamento.lista_bs['implantacao_macro'], 'Macro')
        #
        # self.agrupamento.qtd_fibra_instalada_hetnet, self.agrupamento.qtd_modem_pon_hetnet = \
        #     self._calcula_dimensionamento_rede_transporte_fibra(self.agrupamento.lista_bs['implantacao_hetnet'], 'Hetnet')
        #
        # self.agrupamento.qtd_antena_mw_pt_pt_macro_only, self.agrupamento.qtd_sw_carrier_mw_macro_only = \
        #     self._calcula_dimensionamento_rede_transporte_microondas(self.agrupamento.lista_bs['implantacao_macro'])
        #
        # self.agrupamento.qtd_antena_mw_pt_pt_hetnet, self.agrupamento.qtd_sw_carrier_mw_hetnet = \
        #     self._calcula_dimensionamento_rede_transporte_microondas(self.agrupamento.lista_bs['implantacao_hetnet'])

        #Código Marcela
        # print('Estratégia Macro:')
        # self.qtd_fibra_instalada_macro_only, self.qtd_modem_pon_macro_only = \
        #    self._calcula_dimensionamento_rede_transporte_fibra(self.lista_bs['implantacao_macro'])

        # print('Estratégia Hetnet:')
        # self.qtd_fibra_instalada_hetnet, self.qtd_modem_pon_hetnet = \
        #     self._calcula_dimensionamento_rede_transporte_fibra(self.lista_bs['implantacao_hetnet'])

        print('Estratégia Macro:')
        self.agrupamento.qtd_antena_mw_macro, self.agrupamento.qtd_sw_carrier_mw_macro_only = \
            self._calcula_dimensionamento_rede_transporte_microondas(self.agrupamento.lista_bs['implantacao_macro'])

        print('Estratégia Hetnet:')
        self.agrupamento.qtd_antena_mw_hetnet, self.agrupamento.qtd_sw_carrier_mw_hetnet = \
            self._calcula_dimensionamento_rede_transporte_microondas(self.agrupamento.lista_bs['implantacao_hetnet'])

    def _calcula_dimensionamento_rede_transporte_fibra(self, lista_bs, cenario):
        print('Implantação Fibra Aglomerado {}:'.format(self.agrupamento.id_agrupamento_territorial))
        total_bs = np.zeros(self.tempo_analise)
        quantidadade_fibra_instalada = np.zeros(self.tempo_analise)
        quantidade_modem_pon = np.zeros(self.tempo_analise)

        # Calcula a quantidade acumulada de BSs por ano
        for bs in lista_bs:
            for ano in range(bs.ano, self.tempo_analise):
                total_bs[ano] += 1

        print('Total Acumulado de BSs por Ano: ')
        print(total_bs)

        # Calcula as implantaçãoes de de fibra (km) e modems PON por ano
        max_numero_bs = -99.0
        for ano, total_ano in enumerate(total_bs):
            if total_ano > max_numero_bs:
                if total_ano == 1:
                    quantidadade_fibra_instalada[ano] = 0
                    quantidade_modem_pon[ano] = 1
                    max_numero_bs = total_ano
                else:
                    max_numero_bs = total_ano
                    hub = Util().busca_bs_hub(
                        lista_bs)  # nesse caso é a BS principal, agregadora de tráfego
                    quantidade_modem_pon[ano] = total_ano - np.sum(quantidade_modem_pon[:ano])
                    bs_nao_hub = Util().busca_bs_nao_hub(lista_bs, ano)
                    for bs in bs_nao_hub:
                        if (hub is None) == False and len(self.pontos_agrupamento)>0 :
                            quantidadade_fibra_instalada[ano] += Util().get_distancia_manhattan(bs,
                                                                                    hub, self.pontos_agrupamento, Grafo() )  # a distancia esta em metros
        print('Quantidade de Fibra Instalada por Ano Cenário '+cenario)
        print(quantidadade_fibra_instalada)

        print('Quantidade de Modem PON por Ano Cenário '+cenario)
        print(quantidade_modem_pon)
        print()

        return quantidadade_fibra_instalada, quantidade_modem_pon

    def agrupa_pontos_ruas_agrupamento(self):
        pontos = []
        for r in self.agrupamento.ruas:
            for v in r.vizinhos_obj:
                pontos.append(v)
        return pontos

    def bs_agrupamento(self):
        lista_geral_bs = []
        for bs_m in self.agrupamento.lista_bs['implantacao_macro']:
            lista_geral_bs.append(bs_m)
        for bs_h in self.agrupamento.lista_bs['implantacao_hetnet']:
            lista_geral_bs.append(bs_h)
        return lista_geral_bs


    def _calcula_dimensionamento_rede_transporte_microondas(self, lista_bs):
        print('Implantação Microwave Aglomerado {}:'.format(self.agrupamento.id_agrupamento_territorial))
        total_bs = np.zeros(self.tempo_analise)
        quantidade_antena_mw_pt_pt = np.zeros(self.tempo_analise)
        quantidade_sw_carrier_mw = np.zeros(self.tempo_analise)

        # Quantidade de portas por Switch
        quantidade_portas_sw_carrier = 12

        # Calcula a quantidade acumulada de BSs por ano
        for bs in lista_bs:
            for ano in range(bs.ano, self.tempo_analise):
                total_bs[ano] += 1

        print('Total Acumulado de BSs por Ano: ')
        print(total_bs)

        # Calcula as implantaçãoes de Antenas microondas
        max_numero_bs = -99.0
        for ano, total_ano in enumerate(total_bs):
            if total_ano > max_numero_bs:
                if total_ano == 1:
                    quantidade_antena_mw_pt_pt[ano] = 0
                    max_numero_bs = total_ano
                else:
                    max_numero_bs = total_ano
                    # Nesse caso, dentro do aglomerado chama-se a BS que não é Hub de ponto a ponto;
                    quantidade_antena_mw_pt_pt[ano] = total_ano - np.sum(quantidade_antena_mw_pt_pt[:ano]) - 1
            if total_ano > 1:
                if np.sum(quantidade_sw_carrier_mw[:ano]) * quantidade_portas_sw_carrier < total_ano:
                    quantidade_sw_carrier_mw[ano] = np.ceil(total_ano / quantidade_portas_sw_carrier) - \
                                                np.sum(quantidade_sw_carrier_mw[:ano])

        # Multiplico por 02 (dois) a quantidade de antennas pt-pt, pois é um par, para cada BS
        quantidade_antena_mw_pt_pt = quantidade_antena_mw_pt_pt * 2

        print('Total de Antenas MW Pt-Pt Implantadas por Ano:')
        print(quantidade_antena_mw_pt_pt)

        print('Total de SW Carrier Implantados por Ano:')
        print(quantidade_sw_carrier_mw)
        print()

        return quantidade_antena_mw_pt_pt, quantidade_sw_carrier_mw


