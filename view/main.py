from cenario_geografico.cenario_geografico import CenarioGeografico
from cenario_geografico.mapa import Mapa
from cenario_geografico.grafo import Grafo
from central_office.cenario_central_office import CenarioCentralOffice
from conf.configuracao import Configuracao
from conf.parametro import Parametro
from custos.npv.cenario_npv import CenarioNPV
from previsao_de_trafego.cenario_demanda_trafego import CenarioDemandaTrafego
from rede_de_acesso.cenario_rede_acesso import CenarioRedeAcesso
from rede_de_transporte.cenario_rede_transporte import CenarioRedeTransporte
from sistema_fotovoltaico.cenario_fotovoltaico import CenarioFotovoltaico
from util.graficos import Grafico


class Main:
    if __name__ == '__main__':

        mapa = Mapa()
        grafo = Grafo()
        configuracao = Configuracao()
        configuracao.set(Parametro.TEMPO_ANALISE, 10)
        cenario_geografico = CenarioGeografico(mapa, grafo)
        cenario_rede_acesso = CenarioRedeAcesso(configuracao.tempo_analise)

        rede_transporte = CenarioRedeTransporte(configuracao.tempo_analise)
        sf = CenarioFotovoltaico(configuracao.tempo_analise)

        municipios = cenario_geografico.gera_municipio()

        # carrega as aplicações que serão usadas pelos agrupamentos do municipio
        cenario_demanda_trafego = CenarioDemandaTrafego(configuracao.tempo_analise)
        aplicacoes = cenario_demanda_trafego.carrega_aplicacoes()

        for municipio in municipios:
            cenario_geografico.popula_municipio(municipio)
            cenario_demanda_trafego.carrega_demanda_trafego_agrupamento(municipio, aplicacoes)
            cenario_rede_acesso.carrega_rede_acesso(municipio)
            rede_transporte.carrega_rede_transporte(municipio)
            CenarioCentralOffice(configuracao.tempo_analise, municipio).carrega_central_office()
            sf.carrega_sistema_fotovoltaico(municipio)
            CenarioNPV(configuracao.tempo_analise, municipio).gera_npv()
            Grafico(configuracao.tempo_analise, municipio).get_graficos()

