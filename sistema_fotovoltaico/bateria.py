from enum import Enum


class Bateria(Enum):
    BATERIA_1 = (1, 2640.0, 0.6, 18, 2703.0, 0.002543636364, 4)

    def __init__(self, id, capacidade_total_bateria, porcentagem_uso_bateria, tempo_sem_geracao_energia,
                 potencia_equipamento_total, preco, vida_util):
        self.id = id
        self.capacidade_total_bateria = capacidade_total_bateria
        self.porcentagem_uso_bateria = porcentagem_uso_bateria
        self.tempo_sem_geracao_energia = tempo_sem_geracao_energia
        self.potencia_equipamento_total = potencia_equipamento_total
        self.preco = preco
        self.vida_util = vida_util