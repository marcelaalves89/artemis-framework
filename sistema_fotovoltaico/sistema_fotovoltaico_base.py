import math
from sistema_fotovoltaico.medidor import Medidor
from sistema_fotovoltaico.painel import Painel
from sistema_fotovoltaico.radiacao import Radiacao


class SistemaFotovoltaicoBase:

    def __init__(self, tempo_analise, agrupamento):
        self.tempo_analise = tempo_analise
        self.agrupamento = agrupamento

    #equação 16
    def energia_minima_gerada(self, ano, consumo):
        return consumo - (self.quantidade_medidores() * Medidor.MED_1.consumo_minimo_diario)

    def quantidade_medidores(self):
        return 1


    # equação 20
    def energia_gerada_painel(self):
        return Painel.PAINEL_320W.area * Painel.PAINEL_320W.eficiencia * Radiacao.valor #* Painel.PAINEL_320W.taxa_desempenho

    #Novas Formulações
    def energia_maxima_gerada_painel(self, inversor):
        quantitativo_paineis_por_inversor = self.gera_quantitativo_paineis_por_inversor(inversor)
        energia_gerada_painel = self.energia_gerada_painel()
        return self.gera_quantitativo_paineis_por_inversor(inversor) * self.energia_gerada_painel()
    #floor arredonda para baixo
    def gera_paineis(self, potencia_bs):
        return math.floor(potencia_bs/self.energia_gerada_painel())

    def gera_inversores(self, quantidade_paineis, inversor):
      return math.ceil(quantidade_paineis/self.gera_quantitativo_paineis_por_inversor(inversor))

    def energia_gerada_sistema_fotovoltaico_painel(self, qtd_paineis, conf_inversor):
        energia_gerada_por_painel = self.energia_gerada_painel()
        return qtd_paineis * self.energia_gerada_painel()

    def gera_quantitativo_paineis_por_inversor(self, inversor):
        potencia_saida_inversor = inversor.value[3]
        potencia_saida_painel_kW = Painel.PAINEL_320W.potencia_saida / 1000
        return math.ceil(potencia_saida_inversor / potencia_saida_painel_kW)

    #localização sistema fotovoltaico
    def busca_localizacao(self, tipo_cenario):
        ponto_localizacao = 0
        for bs in self.agrupamento.lista_bs[tipo_cenario]:
            if bs.hub_bs:
                ponto_localizacao = bs.id_rua
                break
        return ponto_localizacao




