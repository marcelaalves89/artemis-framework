import numpy as np
from custos.sistema_fotovoltaico.tco_fotovoltaico import TCOFotovoltaico
from entidade.antena import Antena
from entidade.equipamento_central_office import EquipamentoCentralOffice
from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico
from sistema_fotovoltaico.sistema_fotovoltaico_offgrid import SistemaFotovoltaicoOffGrid
from sistema_fotovoltaico.sistema_fotovoltaico_ongrid import SistemaFotovoltaicoOnGrid



class CenarioFotovoltaico:

    def __init__(self, tempo_analise):
        self.tempo_analise = tempo_analise

    def carrega_sistema_fotovoltaico(self, municipio):
        for key in municipio.cenarios:
            for agrupamento in municipio.cenarios.get(key).agrupamentos_territoriais:
                #if agrupamento.municipio != 1502301:
                    self.gera_consumo_cenario_radio(agrupamento)
                    self.gera_consumo_transporte(agrupamento)
            #falta converter para boleano
            #if agrupamento.possui_energia == 1:
                    self.gera_sistema(agrupamento, TipoSistemaFotovoltaico.ONGRID.name, False)
                    self.gera_sistema(agrupamento, TipoSistemaFotovoltaico.OFFGRID.name, False)
                    self.adiciona_tco_fotovoltaico_agrupamento(agrupamento)

            self.gera_consumo_central_office(municipio.cenarios[key])
            self.gera_sistema(municipio.cenarios[key], TipoSistemaFotovoltaico.ONGRID.name, True)
            self.adiciona_tco_fotovoltaico_municipio(municipio.cenarios[key])

    def gera_sistema(self, agrupamento, tipo_sistema, is_municipio):
        if tipo_sistema == TipoSistemaFotovoltaico.ONGRID.name:
            s = SistemaFotovoltaicoOnGrid(self.tempo_analise, agrupamento,is_municipio )
        else:
            s = SistemaFotovoltaicoOffGrid(self.tempo_analise, agrupamento)
        if is_municipio:
            s.gera_sistema_fotovoltaico(agrupamento.consumo_diario_co, '')
            self.gera_TCO_fotovoltaico(agrupamento, '', tipo_sistema)
            self.seleciona_configuracao_inversor(agrupamento, '', tipo_sistema)
        else:
            s.gera_sistema_fotovoltaico(agrupamento.consumo_diario['implantacao_macro'], 'implantacao_macro')
            s.gera_sistema_fotovoltaico(agrupamento.consumo_diario['implantacao_hetnet'], 'implantacao_hetnet')
            # calcula TCO
            self.gera_TCO_fotovoltaico(agrupamento, 'implantacao_macro', tipo_sistema)
            self.gera_TCO_fotovoltaico(agrupamento, 'implantacao_hetnet', tipo_sistema)

            self.seleciona_configuracao_inversor(agrupamento, 'implantacao_macro', tipo_sistema)
            self.seleciona_configuracao_inversor(agrupamento, 'implantacao_hetnet', tipo_sistema)


    def gera_TCO_fotovoltaico(self, agrupamento, tipo_cenario, tipo_sistema):
        for sfv in agrupamento.sistema_fotovoltaico:
            if sfv.tipo_cenario == tipo_cenario and sfv.tipo_sistema == tipo_sistema:
                tco_m = TCOFotovoltaico(sfv, self.tempo_analise)
                tco_m.calcula_TCO()

    #verifica qual configuração possui o menor TCO
    def sistema_menor_tco(self, agrupamento, tipo_cenario, tipo_sistema):
        tco_temp = 0
        configuracao_inversor_menor_tco = ''
        for sfv in agrupamento.sistema_fotovoltaico:
            if sfv.tipo_cenario == tipo_cenario and sfv.tipo_sistema == tipo_sistema:
                if  sfv.tco_total < tco_temp or tco_temp == 0:
                    configuracao_inversor_menor_tco = sfv.configuracao_inversor
                tco_temp = sfv.tco_total
        return configuracao_inversor_menor_tco

    #ativa a configuração de sistema fotovoltaico que possui o menor tco
    def seleciona_configuracao_inversor(self, agrupamento, tipo_cenario, tipo_sistema):
        configuracao_inversor_menor_tco = self.sistema_menor_tco(agrupamento, tipo_cenario, tipo_sistema)
        for sfv in agrupamento.sistema_fotovoltaico:
            if sfv.tipo_cenario == tipo_cenario and sfv.tipo_sistema == tipo_sistema:
                if sfv.configuracao_inversor == configuracao_inversor_menor_tco:
                    sfv.ativo = True


    #subtarefa 2
    def gera_consumo_cenario_radio(self, agrupamento):
        agrupamento.consumo_diario = dict(implantacao_macro=np.zeros(self.tempo_analise),
                                   implantacao_hetnet=np.zeros(self.tempo_analise))

        self.gera_consumo_total_bs(agrupamento, 'implantacao_macro')
        self.gera_consumo_total_bs(agrupamento, 'implantacao_hetnet')

    def gera_consumo_transporte(self, agrupamento):
        self.gera_consumo_total_transporte(agrupamento, 'implantacao_macro')
        self.gera_consumo_total_transporte(agrupamento, 'implantacao_hetnet')

    #subtarefa 2
    def gera_consumo_total_bs(self, agrupamento, tipo_cenario_rede):
     #   pt = 0
        for ano in range(self.tempo_analise):
            pt = 0
            for b in agrupamento.lista_bs[tipo_cenario_rede]:
                if b.ano <= ano:
                    pt += b.tipo_BS.potencia_transmissao
            agrupamento.consumo_diario[tipo_cenario_rede][ano] = pt

    def gera_consumo_total_transporte(self, agrupamento, tipo_cenario_rede):
       #4 antenas de microondas large foram implentadas no ano 0
        bs_large = np.zeros(self.tempo_analise)
        bs_large[0] = 4
        if tipo_cenario_rede == 'implantacao_macro':
            qtd_small = agrupamento.qtd_antena_mw_macro
            qtd_carrier = agrupamento.qtd_sw_carrier_mw_macro_only
        else:
            qtd_small = agrupamento.qtd_antena_mw_hetnet
            qtd_carrier = agrupamento.qtd_sw_carrier_mw_hetnet
        large_temp = 0
        small_temp = 0
        carrier_temp = 0
        for ano in range(self.tempo_analise):
            pt = 0
            pt += (bs_large[ano] + large_temp) * Antena.ANTENA_MW_LARGE.potencia
            if bs_large[ano] > 0:
                large_temp = bs_large[ano]
            print('Large')
            print((bs_large[ano] + small_temp)  * Antena.ANTENA_MW_LARGE.potencia)
            pt += qtd_small[ano] * Antena.ANTENA_MW_SMALL.potencia
            if qtd_small[ano] > 0:
                small_temp = qtd_small[ano]
            print('Small')
            print(qtd_small[ano] * Antena.ANTENA_MW_SMALL.potencia)
            pt += (qtd_carrier[ano] + carrier_temp) * Antena.SWITCH_CARRIER.potencia
            if qtd_carrier[ano] > 0:
                carrier_temp = qtd_carrier[ano]
            print('Carrier')
            print(qtd_carrier[ano] * Antena.SWITCH_CARRIER.potencia)
            total = pt + agrupamento.consumo_diario[tipo_cenario_rede][ano]
            agrupamento.consumo_diario[tipo_cenario_rede][ano] = total

    def gera_consumo_central_office(self, municipio):
        qt_fs_temp = 0.0
        qtd_as_temp = 0.0
        qtd_udc_temp = 0.0
        for ano in range(self.tempo_analise):
            pt = 0
            pt += (municipio.co.qtd_fs[ano] + qt_fs_temp) * EquipamentoCentralOffice.SWITCH_FISICO.potencia
            if municipio.co.qtd_fs[ano]>0:
                qt_fs_temp = municipio.co.qtd_fs[ano]
            print('SF')
            print(municipio.co.qtd_fs[ano] * EquipamentoCentralOffice.SWITCH_FISICO.potencia)
            pt += (municipio.co.qtd_as[ano] + qtd_as_temp) * EquipamentoCentralOffice.SWITCH_AGREGACAO.potencia
            if municipio.co.qtd_as[ano]>0:
                qtd_as_temp = municipio.co.qtd_as[ano]
            print('AG')
            print(municipio.co.qtd_as[ano] * EquipamentoCentralOffice.SWITCH_AGREGACAO.potencia)
            pt += (municipio.co.qtd_udc[ano] + qtd_udc_temp) * EquipamentoCentralOffice.MICRO_DATACENTER.potencia
            if municipio.co.qtd_udc[ano]>0:
                qtd_udc_temp = municipio.co.qtd_udc[ano]
            print('DT')
            print(municipio.co.qtd_udc[ano] * EquipamentoCentralOffice.MICRO_DATACENTER.potencia)
            municipio.consumo_diario_co[ano] = pt

    def adiciona_tco_fotovoltaico_agrupamento(self, agrupamento):
        agrupamento.custo_energia_minima_rede = dict(macro=list(), hetnet=list())
        agrupamento.custo_aluguel_fotovoltaico= dict(macro=list(), hetnet=list())

        for sfv in agrupamento.sistema_fotovoltaico:
            if sfv.ativo and sfv.tipo_sistema == TipoSistemaFotovoltaico.ONGRID.name:
                if sfv.tipo_cenario == 'implantacao_macro':
                    agrupamento.capex_macro['Fotovoltaico'] = sfv.capex
                    agrupamento.opex_macro['Fotovoltaico'] = sfv.opex
                    agrupamento.custo_energia_minima_rede['macro'] = list(sfv.custo_energia_minima_rede.values())
                    agrupamento.custo_aluguel_fotovoltaico['macro'] = list(sfv.custo_aluguel_fotovoltaico.values())
                else:
                    agrupamento.capex_hetnet['Fotovoltaico'] = sfv.capex
                    agrupamento.opex_hetnet['Fotovoltaico'] = sfv.opex
                    agrupamento.custo_energia_minima_rede['hetnet'] = list(sfv.custo_energia_minima_rede.values())
                    agrupamento.custo_aluguel_fotovoltaico['hetnet'] = list(sfv.custo_aluguel_fotovoltaico.values())


    def adiciona_tco_fotovoltaico_municipio(self, municipio):
        for sfv in municipio.sistema_fotovoltaico:
            if sfv.ativo and sfv.tipo_sistema == TipoSistemaFotovoltaico.ONGRID.name:
                municipio.capex_fotovoltaico_co = sfv.capex
                municipio.opex_fotovoltaico_co = sfv.opex
                municipio.custo_energia_minima_rede_co = list(sfv.custo_energia_minima_rede.values())
                municipio.custo_aluguel_fotovoltaico_co = list(sfv.custo_aluguel_fotovoltaico.values())


    def recupera_valor_acumulado(self, objeto):
        valor_acumulado =0
        for ano in range(self.tempo_analise):
            valor_acumulado += objeto.get(ano)
        return valor_acumulado


    def acumulado_capex_opex(self, sfv):
        capex_agrupamento = self.recupera_valor_acumulado(sfv.capex)
        opex_agrupamento = self.recupera_valor_acumulado(sfv.opex)
        return capex_agrupamento, opex_agrupamento
