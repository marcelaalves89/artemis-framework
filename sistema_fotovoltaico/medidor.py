from enum import Enum

class Medidor(Enum):

	MED_1 = (1, 0.0005078181818, 0.000001563230066, 0.000001157948197, 3.33333333, 234) #consumo_minimo_diario = 100kW/aomes / 30

	def __init__(self, id, preco_instalacao, preco_compra_kWh, preco_venda_kWh, consumo_minimo_diario, qtd_paineis_minimo_instalacao):
		self.id = id
		self.preco_instalacao = 0.0005078181818
		self.preco_compra_kWh = preco_compra_kWh
		self.preco_venda_kWh = preco_venda_kWh
		self.consumo_minimo_diario = consumo_minimo_diario
		self.qtd_paineis_minimo_instalacao = qtd_paineis_minimo_instalacao
