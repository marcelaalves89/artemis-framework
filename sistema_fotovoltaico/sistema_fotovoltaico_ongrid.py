import numpy as np
from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico
from sistema_fotovoltaico.inversor import Inversor
from entidade.sistema_fotovoltaico import SistemaFotovoltaico
from sistema_fotovoltaico.sistema_fotovoltaico_base import SistemaFotovoltaicoBase
from util.util import Util


class SistemaFotovoltaicoOnGrid(SistemaFotovoltaicoBase):

    def __init__(self, tempo_analise, agrupamento, is_municipio=False):
        super().__init__(tempo_analise, agrupamento)
        self.is_municipio = is_municipio

    def gera_sistema_fotovoltaico(self, consumo_diario, tipo_cenario):
        for inv in Inversor:
            if inv.tipo_sistema == TipoSistemaFotovoltaico.ONGRID.name:
                #verificar localização central office
                if self.is_municipio:
                    localizacao = 0
                else:
                    localizacao = self.busca_localizacao(tipo_cenario)

                sfv = SistemaFotovoltaico(inv, tipo_cenario, TipoSistemaFotovoltaico.ONGRID.name,
                                      localizacao)
                sfv.energia_minima = np.zeros(self.tempo_analise)
                self.calcula_sistema_fotovoltaico(sfv, consumo_diario)

    def calcula_sistema_fotovoltaico(self, sfv, consumo_diario):
        novos_inversores_temp = 0
        novos_paineis_temp =0
        novos_paineis = dict()
        novos_inversores = dict()
        for ano in range(self.tempo_analise):
            consumo_diario_kWh = Util().convert_kWh(consumo_diario[ano])
            sfv.energia_minima[ano] = self.energia_minima_gerada(ano, consumo_diario_kWh)
            sfv.qtd_paineis[ano] = self.gera_paineis(consumo_diario_kWh)
            sfv.qtd_inversores[ano] = self.gera_inversores(sfv.qtd_paineis[ano], sfv.configuracao_inversor)

            novos_paineis[ano] =  abs(sfv.qtd_paineis[ano] - novos_paineis_temp)
            novos_inversores[ano] = abs(sfv.qtd_inversores[ano] - novos_inversores_temp)

            sfv.energia_gerada[ano] = self.energia_gerada_sistema_fotovoltaico_painel(sfv.qtd_paineis[ano],sfv.configuracao_inversor ) #Para saber a energia excedente = energia_gerada - energia minima(esta em kWh)
            sfv.qtd_medidores [ano] = self.quantidade_medidores()

            novos_paineis_temp = sfv.qtd_paineis[ano]
            novos_inversores_temp = sfv.qtd_inversores[ano]
        self.agrupamento.sistema_fotovoltaico.append(sfv)




