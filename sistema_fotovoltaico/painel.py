from enum import Enum

class Painel(Enum):
	PAINEL_320W = (1, 415, 2.18, 25, 0.001074909091, 0.1668, 0.995, 0.0003381818182, 12, 7)


	def __init__(self, id, potencia_saida, area, tempo_garantia_painel, preco, eficiencia, taxa_desempenho, custo_instalacao_kit,
				tempo_garantia_kit_instalacao, tempo_exposicao_sol):
		self.id = id
		self.potencia_saida = potencia_saida
		self.area = area
		self.tempo_garantia_painel = tempo_garantia_painel
		self.preco = preco
		self.eficiencia = eficiencia
		self.taxa_desempenho = taxa_desempenho
		self.custo_instalacao_kit = custo_instalacao_kit
		self.tempo_garantia_kit_instalacao = tempo_garantia_kit_instalacao
		#rever posicao
		self.tempo_exposicao_sol = tempo_exposicao_sol

