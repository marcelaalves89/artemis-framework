import numpy as np
import math
from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico
from sistema_fotovoltaico.bateria import Bateria
from sistema_fotovoltaico.inversor import Inversor
from entidade.sistema_fotovoltaico import SistemaFotovoltaico
from sistema_fotovoltaico.sistema_fotovoltaico_base import SistemaFotovoltaicoBase
from util.util import Util


class SistemaFotovoltaicoOffGrid(SistemaFotovoltaicoBase):

    def __init__(self, tempo_analise, agrupamento):
        super().__init__(tempo_analise, agrupamento)

    def gera_sistema_fotovoltaico(self, potencia_equipamento_total, tipo_cenario):
        for inv in Inversor:
            if inv.tipo_sistema == TipoSistemaFotovoltaico.OFFGRID.name:
                sfv = SistemaFotovoltaico(inv, tipo_cenario, TipoSistemaFotovoltaico.OFFGRID.name,
                                      self.busca_localizacao(tipo_cenario))
                sfv.energia_minima = np.zeros(self.tempo_analise)
                self.calcula_sistema_fotovoltaico(sfv, potencia_equipamento_total)

    def calcula_sistema_fotovoltaico(self, sfv, potencia_equipamento_total):
        novos_inversores_temp = 0
        novos_paineis_temp =0
        novos_paineis = dict()
        novos_inversores = dict()
        sfv.qtd_medidores = np.zeros(self.tempo_analise)
        for ano in range(self.tempo_analise):
            potencia_equipamento_total_kWh = Util().convert_kWh(potencia_equipamento_total[ano])
            sfv.qtd_paineis[ano] = self.gera_paineis(potencia_equipamento_total_kWh)
            sfv.qtd_inversores[ano] = self.gera_inversores(sfv.qtd_paineis[ano], sfv.configuracao_inversor)

            novos_paineis[ano] = sfv.qtd_paineis[ano] - novos_paineis_temp
            novos_inversores[ano] = sfv.qtd_inversores[ano] - novos_inversores_temp

            sfv.energia_gerada[ano] = self.energia_gerada_sistema_fotovoltaico_painel(sfv.qtd_paineis[ano],sfv.configuracao_inversor )

            sfv.qtd_baterias [ano] = self.quantidade_baterias(potencia_equipamento_total[ano])

            novos_paineis_temp = sfv.qtd_paineis[ano]
            novos_inversores_temp = sfv.qtd_inversores[ano]
        self.agrupamento.sistema_fotovoltaico.append(sfv)


    def capacidade_util_bateria(self):
       return Bateria.BATERIA_1.capacidade_total_bateria * Bateria.BATERIA_1.porcentagem_uso_bateria

    def consumo_bateria(self, potencia_equipamento_total):
        return potencia_equipamento_total * Bateria.BATERIA_1.tempo_sem_geracao_energia


    def quantidade_baterias(self, potencia_equipamento_total):
            return math.ceil(self.consumo_bateria(potencia_equipamento_total)/self.capacidade_util_bateria())







