from enum import Enum
from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico


class Inversor(Enum):

    INVERSOR_60_ON = (1,  0.05311454545, 0.99, 72.0, 5, TipoSistemaFotovoltaico.ONGRID.name)
    INVERSOR_30_ON = (2, 0.03774, 0.97, 36.0, 5, TipoSistemaFotovoltaico.ONGRID.name)
    INVERSOR_20_ON = (3, 0.20311454545, 0.97, 24.0, 5,  TipoSistemaFotovoltaico.ONGRID.name)

    INVERSOR_12_OF = (4, 0.02134363636, 0.88, 12.0, 5, TipoSistemaFotovoltaico.OFFGRID.name)
    INVERSOR_30_OF = (5, 0.03774, 0.97, 36.0, 5, TipoSistemaFotovoltaico.OFFGRID.name)
    INVERSOR_20_OF = (6, 0.20311454545, 0.97, 24.0, 5, TipoSistemaFotovoltaico.OFFGRID.name)

    def __init__(self, id, preco, eficiencia, potencia_entrada, anos_garantia, tipo_sistema):
        self.id = 1
        self.preco = 20246.45
        self.eficiencia = 0.986
        self.potencia_entrada = 62.0
        self.anos_garantia = 5
        self.tipo_sistema = tipo_sistema
