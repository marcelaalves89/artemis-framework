from entidade.central_office import CentralOffice
from entidade.regiao import Regiao
import numpy as np


class Municipio(Regiao):

    def __init__(self, id, nome, area, tempo_viagem, tempo_medio_disponibilidade):
        self.id_municipio = id
        super().__init__(nome, area) #Chama o construtor da região.py
        self._agrupamentos_territoriais = []#Meu municipio tem uma lista de agrupamentos territoriais
        self.estradas = []
        self.tempo_viagem = tempo_viagem
        self.tempo_medio_disponibilidade = tempo_medio_disponibilidade
        self.npv = dict()
        self.npv_sf = dict()
        self.tipos_rede_radio = ['Macro', 'Hetnet']
        self.co = CentralOffice()
        self.consumo_diario_co = dict()
        self.sistema_fotovoltaico = []
        self.capex_fotovoltaico_co= dict()
        self.opex_fotovoltaico_co=  dict()
        self.custo_energia_minima_rede_co = list()
        self.custo_aluguel_fotovoltaico_co = list()

    @property
    def agrupamentos_territoriais(self):
        return self._agrupamentos_territoriais

    @agrupamentos_territoriais.setter
    def agrupamentos_territoriais(self, value):
        self._agrupamentos_territoriais = value

    @property
    def cod_estado(self):
        return self._cod_estado

    @cod_estado.setter
    def cod_estado(self, value):
        self._cod_estado = value

    @property
    def tipo_municipio(self):
        return self._tipo_municipio

    @tipo_municipio.setter
    def tipo_municipio(self, value):
        self._tipo_municipio = value

    def total_habitantes(self):
        numero_habitantes = 0
        for agrupamento in self._agrupamentos_territoriais:
            numero_habitantes = numero_habitantes + agrupamento.numero_habitantes
        self.numero_habitantes = numero_habitantes
        return numero_habitantes

    def __repr__(self):
        return str(self.__dict__)