
class TipoPonto:

    PONTO_RUA = 1
    PONTO_REDE = 2
    PONTO_REDE_CO = 3
    PONTO_REDE_HUB = 4
    PONTO_MACRO = 5
    PONTO_MICRO = 6
    PONTO_PICO = 7
    PONTO_FEMTO = 8


    def isRede(self, tipo):
        return (tipo == self.PONTO_REDE)

    def isRedeCO(self, tipo):
        return (tipo ==  self.PONTO_REDE_CO)

    def isRedeHub(self, tipo):
        return (tipo ==  self.PONTO_REDE_HUB)

    def isMacro(self, tipo):
        return (tipo ==  self.PONTO_MACRO)

    def isMicro(self, tipo):
        return (tipo ==  self.PONTO_MICRO)

    def isPico(self, tipo):
        return (tipo ==  self.PONTO_PICO)

    def isFemto(self, tipo):
        return (tipo ==  self.PONTO_FEMTO)