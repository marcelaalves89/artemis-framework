from enum import Enum

class TipoServicoAplicacao(Enum):

    E_HEALTH = 1
    E_LEARNING = 2
    E_GOV = 3
    E_FINANCE = 4
    IOT = 5

    def __getitem__(self, value):
        return self.names[value]