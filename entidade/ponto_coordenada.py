class PontoCoordenada:

    def __init__(self, id, nome, coordenadas, id_municipio):
        self.id = id
        self.nome = nome
        self.coordenadas = coordenadas
        self.id_municipio = id_municipio
        #self.vizinhos_obj =[]

    @property
    def tipo_ponto(self):
        return self._tipo_ponto

    @tipo_ponto.setter
    def tipo_ponto(self, value):
        self._tipo_ponto = value

    @property
    def vizinhos(self):
        return self._vizinhos

    @vizinhos.setter
    def vizinhos(self, value):
        self._vizinhos = value

    @property
    def coordenadas_geograficas_obj(self):
        return self._coordenadas_geograficas_obj

    @coordenadas_geograficas_obj.setter
    def coordenadas_geograficas_obj(self, value):
        self._coordenadas_geograficas_obj = value

    @property
    def coordenadas_obj_line(self):
        return self._coordenadas_obj_line

    @coordenadas_obj_line.setter
    def coordenadas_obj_line(self, value):
        self._coordenadas_obj_line = value

    def __repr__(self):
        return str(self.__dict__)
