class TipoPercurso:

    RODOVIA_VELOCIDADE_MAIOR_IGUAL_70_KM = 0
    RODOVIA_VELOCIDADE_MAIOR_IGUAL_40_KM = 1
    RODOVIA_VELOCIDADE_MENOR_40_KM = 2
    FLUVIAL_MARITIMO = 3
    FLORESTAL_MONTANHOSO_ACIDADENTADO = 4

    def peso(self, tipo):
        if(tipo == self.RODOVIA_VELOCIDADE_MAIOR_IGUAL_70_KM):
            return 1
        elif(tipo== self.RODOVIA_VELOCIDADE_MAIOR_IGUAL_40_KM):
            return 2
        elif (tipo == self.RODOVIA_VELOCIDADE_MENOR_40_KM):
            return 4
        elif (tipo == self.FLUVIAL_MARITIMO):
            return 8
        elif (tipo == self.FLORESTAL_MONTANHOSO_ACIDADENTADO):
            return 16

