
class Vizinho:

    def __init__(self, id, id_ponto_vizinho, id_ponto_origem):
        self.id = id
        self.id_ponto_vizinho = id_ponto_vizinho
        self.id_ponto_origem = id_ponto_origem

    @property
    def distancia(self):
        return self._distancia

    @distancia.setter
    def distancia(self, value):
        self._distancia = value

    @property
    def distancia_simples(self):
        return self._distancia_simples

    @distancia_simples.setter
    def distancia_simples(self, value):
        self._distancia_simples = value

    @property
    def coordenadas_obj_line(self):
        return self._coordenadas_obj_line

    @coordenadas_obj_line.setter
    def coordenadas_obj_line(self, value):
        self._coordenadas_obj_line = value

    @property
    def coordenadas_obj_line_dist_simples(self):
        return self._coordenadas_obj_line_dist_simples

    @coordenadas_obj_line_dist_simples.setter
    def coordenadas_obj_line_dist_simples(self, value):
        self._coordenadas_obj_line_dist_simples = value

    @property
    def id_agrupamento(self):
        return self._id_agrupamento

    @id_agrupamento.setter
    def id_agrupamento(self, value):
        self._id_agrupamento = value

    @property
    def id_municipio(self):
        return self._id_municipio

    @id_municipio.setter
    def id_municipio(self, value):
        self._id_municipio = value

    @property
    def tipo_infra(self):
        return self._tipo_infra

    @tipo_infra.setter
    def tipo_infra(self, value):
        self._tipo_infra= value

    @property
    def tipo_percurso(self):
        return self._tipo_percurso

    @tipo_percurso.setter
    def tipo_percurso(self, value):
        self._tipo_percurso= value

    def __repr__(self):
        return str(self.__dict__)