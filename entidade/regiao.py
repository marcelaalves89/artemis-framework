#Classe pai que agrupa os atributos comuns aos municípios, agrupamentos e setores censitários
from entidade.dados_socio_economico import DadosSocioEconomico


class Regiao:

    def __init__(self, nome, area):
       # self.id = id
        self.nome = nome
        self.area = area
        self.dados_socio_economico = DadosSocioEconomico()

    #Situação esta relacionada ao fato da região se ela é urbana ou rural
    @property
    def situacao(self):
        return self._situacao

    @situacao.setter
    def situacao(self, value):
        self._situacao = value

    @property
    def coordenadas_geograficas(self):
        return self._coordenadas_geograficas

    @coordenadas_geograficas.setter
    def coordenadas_geograficas(self, value):
        self._coordenadas_geograficas = value

    @property
    def centroide(self):
        return self._centroide

    @centroide.setter
    def centroide(self, value):
        self._centroide = value

    @property
    def total_domicilios(self):
        return self._total_domicilios

    @total_domicilios.setter
    def total_domicilios(self, value):
        self._total_domicilios = value

    @property
    def numero_habitantes(self):
        return self._numero_habitantes

    @numero_habitantes.setter
    def numero_habitantes(self, value):
        self._numero_habitantes = value

    @property
    def coordenadas_geograficas_obj(self):
        return self._coordenadas_geograficas_obj

    @coordenadas_geograficas_obj.setter
    def coordenadas_geograficas_obj(self, value):
        self._coordenadas_geograficas_obj = value

    @property
    def tipo_regiao(self):
        return self._tipo_regiao

    @tipo_regiao.setter
    def tipo_regiao(self, value):
        self._tipo_regiao = value


    def __repr__(self):
        return str(self.__dict__)