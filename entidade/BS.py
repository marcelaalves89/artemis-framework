from entidade.atualizacao_BS import AtualizacaoBS
from entidade.tipo_BS import TipoBS


class BS:

    def __init__(self, id, id_municipio, id_agrupamento, tipo_bs, id_operadora, tipo_ponto
                 ,id_rua, ano=0, hub_bs=False, existenncia_previa=True):
        self.id = id
        self.id_municipio =id_municipio
        self.id_agrupamento = id_agrupamento
        self.tipo_BS = tipo_bs
        self.id_operadora = id_operadora
        self.tipo_ponto = tipo_ponto
        self.id_rua = id_rua
        self.ano = ano
        self.hub_bs = hub_bs
        self.existencia_previa = existenncia_previa
        self.atualizacoes = list()

    def upgrade(self, ano):
        switcher = {
            TipoBS.MACRO_2G: TipoBS.MACRO_3G,
            TipoBS.MACRO_3G: TipoBS.MACRO_4G,
            TipoBS.MACRO_4G: TipoBS.MACRO_45G,
            TipoBS.MICRO_4G: TipoBS.MICRO_45G,
            TipoBS.FEMTO_4G: TipoBS.FEMTO_45G,
        }
        atualizacao = switcher.get(self.tipo_BS, False)
        if atualizacao is False:
            return False
        else:
            self.tipo_BS = atualizacao
            self.atualizacoes.append(AtualizacaoBS(atualizacao, ano))
            return True

    def upgrade_5G(self, ano):
        switcher = {
            TipoBS.MACRO_2G: TipoBS.MACRO_5G,
            TipoBS.MACRO_3G: TipoBS.MACRO_5G,
            TipoBS.MACRO_4G: TipoBS.MACRO_5G,
            TipoBS.MACRO_45G: TipoBS.MACRO_5G,
            TipoBS.MICRO_4G: TipoBS.MICRO_5G,
            TipoBS.MICRO_45G: TipoBS.MICRO_5G,
            TipoBS.FEMTO_4G: TipoBS.FEMTO_5G,
            TipoBS.FEMTO_45G: TipoBS.FEMTO_5G,
        }
        atualizacao = switcher.get(self.tipo_BS, False)
        if atualizacao is False:
            return False
        else:
            self.tipo_BS = atualizacao
            self.atualizacoes.append(AtualizacaoBS(atualizacao, ano))
            return True

    def ajuste_atualizacoesBS(self):
        lista_ajustada = list()
        ultimo_ano = -1
        for at in self.atualizacoes:
            if at.ano != ultimo_ano:
                ultimo_ano = at.ano
                lista_ajustada.append(at)
            else:
                lista_ajustada[-1] = at
        self.atualizacoes = lista_ajustada

    def __str__(self):
        return "id={}, tipo={}, ano={}, ponto={}, hub_bs={}, existencia_previa={}".format(self.id, self.tipo_BS,
                                                                                          self.ano, self.id_rua,
                                                                                          self.hub_bs,
                                                                                          self.existencia_previa)
