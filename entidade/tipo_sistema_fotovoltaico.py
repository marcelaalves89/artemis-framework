from enum import Enum

class TipoSistemaFotovoltaico(Enum):
    ONGRID = 1
    OFFGRID = 2
    HIBRIDO = 3
