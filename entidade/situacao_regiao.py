from entidade.tipo_regiao import TipoRegiao

class SituacaoRegiao:

    situacoes_urbanas = {1: 'Sede',
                         2: 'Vila',
                         3: 'Urbana Isolada',
                         9: 'Urbano'}

    situacoes_rurais = {4: 'Extensão Urbana',
                        5: 'Povoado',
                        6: 'Núcleo',
                        7: 'Outros',
                        8: 'Zona Rural'}

    def situacao(self, tipo_regiao, value):
        if(tipo_regiao == TipoRegiao.URBANO.value):
            return self.situacoes_urbanas.get(value)
        else:
            return self.situacoes_rurais.get(value)





