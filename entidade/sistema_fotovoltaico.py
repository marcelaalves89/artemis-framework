class SistemaFotovoltaico:

    def __init__(self, configuracao_inversor, tipo_cenario, tipo_sistema, localizacao, ativo=False):
        self.configuracao_inversor = configuracao_inversor
        self.qtd_inversores = dict()
        self.qtd_paineis = dict()
        self.qtd_medidores = dict()
        self.qtd_baterias = dict()
        self.tipo_cenario = tipo_cenario
        self.tipo_sistema = tipo_sistema
        self.energia_gerada = dict()
        self.energia_minima = dict()
        self.localizacao = localizacao
        self.opex = dict()
        self.capex = dict()
        self.custo_energia_minima_rede = dict()
        self.custo_aluguel_fotovoltaico = dict()
        self.tco_total = 0
        self.ativo = ativo

