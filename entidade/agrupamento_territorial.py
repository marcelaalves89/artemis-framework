from entidade.regiao import Regiao

class AgrupamentoTerritorial(Regiao):

    def __init__(self, id, nome, area):
        self.id_agrupamento_territorial = id
        super().__init__(nome, area)
        self._setores_censitarios = []
        self.ruas = []
        self.vizinhos_obj = []
        #rede acesso
        self.tempo_maturacao = 3.0
      #  self.lista_bs = dict(implantacao_macro=list(), implantacao_hetnet=list())
        self.capacidade_atendimento_rede_acesso = dict(implantacao_macro=list(), implantacao_hetnet=list())
        self.quantidade_bs = dict(implantacao_macro=list(), implantacao_hetnet=list())
        self._demanda_trafego =list()
        self.sistema_fotovoltaico = []



    @property
    def municipio(self):
        return self._municipio

    @municipio.setter
    def municipio(self, municipio):
        self._municipio = municipio

    @property
    def setores_censitarios(self):
        return self._setores_censitarios

    @setores_censitarios.setter
    def setores_censitarios(self, setores_censitarios):
        self._setores_censitarios = setores_censitarios

    @property
    def vizinhos(self):
        return self._vizinhos

    @vizinhos.setter
    def vizinhos(self, value):
        self._vizinhos = value

    @property
    def possui_energia(self):
        return self._possui_energia

    @possui_energia.setter
    def possui_energia(self, value):
        self._possui_energia = value

    #demanda trafego
    @property
    def demanda_trafego_por_area(self):
        return self._demanda_trafego_por_area

    @demanda_trafego_por_area.setter
    def demanda_trafego_por_area(self, value):
        self._demanda_trafego_por_area = value

    @property
    def densidade_usuarios(self):
        return self._densidade_usuarios

    @densidade_usuarios.setter
    def densidade_usuarios(self, value):
        self._densidade_usuarios = value

    @property
    def demanda_usuarios(self):
        return self._demanda_usuarios

    @demanda_usuarios.setter
    def demanda_usuarios(self, value):
        self._demanda_usuarios = value

    @property
    def demanda_aplicacoes(self):
        return self._demanda_aplicacoes

    @demanda_aplicacoes.setter
    def demanda_aplicacoes(self, value):
        self._demanda_aplicacoes = value

    @property
    def user_fraction(self):
        return self._user_fraction

    @user_fraction.setter
    def user_fraction(self, value):
        self._user_fraction = value

    @property
    def demanda_trafego_terminais(self):
        return self._demanda_trafego_terminais

    @demanda_trafego_terminais.setter
    def demanda_trafego_terminais(self, value):
        self._demanda_trafego_terminais = value

  #demanda trafego
    @property
    def demanda_trafego(self):
        return self._demanda_trafego

    @demanda_trafego.setter
    def demanda_trafego(self, value):
        self._demanda_trafego = value

    @property
    def possivel_tipo_ponto(self):
        return self._possivel_tipo_ponto

    @possivel_tipo_ponto.setter
    def possivel_tipo_ponto(self, possivel_tipo_ponto):
        self._possivel_tipo_ponto = possivel_tipo_ponto


