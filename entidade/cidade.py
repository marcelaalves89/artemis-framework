class Municipio:
    def __init__(self, area, populacao):
        self._area = area
        self._populacao = populacao

    @property
    def area(self):
        return self._area

    @property
    def populacao(self):
        return self._populacao

    @area.setter
    def area(self, area):
        if area < 0:
            raise RuntimeError('[ERRO]: O parametro area deve ser um valor positivo')
        else: self._area = area

    @populacao.setter
    def populacao(self, populacao):
        if populacao < 0:
            raise RuntimeError('[ERROR]: O parametro numero_habitantes deve ser um valor positivo')
        else: self._populacao = populacao

    def __str__(self):
        return 'Municipio [area={}, numero_habitantes={}]'.format(self._area, self._populacao)
