from entidade.ponto_coordenada import PontoCoordenada


class Estrada (PontoCoordenada):

    def __init__(self, id, nome, coordenadas, id_municipio):
        super().__init__(id, nome, coordenadas, id_municipio)
        self.id_estrada = id
        self.vizinhos_obj = []

    @property
    def id_tipo_percurso(self):
        return self._id_tipo_percurso

    @id_tipo_percurso.setter
    def id_tipo_percurso(self, id_tipo_percurso):
        self._id_tipo_percurso = id_tipo_percurso

    @property
    def id_tipo_infraestrutura(self):
        return self._id_tipo_infraestrutura

    @id_tipo_infraestrutura.setter
    def id_tipo_infraestrutura(self, id_tipo_infraestrutura):
        self._id_tipo_infraestrutura = id_tipo_infraestrutura

    @property
    def id_agrupamento_origem(self):
        return self._id_agrupamento_origem

    @id_agrupamento_origem.setter
    def id_agrupamento_origem(self, id_agrupamento_origem):
        self._id_agrupamento_origem = id_agrupamento_origem

    @property
    def id_agrupamento_destino(self):
        return self._id_agrupamento_destino

    @id_agrupamento_destino.setter
    def id_agrupamento_destino(self, id_agrupamento_destino):
        self._id_agrupamento_destino = id_agrupamento_destino
