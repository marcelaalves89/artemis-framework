from enum import Enum

class TipoMunicipio(Enum):

    RURAL_ADJACENTE = 1
    RURAL_REMOTO = 2
    URBANO = 3
    INTERMEDIARIO_ADJACENTE = 4
    INTERMEDIARIO_REMOTO = 5

    def __getitem__(self, value):
        return self.names[value]
