
class TipoInfraEstrutura:

    FIBRA_OTICA = 0
    VIA_RADIO = 1
    COBRE = 2
    INESPRESSIVA_INEXISTENTE = 3

    def peso(self, tipo):
        if (tipo == self.FIBRA_OTICA):
            return 1
        elif (tipo == self.VIA_RADIO):
            return 2
        elif (tipo == self.COBRE):
            return 4
        elif (tipo == self.INESPRESSIVA_INEXISTENTE):
            return 8