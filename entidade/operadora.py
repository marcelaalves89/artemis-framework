class Operadora:

    operadoras = {1: 'CLARO', 2: 'OI', 3: 'TIM', 4: 'VIVO'}

    def busca_id(self, nome):
        for value, id in self.operadoras.items():
            if(value == nome):
                return id
