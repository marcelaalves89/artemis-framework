
class DadosSocioEconomico:

    @property
    def total_alunos(self):
        return self._total_alunos

    @total_alunos.setter
    def total_alunos(self, value):
        self._total_alunos = value

    @property
    def percentual_alunos_ead(self):
        return self._percentual_alunos_ead

    @percentual_alunos_ead.setter
    def percentual_alunos_ead(self, value):
        self._percentual_alunos_ead = value

    @property
    def total_docentes(self):
        return self._total_docentes

    @total_docentes.setter
    def total_docentes(self, value):
        self._total_docentes = value

    @property
    def percentual_pop_ativa(self):
        return self._percentual_pop_ativa

    @percentual_pop_ativa.setter
    def percentual_pop_ativa(self, value):
        self._percentual_pop_ativa = value

    @property
    def percentual_pop_inativa(self):
        return 1.0 - self.percentual_pop_ativa

    @percentual_pop_inativa.setter
    def percentual_pop_inativa(self, value):
        self._percentual_pop_inativa = value

    @property
    def total_servidores_publicos(self):
        return self._total_servidores_publicos

    @total_servidores_publicos.setter
    def total_servidores_publicos(self, value):
        self._total_servidores_publicos = value

    @property
    def total_servidores_publicos_saude(self):
        return self._total_servidores_publicos_saude

    @total_servidores_publicos_saude.setter
    def total_servidores_publicos_saude(self, value):
        self._total_servidores_publicos_saude = value


    @property
    def total_agencias_bancarias(self):
        return self._total_agencias_bancarias

    @total_agencias_bancarias.setter
    def total_agencias_bancarias(self, value):
        self._total_agencias_bancarias = value

    @property
    def total_veiculos(self):
        return self._total_veiculos

    @total_veiculos.setter
    def total_veiculos(self, value):
        self._total_veiculos = value

    @property
    def total_cruzamentos(self):
        return self._total_cruzamentos

    @total_cruzamentos.setter
    def total_cruzamentos(self, value):
        self._total_cruzamentos = value

    def __repr__(self):
        return str(self.__dict__)
