from entidade.regiao import Regiao


class SetorCensitario(Regiao):

    def __init__(self, id, nome, area):
        self.id_setor_censitario = id
        super().__init__(nome, area)

    @property
    def municipio(self):
        return self._municipio

    @municipio.setter
    def municipio(self, value):
        self._municipio = value

    @property
    def agrupamento_territorial(self):
        return self._agrupamento_territorial

    @agrupamento_territorial.setter
    def agrupamento_territorial(self, value):
        self._agrupamento_territorial = value


