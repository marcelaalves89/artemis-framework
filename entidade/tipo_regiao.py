from enum import Enum

class TipoRegiao(Enum):

    URBANO = 1
    RURAL = 2

    def __getitem__(self, value):
        return self.names[value]
