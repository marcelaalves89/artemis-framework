import pandas as pd
import matplotlib.pyplot as plt
from conf.configuracao import Configuracao
from conf.parametro import Parametro
from conf.path import Path
import numpy as np
import copy as cp
from entidade.BS import BS
from entidade.tipo_BS import TipoBS
from rede_de_acesso.rede_acesso_antigo import RedeAcessoAntigo


class CenarioRedeAcessoAntigo:

    def __init__(self):
        self.configuracao = Configuracao()
        self.configuracao.set(Parametro.TEMPO_ANALISE, 15)

    def gera_bs_pre_implantada(self, agrupamento):
        header_csv = ['id', 'id_municipio', 'cod_agrupamento','tipo_bs', 'id_operadora','tipo_ponto',
                      'id_pontos_rua_agrupamento', 'hub_bs']
        df = pd.read_csv(Path.ARQUIVO_BS.valor, delimiter=";", usecols=header_csv)

        for index, row in df.iterrows():
            if(row['cod_agrupamento'] == agrupamento.id_agrupamento_territorial):

                bs = BS(row['id'], row['id_municipio'], row['cod_agrupamento'],
                        TipoBS[row['tipo_bs']],
                        row['id_operadora'], row['tipo_ponto'], row['id_pontos_rua_agrupamento'], 0, row['hub_bs'])
                agrupamento.lista_bs['implantacao_macro'].append(bs)
                agrupamento.lista_bs['implantacao_hetnet'].append(cp.deepcopy(bs))
            else:
                continue

        return agrupamento

    def carrega_rede_acesso(self, municipio):
        for agrupamento in municipio.agrupamentos_territoriais:
                self.gera_bs_pre_implantada(agrupamento)
                rede_acesso = RedeAcessoAntigo(agrupamento, self.configuracao.tempo_analise)
                rede_acesso.calcula_dimensionamento_rede_acesso()
                print()

        self.debug(municipio)


    def debug(self, municipio):
        time = np.arange(self.configuracao.tempo_analise)
        capacidade_atendimento_rede_acesso_macro = np.zeros(self.configuracao.tempo_analise)
        capacidade_atendimento_rede_acesso_femto = np.zeros(self.configuracao.tempo_analise)
        volume_trafego_rede_acesso_total = np.zeros(self.configuracao.tempo_analise)

        for ag in municipio.agrupamentos_territoriais:
                capacidade_atendimento_rede_acesso_macro += ag.capacidade_atendimento_rede_acesso['implantacao_macro']
                capacidade_atendimento_rede_acesso_femto += ag.capacidade_atendimento_rede_acesso['implantacao_hetnet']
                volume_trafego_rede_acesso_total = volume_trafego_rede_acesso_total + ag.demanda_trafego

                # plt.title('Capacidade de Atendimento Rede de Acesso - Aglomerado {}'.format(ag.id_agrupamento_territorial))
                # plt.plot(ag.demanda_trafego, ag.capacidade_atendimento_rede_acesso['implantacao_macro'], '-*',
                #      label='Capacidade Implantação Macro Only [Mbps]')
                # plt.plot(ag.demanda_trafego, ag.capacidade_atendimento_rede_acesso['implantacao_hetnet'], '-o',
                #      label='Capacidade Implantação HetNet [Mbps]')
                # plt.xlabel('Volume de Tráfego de Dados do Aglomerado [Mbps]')
                # plt.ylabel('Capacidade de Atendimento [Mbps]')
                # plt.grid(linestyle=':')
                # plt.legend(loc='best')
                # plt.figure()

        #         plt.title('Capacidade de Atendimento Rede de Acesso - Aglomerado {}'.format(ag.id_agrupamento_territorial))
        #         plt.plot(time, ag.capacidade_atendimento_rede_acesso['implantacao_macro'], '-*',
        #              label='Capacidade Implantação Macro Only [Mbps]')
        #         plt.plot(time, ag.capacidade_atendimento_rede_acesso['implantacao_hetnet'], '-o',
        #              label='Capacidade Implantação HetNet [Mbps]')
        #         plt.plot(time, ag.demanda_trafego, '-.', label='Volume de Tráfego [Mbps]')
        #         plt.xlabel('Unidade de Tempo (t)')
        #         plt.ylabel('Capacidade de Atendimento [Mbps]')
        #         plt.grid(linestyle=':')
        #         plt.legend(loc='best')
        #         plt.figure()
        #
        #
        # plt.title('Capacidade de Atendimento Rede de Acesso - Municipio {}'.format(municipio.id_municipio))
        # plt.plot(time, capacidade_atendimento_rede_acesso_macro, '-*',
        #          label='Capacidade Implantação Macro Only [Mbps]')
        # plt.plot(time, capacidade_atendimento_rede_acesso_femto, '-o',
        #          label='Capacidade Implantação HetNet [Mbps]')
        # plt.plot(time, volume_trafego_rede_acesso_total, '-.', label='Volume de Tráfego [Mbps]')
        # plt.xlabel('Unidade de Tempo (t)')
        # plt.ylabel('Capacidade de Atendimento [Mbps]')
        # plt.grid(linestyle=':')
        # plt.legend(loc='best')
        # plt.show()