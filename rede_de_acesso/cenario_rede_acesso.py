import pandas as pd
from conf.path import Path
import copy as cp

from custos.radio_transporte.tco_radio import TcoRadio
from entidade.BS import BS
from entidade.tipo_BS import TipoBS
from rede_de_acesso.estrategia_cenario import EstrategiaCenario
from rede_de_acesso.rede_acesso import RedeAcesso


class CenarioRedeAcesso:

    def __init__(self, tempo_analise):
        self.tempo_analise = tempo_analise
        self.tco_radio = dict()

        self.npv = dict()

    def gera_bs_pre_implantada(self, agrupamento):
        header_csv = ['id', 'id_municipio', 'cod_agrupamento','tipo_bs', 'id_operadora','tipo_ponto',
                      'id_pontos_rua_agrupamento', 'hub_bs']
        df = pd.read_csv(Path.ARQUIVO_BS.valor, delimiter=";", usecols=header_csv)

        for index, row in df.iterrows():
            if (row['cod_agrupamento'] == agrupamento.id_agrupamento_territorial):
                bs = BS(row['id'], row['id_municipio'], row['cod_agrupamento'],
                        TipoBS[row['tipo_bs']],
                        row['id_operadora'], row['tipo_ponto'], row['id_pontos_rua_agrupamento'], 0, row['hub_bs'])
                agrupamento.lista_bs['implantacao_macro'].append(bs)
                agrupamento.lista_bs['implantacao_hetnet'].append(cp.deepcopy(bs))
            else:
                continue

        return agrupamento

    def carrega_rede_acesso(self, municipio):
        cenarios_estrategia_municipio = dict()
        for estrategia in EstrategiaCenario:
            for agrupamento in municipio.agrupamentos_territoriais:
                agrupamento.tipo_cenario = estrategia.tipo_cenario
                agrupamento.estrategia_atualizacao_bs = estrategia.estrategia_atualizacao_bs
                agrupamento.lista_bs = dict(implantacao_macro=list(), implantacao_hetnet=list())
                if estrategia.tipo_estrategia == 'Brownfield':
                    self.gera_bs_pre_implantada(agrupamento)

            cenarios_estrategia_municipio[estrategia.nome] = cp.deepcopy(municipio)
        self.run(cenarios_estrategia_municipio)
        municipio.cenarios = cenarios_estrategia_municipio

    def carrega_estrategias(self, municipios):
        self.tco_radio = dict()
        self.tco_transporte = dict()
        self.npv = dict()
        for key in municipios:
        # Calcula Dimensionamento de Trafego e Rede para cada cenário
            print('Municipio/Cenário {}'.format(key))
            for agrupamento in municipios.get(key).agrupamentos_territoriais:
                rede_acesso = RedeAcesso(agrupamento, self.tempo_analise)
                rede_acesso.calcula_dimensionamento_rede_acesso()
                print()

            # Calcula TCO e NPV para cada cenário
            self.tco_radio[key] = TcoRadio(municipios[key], self.tempo_analise)

    def run(self, municipios):
        self.carrega_estrategias(municipios)
        for key in municipios:
            # Calcula o TCO de Rádio e Transporte
            self.tco_radio[key].get_tco()



