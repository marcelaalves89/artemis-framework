import copy as cp
from entidade.BS import BS
from entidade.tipo_BS import TipoBS
import numpy as np

from entidade.tipo_ponto import TipoPonto


class RedeAcesso:

    def __init__(self, agrupamento, tempo_analise):
        self.agrupamento = agrupamento
        self.agrupamento.capacidade_atendimento_rede_acesso = dict(implantacao_macro=np.zeros(tempo_analise),
                                                                   implantacao_hetnet=np.zeros(tempo_analise))
        self.agrupamento.demanda_trafego = np.zeros(tempo_analise)
        self.agrupamento.quantidade_novas_bs_macro = dict()
        self.agrupamento.quantidade_novas_bs_hetnet = dict()
        self.agrupamento.total_bs_macro = dict()
        self.agrupamento.total_bs_hetnet = dict()
        self.list_pontos_macro = []
        self.list_pontos_micro = []
        self.list_pontos_pico = []
        self.list_pontos_femto = []
        self.pontos_selecionados = dict()

    def adicionar_BS(self, BS):
        self.agrupamento.lista_bs['implantacao_macro'].append(BS)
        self.agrupamento.lista_bs['implantacao_hetnet'].append(cp.deepcopy(BS))

    def inicializa_quantitativo_bs(self, ano):
        self.agrupamento.quantidade_novas_bs_macro[ano] = []
        self.agrupamento.quantidade_novas_bs_hetnet[ano] = []
        self.agrupamento.total_bs_macro[ano] = []
        self.agrupamento.total_bs_hetnet[ano] = []

    def carrega_possiveis_pontos_bs(self):
        #carrega todos os possíveis pontos das BSs do agrupamento
        for p in self.agrupamento.ruas:
            if p.possivel_tipo_ponto != '':
                if TipoPonto().isMacro(p.possivel_tipo_ponto):
                    self.list_pontos_macro.append(p.id)
                elif TipoPonto().isMicro(p.possivel_tipo_ponto):
                    self.list_pontos_micro.append(p.id)
                elif TipoPonto().isPico(p.possivel_tipo_ponto):
                    self.list_pontos_pico.append(p.id)
                elif TipoPonto().isFemto(p.possivel_tipo_ponto):
                    self.list_pontos_femto.append(p.id)

    def calcula_dimensionamento_rede_acesso(self):
        total_novas_bs_macro_agrupamento = 0
        total_novas_bs_hetnet_agrupamento = 0

        self.carrega_possiveis_pontos_bs()

        print('Dimensionamento da Rede de Rádio do Aglomerado {}:'.format(self.agrupamento.id_agrupamento_territorial))

        pontos_macro = cp.deepcopy(self.list_pontos_macro)
        pontos_micro = cp.deepcopy(self.list_pontos_micro)
        pontos_femto = cp.deepcopy(self.list_pontos_femto)
        pontos_pico = cp.deepcopy(self.list_pontos_pico)
        for ano, demanda_ano in enumerate(self.agrupamento.demanda_trafego_por_area):
            self.inicializa_quantitativo_bs(ano)

            self.agrupamento.demanda_trafego[ano] = demanda_ano * self.agrupamento.area
            print('Ano (t): {}'.format(ano))
            print('Demanda de Trafego: {} Mbps'.format(self.agrupamento.demanda_trafego[ano]))


            capacidade_atendimento_macro, capacidade_atendimento_femto = self.__capacidade_atendimento_rede_acesso()
            if capacidade_atendimento_macro == 0:
                print('Capacidade de Atendimento Inexistente no Aglomerado {}:'.format(self.agrupamento.id_agrupamento_territorial))
                print('Realizando a implantação de uma BS para criação de cobertura básica')

                self.implanta_cobertura_basica(ano)
                print()
                # if self.agrupamento.situacao == 'Sede':
                #     nova_bs =BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                #            TipoBS.MACRO_4G, 0, 0, 0, ano, True, False)
                #         #BS(0, TipoBS.MACRO_4G, ano, False)
                #     self.adicionar_BS(nova_bs)
                #     print('Implantação de uma {} BS com tecnologia {}'.format(nova_bs.tipo_BS.tipo,
                #                                                               nova_bs.tipo_BS.tecnologia))
                # else:
                #     diff_macro = abs((TipoBS.MACRO_4G.cobertura_por_setor * TipoBS.MACRO_4G.setores) - self.agrupamento.area)
                #     diff_micro = abs((TipoBS.MICRO_4G.cobertura_por_setor * TipoBS.MICRO_4G.setores) - self.agrupamento.area)
                #     if diff_macro < diff_micro:
                #         #Verificar como identificar operadora, tipo_ponto, id_rua
                #         nova_bs =BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                #            TipoBS.MACRO_4G, 0, 0, 0, ano, True, False)
                #         # BS(0, TipoBS.MACRO_4G, ano, False)
                #        # self, id, id_municipio, id_agrupamento, tipo_bs, id_operadora, tipo_ponto
                #        # , id_rua, ano = 0, existenncia_previa = True
                #     else:
                #         nova_bs =BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                #            TipoBS.MICRO_4G, 0, 0, 0, ano, True, False)
                #             #BS(0, TipoBS.MICRO_4G, ano, False)
                #     bs_macro= BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                #            TipoBS.MACRO_4G, 0, 0, 0, ano, True, False)
                #     self.agrupamento.lista_bs['implantacao_macro'].append(bs_macro)
                #       #  BS(0, TipoBS.MACRO_4G, ano, False) )
                #     self.agrupamento.lista_bs['implantacao_hetnet'].append( nova_bs )
                #  #   self.agrupamento.quantidade_novas_bss['implantacao_macro'][ano] = nova_bs
                #
                #     self.agrupamento.quantidade_novas_bs_macro[ano].append(bs_macro)
                #     self.agrupamento.quantidade_novas_bs_hetnet[ano].append(nova_bs)


                    # print('Estratégia de Implantação Macro Only:')
                    # print('Implantação de uma Macro BS com tecnologia 4G')
                    # print('Estratégia de Implantação HetNet:')
                    # print('Implantação de uma {} BS com tecnologia {}'.format(nova_bs.tipo_BS.tipo, nova_bs.tipo_BS.tecnologia))
                    # print()

            capacidade_atendimento_macro, capacidade_atendimento_femto = self.__capacidade_atendimento_rede_acesso()
            self.agrupamento.capacidade_atendimento_rede_acesso['implantacao_macro'][ano] = capacidade_atendimento_macro
            self.agrupamento.capacidade_atendimento_rede_acesso['implantacao_hetnet'][ano] = capacidade_atendimento_femto

            print('Estratégia de Implantação Macro Only:')
            print('Capacidade de Atendimento de BSs existentes: {} Mbps'.format(capacidade_atendimento_macro))
            print('Estratégia de Implantação HetNet:')
            print('Capacidade de Atendimento de BSs existentes: {} Mbps'.format(capacidade_atendimento_femto))
            print()

            demanda_expansao_macro = self.agrupamento.demanda_trafego[ano] - capacidade_atendimento_macro
            demanda_expansao_femto = self.agrupamento.demanda_trafego[ano] - capacidade_atendimento_femto

            if demanda_expansao_macro >= 0 or demanda_expansao_femto >= 0:
                if demanda_expansao_macro >= 0:
                    print('Estratégia de Implantação Macro Only:')
                    print('Necessidade de atualização em {} Mbps'.format(demanda_expansao_macro))

                if demanda_expansao_femto >= 0:
                    print('Estratégia de Implantação HetNet:')
                    print('Necessidade de atualização em {} Mbps'.format(demanda_expansao_femto))

                print()

                teste_condicao = True
                while teste_condicao:
                    teste_condicao = self.__checa_possui_bs_atualizavel(ano, 'Macro')
                    print('É possível o upgrade de BSs na Estratégia de Implantação Macro Only? {}'.format(teste_condicao))

                    if teste_condicao is True:
                        print('Executa atualizacoes de Macro BSs')
                        self.__upgrade_bs(ano, demanda_expansao_macro, 'Macro')
                    print()

                    capacidade_atendimento_macro, capacidade_atendimento_femto = self.__capacidade_atendimento_rede_acesso()
                    if self.agrupamento.demanda_trafego[ano] - capacidade_atendimento_macro <= 0:
                        break

                teste_condicao = True
                while teste_condicao:
                    teste_condicao = self.__checa_possui_bs_atualizavel(ano, 'HetNet')
                    print('É possível o upgrade de BSs na Estratégia de Implantação HetNet? {}'.format(teste_condicao))

                    if teste_condicao is True:
                        print('Executa atualizacoes de Femto BSs')
                        self.__upgrade_bs(ano, demanda_expansao_femto, 'Femto')
                    print()

                    capacidade_atendimento_macro, capacidade_atendimento_femto = self.__capacidade_atendimento_rede_acesso()
                    if self.agrupamento.demanda_trafego[ano] - capacidade_atendimento_femto <= 0:
                        break

                capacidade_atendimento_macro, capacidade_atendimento_femto = self.__capacidade_atendimento_rede_acesso()
                demanda_expansao_macro = self.agrupamento.demanda_trafego[ano] - capacidade_atendimento_macro
                demanda_expansao_femto = self.agrupamento.demanda_trafego[ano] - capacidade_atendimento_femto

                if demanda_expansao_macro >= 0:
                    print('Estratégia de Implantação Macro Only: Necessidade de implantação de novas BSs')
                    print('Realiza a implantação de BSs novas do tipo Macro')
                    self.__implatacao_novas_bs(ano, demanda_expansao_macro, 'Macro')
                    print()
                if demanda_expansao_femto >= 0:
                    print('Estratégia de Implantação Femto: Necessidade de implantação de novas BSs')
                    print('Realiza a implantação de BSs novas do tipo Femto')
                    self.__implatacao_novas_bs(ano, demanda_expansao_femto, 'Femto')
                    print()
            else:
                print('Rede de Acesso não precisa ser atualizada')
            print()
            capacidade_atendimento_macro, capacidade_atendimento_femto = self.__capacidade_atendimento_rede_acesso()
            self.agrupamento.capacidade_atendimento_rede_acesso['implantacao_macro'][ano] = capacidade_atendimento_macro
            self.agrupamento.capacidade_atendimento_rede_acesso['implantacao_hetnet'][ano] = capacidade_atendimento_femto
            #quantitativo por ano
            self.carrega_quantitativo_bs(ano)
            #acumula quantitativo para o total geral do agrupamento
            if len(self.agrupamento.quantidade_novas_bs_macro[ano])>0:
                total_novas_bs_macro_agrupamento += len(self.agrupamento.quantidade_novas_bs_macro[ano])
            if len(self.agrupamento.quantidade_novas_bs_hetnet[ano])>0:
                total_novas_bs_hetnet_agrupamento += len(self.agrupamento.quantidade_novas_bs_hetnet[ano])

            self.gera_localizacoes_bs(ano, pontos_macro, pontos_micro, pontos_pico, pontos_femto)

        #quantitativo por agrupamento
        print('Total Geral Novas Bs Macro do Agrupamento: ' + str(total_novas_bs_macro_agrupamento))
        print('Total Geral Novas Bs Hetnet do Agrupamento: ' + str(total_novas_bs_hetnet_agrupamento))

        self.atualiza_tipo_ponto_rua_selecionados()

    def gera_localizacoes_bs(self, ano, pontos_macro, pontos_micro, pontos_pico, pontos_femto):
        self.localizacoes_macro(ano)
        self.localizacoes_hetnet(ano, pontos_macro, pontos_micro, pontos_pico, pontos_femto)

    def localizacoes_macro(self, ano):
        for bs in self.agrupamento.quantidade_novas_bs_macro[ano]:
            if bs.tipo_BS.tipo == 'Macro' and len(self.list_pontos_macro)>0:
                bs.id_rua = self.list_pontos_macro[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_MACRO
                self.list_pontos_macro.remove(self.list_pontos_macro[0])
            elif bs.tipo_BS.tipo == 'Micro' and len(self.list_pontos_micro)>0:
                bs.id_rua = self.list_pontos_micro[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_MICRO
                self.list_pontos_micro.remove(self.list_pontos_micro[0])
            elif bs.tipo_BS.tipo == 'Pico' and len(self.list_pontos_pico)>0:
                bs.id_rua = self.list_pontos_pico[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_PICO
                self.list_pontos_pico.remove(self.list_pontos_pico[0])
            elif bs.tipo_BS.tipo == 'Femto' and len(self.list_pontos_femto)>0:
                bs.id_rua = self.list_pontos_femto[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_FEMTO
                self.list_pontos_femto.remove(self.list_pontos_femto[0])

    def localizacoes_hetnet(self, ano, pontos_macro, pontos_micro, pontos_pico, pontos_femto):
        for bs in self.agrupamento.quantidade_novas_bs_hetnet[ano]:
            if bs.tipo_BS.tipo == 'Macro' and len(pontos_macro) > 0:
                bs.id_rua = pontos_macro[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_MACRO
                pontos_macro.remove(pontos_macro[0])
            elif bs.tipo_BS.tipo == 'Micro' and len(pontos_micro) > 0:
                bs.id_rua = pontos_micro[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_MICRO
                pontos_micro.remove(pontos_micro[0])
            elif bs.tipo_BS.tipo == 'Pico' and len(pontos_pico) > 0:
                bs.id_rua = pontos_pico[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_PICO
                pontos_pico.remove(pontos_pico[0])
            elif bs.tipo_BS.tipo == 'Femto' and len(pontos_femto) > 0:
                bs.id_rua = pontos_femto[0]
                if self.pontos_selecionados.get(bs.id_rua) not in self.pontos_selecionados:
                    self.pontos_selecionados[bs.id_rua] = TipoPonto.PONTO_FEMTO
                pontos_femto.remove(pontos_femto[0])

    def atualiza_tipo_ponto_rua_selecionados(self):
        #atualiza a coluna tipo_ponto como o tipo do ponto selecionado como localização da BS
        for p in self.agrupamento.ruas:
            if self.pontos_selecionados.get(p.id):
                p.tipo_ponto = self.pontos_selecionados[p.id]

    def carrega_quantitativo_bs(self, ano):
        self.agrupamento.total_bs_macro[ano] = self.agrupamento.lista_bs['implantacao_macro']
        print('Total Geral de BSs Macro no Ano: ' + str(len(self.agrupamento.total_bs_macro[ano])))
        self.agrupamento.total_bs_hetnet[ano] = self.agrupamento.lista_bs['implantacao_hetnet']
        print('Total Geral de BSs Hetnet no Ano: ' + str(len(self.agrupamento.total_bs_hetnet[ano])))
        if len(self.agrupamento.quantidade_novas_bs_macro[ano]) > 0:
            print(self.agrupamento.quantidade_novas_bs_macro[ano])
        print('Total novas Bs Macro no Ano:' + str(len(self.agrupamento.quantidade_novas_bs_macro[ano])))
        if len(self.agrupamento.quantidade_novas_bs_hetnet[ano]) > 0:
            print(self.agrupamento.quantidade_novas_bs_hetnet[ano])
        print('Total novas Bs Hetnet no Ano:' + str(len(self.agrupamento.quantidade_novas_bs_hetnet[ano])))

    def __capacidade_atendimento_rede_acesso(self):
        capacidade_atendimento_macro = 0.0
        capacidade_atendimento_femto = 0.0

        for c in self.agrupamento.lista_bs['implantacao_macro']:
            capacidade_atendimento_macro += (c.tipo_BS.capacidade * c.tipo_BS.setores)
        for c in self.agrupamento.lista_bs['implantacao_hetnet']:
            capacidade_atendimento_femto += (c.tipo_BS.capacidade * c.tipo_BS.setores)
        return capacidade_atendimento_macro, capacidade_atendimento_femto

    def __checa_possui_bs_atualizavel(self, ano, tipo):
        result = False
        if tipo == 'Macro':
            for bs in self.agrupamento.lista_bs['implantacao_macro']:
                result = result or bs.tipo_BS.atualizavel
        else:
            for bs in self.agrupamento.lista_bs['implantacao_hetnet']:
                result = result or bs.tipo_BS.atualizavel
        return result

    def __upgrade_bs(self, t, demanda_expansao, tipo):
        if demanda_expansao >=0:
            if tipo == 'Macro':
                lista_bs = self.agrupamento.lista_bs['implantacao_macro']
            else:
                lista_bs = self.agrupamento.lista_bs['implantacao_hetnet']

            capacidade_expandida_acumulada = 0.0
            for bs in lista_bs:
                capacidade_antes = bs.tipo_BS.capacidade * bs.tipo_BS.setores
                print('Capacidade antes da Atualização: {} Mbps ({} BS com tecnologia {})'.format(capacidade_antes,
                                                                                               bs.tipo_BS.tipo,
                                                                                                  bs.tipo_BS.tecnologia))
                if self.agrupamento.estrategia_atualizacao_bs == 'Tradicional':
                    bs.upgrade(t)
                elif self.agrupamento.estrategia_atualizacao_bs == 'Acelerada':
                    bs.upgrade_5G(t)
                else:
                    raise RuntimeError('Estratégia de Atualização de BS não encontrada: {}'.format(self.agrupamento.estrategia_atualizacao_bs))

                capacidade_depois = bs.tipo_BS.capacidade * bs.tipo_BS.setores
                print('Capacidade após Atualização: {} Mbps ({} BS com tecnologia {})'.format(capacidade_depois,
                                                                                              bs.tipo_BS.tipo,
                                                                                               bs.tipo_BS.tecnologia))
                capacidade_expandida_acumulada += (capacidade_depois - capacidade_antes)
                if capacidade_expandida_acumulada > demanda_expansao:
                    break

    def __implatacao_novas_bs(self, t, demanda_expansao, tipo_bs):
        if tipo_bs == 'Macro':
            if self.agrupamento.estrategia_atualizacao_bs == 'Tradicional':
                if t <= self.agrupamento.tempo_maturacao:
                    tipo = TipoBS.MACRO_45G
                else:
                    tipo = TipoBS.MACRO_5G
            elif self.agrupamento.estrategia_atualizacao_bs == 'Acelerada':
                tipo = TipoBS.MACRO_5G
            else:
                raise RuntimeError(
                    'Estratégia de Atualização de BS não encontrada: {}'.format(self.agrupamento.estrategia_atualizacao_bs))
        else:
            if self.agrupamento.estrategia_atualizacao_bs == 'Tradicional':
                if t <= self.agrupamento.tempo_maturacao:
                    tipo = TipoBS.FEMTO_45G
                else:
                    tipo = TipoBS.FEMTO_5G
            elif self.agrupamento.estrategia_atualizacao_bs == 'Acelerada':
                tipo = TipoBS.FEMTO_5G
            else:
                raise RuntimeError('Estratégia de Atualização de BS não encontrada: {}'.format(self.estrategia_atualizacao_bs))

        print('Inclusão de BSs por Capacidade em {} Mbps'.format(demanda_expansao))
        n_bs = np.ceil( demanda_expansao/ (tipo.capacidade * tipo.setores) )
        print('Implantar {} BSs com tecnologia {}'.format(n_bs, tipo.tecnologia))
        for nb in range(int(n_bs)):
            nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                           tipo, 0, 0, 0, t, False, False)
                #BS(0, tipo, t, False)
            if tipo_bs == 'Macro':
                self.agrupamento.lista_bs['implantacao_macro'].append(nova_bs)
                self.agrupamento.quantidade_novas_bs_macro[t].append(nova_bs)
            else:
                self.agrupamento.lista_bs['implantacao_hetnet'].append(nova_bs)
                self.agrupamento.quantidade_novas_bs_hetnet[t].append(nova_bs)

    def implanta_cobertura_basica(self, ano):
        ponto = 0#get_ponto_aleatorio()
        if self.agrupamento.estrategia_atualizacao_bs == 'Tradicional':
            diff_macro = (TipoBS.MACRO_4G.cobertura_por_setor * TipoBS.MACRO_4G.setores) - self.agrupamento.area
            diff_micro = (TipoBS.MICRO_4G.cobertura_por_setor * TipoBS.MICRO_4G.setores) - self.agrupamento.area
            if diff_macro >= 0 and diff_micro >= 0:
                if diff_macro < diff_micro:
                    nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                                 TipoBS.MACRO_4G,  0, 0, 0, ano, True, False)
                else:
                    nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                                 TipoBS.MICRO_4G, 0, 0, 0, ano, True, False)
            else:
                nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                             TipoBS.MACRO_4G, 0, 0, 0, ano, True, False)

            print('Estratégia de Implantação Macro Only:')
            print('Implantação de uma Macro BS com tecnologia 4G')
            print('Estratégia de Implantação HetNet:')
            print('Implantação de uma {} BS com tecnologia {}'.format(nova_bs.tipo_BS.tipo, nova_bs.tipo_BS.tecnologia))

            bs_macro = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                                                                     TipoBS.MACRO_4G, 0, 0, 0, ano, True, False)
            self.agrupamento.lista_bs['implantacao_macro'].append(bs_macro)
            self.agrupamento.lista_bs['implantacao_hetnet'].append(nova_bs)

            self.agrupamento.quantidade_novas_bs_macro[ano].append(bs_macro)
            self.agrupamento.quantidade_novas_bs_hetnet[ano].append(nova_bs)

        elif self.agrupamento.estrategia_atualizacao_bs == 'Acelerada':
            diff_macro = (TipoBS.MACRO_5G.cobertura_por_setor * TipoBS.MACRO_5G.setores) - self.agrupamento.area
            diff_micro = (TipoBS.MICRO_5G.cobertura_por_setor * TipoBS.MICRO_5G.setores) - self.agrupamento.area
            if diff_macro >= 0 and diff_micro >= 0:
                if diff_macro < diff_micro:
                    nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                                 TipoBS.MACRO_5G,  0, 0, 0, ano, True, False)
                else:
                    nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                                 TipoBS.MICRO_5G, 0, 0, 0, ano, True, False)
            else:
                nova_bs = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                             TipoBS.MACRO_5G, 0, 0, 0, ano, True, False)
            bs_macro = BS(0, self.agrupamento.municipio, self.agrupamento.id_agrupamento_territorial,
                                                                     TipoBS.MACRO_5G, 0, 0, 0, ano, True, False)
            self.agrupamento.lista_bs['implantacao_macro'].append(bs_macro)
            self.agrupamento.lista_bs['implantacao_hetnet'].append(nova_bs)

            self.agrupamento.quantidade_novas_bs_macro[ano].append(bs_macro)
            self.agrupamento.quantidade_novas_bs_hetnet[ano].append(nova_bs)

            print('Estratégia de Implantação Macro Only:')
            print('Implantação de uma Macro BS com tecnologia 5G')
            print('Estratégia de Implantação HetNet:')
            print('Implantação de uma {} BS com tecnologia {}'.format(nova_bs.tipo_BS.tipo, nova_bs.tipo_BS.tecnologia))
        else:
            raise RuntimeError(
                'Estratégia de Atualização de BS não encontrada: {}'.format(self.agrupamento.estrategia_atualizacao_bs))
