from enum import Enum


class EstrategiaCenario(Enum):
    BF_5GL = (1, 'BF+5GS', 'Original', 'Tradicional', 'Brownfield')
    GF_5GL = (2, 'GF+5GS', 'Alternativo', 'Tradicional', 'Greenfield')
    BF_5GR = (3, 'BF+5GF','Alternativo', 'Acelerada', 'Brownfield')
    GF_5GR = (4, 'GF+5GF','Alternativo', 'Acelerada', 'Greenfield')

    def __init__(self, id, nome, tipo_cenario, estrategia_atualizacao_bs, tipo_estrategia):
        self.id= id
        self.nome = nome
        self.tipo_cenario = tipo_cenario
        self.estrategia_atualizacao_bs = estrategia_atualizacao_bs
        self.tipo_estrategia = tipo_estrategia