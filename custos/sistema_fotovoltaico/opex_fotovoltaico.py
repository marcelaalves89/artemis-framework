from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico
from sistema_fotovoltaico.bateria import Bateria
from sistema_fotovoltaico.medidor import Medidor
from sistema_fotovoltaico.painel import Painel

class OPEXFotovoltaico:

    def __init__(self, qtd_paineis, qtd_inversores, inversor, qtd_medidores, instala_medidor,
                 qtd_inversores_manutencao, paineis_manutencao, qtd_baterias, tipo_sistema):
        self.qtd_paineis = qtd_paineis
        self.qtd_inversores = qtd_inversores
        self.inversor = inversor
        self.qtd_medidores = qtd_medidores
        self.opex = 0
        self.taxa_manutencao = 0.005
        self.custo_aluguel_diario = 0.00000009090909091
        self.instala_medidor = instala_medidor
        self.qtd_inversores_manutencao = qtd_inversores_manutencao
        self.paineis_manutencao = paineis_manutencao
        self.qtd_baterias = qtd_baterias
        self.tipo_sistema = tipo_sistema

    def calcula_opex(self):
        custo_manutencao = self.calcula_custo_manutencao()
        #   preco_instalacao_medidor = 300 #Para o preco do medidor entra uma regrinha, esse custo só acontece uma vez durante os 15 anos de análise
        # equação 25
        if self.tipo_sistema == TipoSistemaFotovoltaico.ONGRID.name:
            self.opex = custo_manutencao + (self.qtd_medidores *
                        (self.preco_instalacao_medidor() + Medidor.MED_1.consumo_minimo_diario * Medidor.MED_1.preco_compra_kWh * 365)) + \
                        (self.qtd_paineis * Painel.PAINEL_320W.area * self.custo_aluguel_diario * 365)
        else:
            self.opex = custo_manutencao + (
                        self.qtd_paineis * Painel.PAINEL_320W.area * self.custo_aluguel_diario * 365)

        return self.opex

    def preco_instalacao_medidor(self):
        preco_instalacao_medidor = 0
        if self.instala_medidor:
            preco_instalacao_medidor = Medidor.MED_1.preco_instalacao
        return preco_instalacao_medidor

    #desmembramento equação 26 baseado na implementação da dissertação
    def calcula_custo_manutencao(self):
        custo_manutencao = self.calcula_custo_manutencao_painel() + self.calcula_custo_manutencao_kit() + self.calcula_custo_manutencao_inversor()
       # Esta bateria, especicamente, não possui manutenção, ela é selada.
       #  if self.tipo_sistema == TipoSistemaFotovoltaico.OFFGRID.name:
       #      custo_manutencao += self.calcula_custo_manutencao_bateria()
        return custo_manutencao

    def calcula_custo_manutencao_painel(self):
        return self.taxa_manutencao * \
                                  (self.qtd_paineis * (Painel.PAINEL_320W.preco +
                                                       Painel.PAINEL_320W.custo_instalacao_kit))
    def calcula_custo_manutencao_kit(self):
        if self.paineis_manutencao > 0:
            return self.taxa_manutencao * \
                                  (self.paineis_manutencao * Painel.PAINEL_320W.custo_instalacao_kit)
        else:
            return 0

    def calcula_custo_manutencao_inversor(self):
        preco_inversor = self.inversor.value[1]
        if self.qtd_inversores_manutencao > 0:
            return self.qtd_inversores_manutencao * preco_inversor * self.taxa_manutencao
        else:
            return 0

    def calcula_custo_manutencao_bateria(self):
        return self.taxa_manutencao * self.qtd_baterias * Bateria.BATERIA_1.preco

    def custo_energia_minima_rede_ano(self, qtd_medidores):
        return (qtd_medidores * Medidor.MED_1.consumo_minimo_diario * Medidor.MED_1.preco_compra_kWh * 365)

    def custo_aluguel(self, qtd_paineis):
        return (self.qtd_paineis * Painel.PAINEL_320W.area * self.custo_aluguel_diario * 365)

