from custos.sistema_fotovoltaico.capex_sistema_fotovoltaico import CAPEXFotovoltaico
from custos.sistema_fotovoltaico.opex_fotovoltaico import OPEXFotovoltaico
from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico
from sistema_fotovoltaico.bateria import Bateria
from sistema_fotovoltaico.medidor import Medidor
from sistema_fotovoltaico.painel import Painel


class TCOFotovoltaico:

    def __init__(self, sistema_fotovoltaico, tempo_analise):
        self.sistema_fotovoltaico = sistema_fotovoltaico
        self.tempo_analise = tempo_analise
        self.capex_acumulado = 0
        self.opex_acumulado = 0
        self.medidor_instalado = False
        #guarda a quantidade de equipamentos para manutenção
        self.qtd_inversores_manutencao = 0
        self.qtd_paineis_manutencao = 0
        self.qtd_baterias_substituicao = 0
        #guarda a quantidade equipamentos e em que ano comecarão a sofrer manutenção,
        # A chave é o ano que começará a manutenção
        self.qtd_inversor_ano_inicio_manutencao = dict()
        self.qtd_painel_ano_inicio_manutencao_kit = dict()
        self.qtd_bateria_ano_substituicao = dict()

    def calcula_TCO(self):
        qtd_painel_temp = 0
        qtd_inversor_temp = 0
        qtd_bateria_temp = 0

        for ano in range(self.tempo_analise):
            #para o capex considerei apenas os novos inversores, paineis do ano
            qtd_painel_ano = abs(self.sistema_fotovoltaico.qtd_paineis[ano] - qtd_painel_temp)
            qtd_inversor_ano = abs(self.sistema_fotovoltaico.qtd_inversores[ano] - qtd_inversor_temp)

            qtd_bateria_temp, qtd_bateria_ano = self.verifica_instalacao_baterias(ano, qtd_bateria_temp)

            capex_fv = CAPEXFotovoltaico(qtd_painel_ano, qtd_inversor_ano, ano, self.sistema_fotovoltaico.configuracao_inversor, self.sistema_fotovoltaico.tipo_sistema, qtd_bateria_ano)
            self.sistema_fotovoltaico.capex[ano] = capex_fv.calcula_capex()
            self.capex_acumulado += self.sistema_fotovoltaico.capex[ano]

            #variavel que verifica a necessidade de instalar o medidor ou não
            instala_medidor = self.considera_instalacao_medidor(self.sistema_fotovoltaico.qtd_paineis[ano])

            self.carrega_possivel_manutencao_painel(ano, qtd_painel_ano)
            self.carrega_possivel_manutencao_inversor(ano, qtd_inversor_ano)


            opex_fv = OPEXFotovoltaico(self.sistema_fotovoltaico.qtd_paineis[ano], self.sistema_fotovoltaico.qtd_inversores[ano], self.sistema_fotovoltaico.configuracao_inversor,
                                       self.sistema_fotovoltaico.qtd_medidores[ano], instala_medidor,
                                       self.qtd_inversores_manutencao, self.qtd_paineis_manutencao, qtd_bateria_temp, self.sistema_fotovoltaico.tipo_sistema)
            if instala_medidor:
                self.medidor_instalado = True
            self.sistema_fotovoltaico.opex[ano]= opex_fv.calcula_opex()
            self.sistema_fotovoltaico.custo_energia_minima_rede[ano] = opex_fv.custo_energia_minima_rede_ano(self.sistema_fotovoltaico.qtd_medidores[ano])
            self.sistema_fotovoltaico.custo_aluguel_fotovoltaico[ano] = opex_fv.custo_aluguel(self.sistema_fotovoltaico.qtd_paineis[ano])
            self.opex_acumulado += self.sistema_fotovoltaico.opex[ano]

            qtd_painel_temp = self.sistema_fotovoltaico.qtd_paineis[ano]
            qtd_inversor_temp = self.sistema_fotovoltaico.qtd_inversores[ano]
        self.sistema_fotovoltaico.tco_total = self.capex_acumulado + self.opex_acumulado

    def verifica_instalacao_baterias(self, ano, qtd_bateria_temp):
        qtd_bateria_ano = 0
        if self.sistema_fotovoltaico.tipo_sistema == TipoSistemaFotovoltaico.OFFGRID.name:
            qtd_bateria_ano = self.sistema_fotovoltaico.qtd_baterias[ano] - qtd_bateria_temp
            qtd_bateria_temp = self.sistema_fotovoltaico.qtd_baterias[ano]
            self.carrega_possivel_substituicao_bateria(ano, qtd_bateria_ano)
            if self.qtd_baterias_substituicao > 0:
                qtd_bateria_ano += self.qtd_baterias_substituicao
        return qtd_bateria_temp, qtd_bateria_ano

    def considera_instalacao_medidor(self, qtd_paineis):
        if self.sistema_fotovoltaico.tipo_sistema == TipoSistemaFotovoltaico.ONGRID.name:
            return qtd_paineis >= Medidor.MED_1.qtd_paineis_minimo_instalacao and self.medidor_instalado == False
        else:
            return False

    def carrega_possivel_manutencao_painel(self, ano, qtd_painel_ano):
        if qtd_painel_ano > 0:
            self.qtd_painel_ano_inicio_manutencao_kit[
                ano + Painel.PAINEL_320W.tempo_garantia_kit_instalacao] = qtd_painel_ano
        if self.qtd_painel_ano_inicio_manutencao_kit.get(ano):
            self.qtd_paineis_manutencao += self.qtd_painel_ano_inicio_manutencao_kit.get(ano)

    def carrega_possivel_manutencao_inversor(self, ano, qtd_inversor_ano):
        if qtd_inversor_ano > 0:
            #Ex: qtd_inversor_ano_inicio_manutencao[ano0 + 5 = 5] = 1 inversor
            self.qtd_inversor_ano_inicio_manutencao[
                ano + self.sistema_fotovoltaico.configuracao_inversor.anos_garantia] = qtd_inversor_ano
        # se em qtd_inversor_ano_inicio_manutencao tiver o ano em questão, recupera o quantitativo que irá sofrer manutenção
        if self.qtd_inversor_ano_inicio_manutencao.get(ano):
            self.qtd_inversores_manutencao += self.qtd_inversor_ano_inicio_manutencao.get(ano)

    def carrega_possivel_substituicao_bateria(self, ano, qtd_bateria):
        self.qtd_baterias_substituicao = 0
        if self.qtd_bateria_ano_substituicao.get(ano):
            self.qtd_baterias_substituicao = self.qtd_bateria_ano_substituicao.get(ano)
        if qtd_bateria > 0 or self.qtd_baterias_substituicao > 0:
            self.qtd_bateria_ano_substituicao[
                ano + Bateria.BATERIA_1.vida_util] = qtd_bateria + self.qtd_baterias_substituicao





