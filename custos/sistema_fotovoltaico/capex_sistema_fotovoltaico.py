from entidade.tipo_sistema_fotovoltaico import TipoSistemaFotovoltaico
from sistema_fotovoltaico.bateria import Bateria
from sistema_fotovoltaico.painel import Painel


class CAPEXFotovoltaico:

    def __init__(self, qtd_paineis, qtd_inversores, ano, inversor, tipo_sistema, qtd_baterias):
        self.qtd_paineis = qtd_paineis
        self.qtd_inversores = qtd_inversores
        self.ano = ano
        self.inversor = inversor
        self.capex = 0
        #dissertação Marcela
        self.taxa_depreciacao_financeira = 0.95
        self.taxa_de_instalacao = 0.1
        self.tipo_sistema = tipo_sistema
        self.qtd_baterias = qtd_baterias

    def calcula_capex(self):
        temp = self.taxa_depreciacao_financeira ** self.ano
        if temp < 0.6:
            temp = 0.6
        preco_inversor = self.inversor.value[1]
        #equação 24
        custo_equipamentos = temp * ((self.qtd_paineis *
                                      (Painel.PAINEL_320W.preco + Painel.PAINEL_320W.custo_instalacao_kit)) +
                                            self.qtd_inversores * preco_inversor) + self.baterias()
        #equação 23
        self.capex = (1 + self.taxa_de_instalacao) * custo_equipamentos

        return self.capex

    def baterias(self):
        if self.tipo_sistema == TipoSistemaFotovoltaico.OFFGRID.name:
            return self.qtd_baterias * Bateria.BATERIA_1.preco
        else:
            return 0
