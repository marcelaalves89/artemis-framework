from custos.npv.npv import NPV


class CenarioNPV:

    def __init__(self, tempo_analise, municipio):
        self.tempo_analise = tempo_analise
        self.municipio = municipio

    def gera_npv(self):

        for key in self.municipio.cenarios:
            self.municipio.cenarios[key].npv[key] = NPV(self.municipio.cenarios[key], self.tempo_analise, False)

            # Calculo a Receita da Rede
            self.municipio.cenarios[key].npv[key].get_income()
            self.municipio.cenarios[key].npv[key].get_arpu()
            self.municipio.cenarios[key].npv[key].get_si()

            # Calculo o Fluxo de Caixa
            self.municipio.cenarios[key].npv[key].get_cf()
            # Calculo o Valor Presente Líquido (VPL/NPV) e Payback
            self.municipio.cenarios[key].npv[key].get_npv()
            #com sistema fotovoltaico
            self.municipio.cenarios[key].npv_sf[key] = NPV(self.municipio.cenarios[key], self.tempo_analise, True)
            # Calculo a Receita da Rede
            self.municipio.cenarios[key].npv_sf[key].get_income()
            self.municipio.cenarios[key].npv_sf[key].get_arpu()
            self.municipio.cenarios[key].npv_sf[key].get_si()
            # Calculo o Fluxo de Caixa
            self.municipio.cenarios[key].npv_sf[key].get_cf()
            # Calculo o Valor Presente Líquido (VPL/NPV) e Payback
            self.municipio.cenarios[key].npv_sf[key].get_npv()

            # Teste
            if key == 'BF+5GS':
                si = self.municipio.cenarios[key].npv_sf[key].si
                macro = self.municipio.cenarios[key].npv_sf['Macro']
                hetnet = self.municipio.cenarios[key].npv_sf['Hetnet']
                payback = si = self.municipio.cenarios[key].npv_sf[key].payback