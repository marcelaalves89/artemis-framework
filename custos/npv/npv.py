import numpy as np

from custos.npv.cf import CF
from util.util import Util


class NPV:

    def __init__(self, municipio, tempo_analise, sistema_fotovoltaico):
        self.tempo_analise = tempo_analise

        self.municipio = municipio
        self.assinaturas_gov = np.zeros(self.tempo_analise)
        self.assinaturas_usuarios = np.zeros(self.tempo_analise)

        # 2806
        self.income = np.zeros(self.tempo_analise)
        self.income_user = np.zeros(self.tempo_analise)

        self.arpu = np.zeros(self.tempo_analise)
        self.si = np.zeros(self.tempo_analise)

        self.tco = dict(Macro=np.zeros(self.tempo_analise), Hetnet=np.zeros(self.tempo_analise))
        self.cf = dict(Macro=np.zeros(self.tempo_analise), Hetnet=np.zeros(self.tempo_analise))
        self.cf_sf = dict(Macro=np.zeros(self.tempo_analise), Hetnet=np.zeros(self.tempo_analise))

        if sistema_fotovoltaico:
            self.municipio.npv_sf = dict(Macro=np.zeros(self.tempo_analise), Hetnet=np.zeros(self.tempo_analise))
        else:
            self.municipio.npv = dict(Macro=np.zeros(self.tempo_analise), Hetnet=np.zeros(self.tempo_analise))

        self.payback = dict(Macro=0.0, Hetnet=0.0)
        self.tipos_rede_radio = ['Macro', 'Hetnet']
        self.sistema_fotovoltaico = sistema_fotovoltaico


    def get_income(self):
        for ag in self.municipio.agrupamentos_territoriais:
            # Calcula o quantitativo de Termianais Gov
            self.assinaturas_gov += ag.total_terminais

            # Calcula o quantitativo de Termianais de Usuários
            self.assinaturas_usuarios += ag.densidade_usuarios * ag.area

        # 12.0 = 12 meses
        # 0.56 = 56% da população é considerada ativa
        self.income = self.assinaturas_usuarios * CF.TAXA_SUBSCRICAO_USUARIO.valor * 12.0 * 0.56
        self.income_user = 0.5 * (self.assinaturas_usuarios * CF.TAXA_SUBSCRICAO_USUARIO.valor * 12.0 * 0.56)

        self.income += self.assinaturas_gov * CF.TAXA_SUBSCRICAO_GOV.valor * 12.0

        self.income *= 0.5

    def get_arpu(self):
        self.arpu = self.income_user/self.assinaturas_usuarios

    def get_si(self):
        # 0.00095 Renda Media Per Capita
        temp = self.arpu / 0.00095
        self.si = temp

    def get_cf(self):
        capex = np.zeros(self.tempo_analise)
        opex = np.zeros(self.tempo_analise)

        for ag in self.municipio.agrupamentos_territoriais:

            # Recupera CAPEX e OPEX do Aglomerado Macro
            capex += ag.capex_macro['Radio']['infraestrutura']
            capex += ag.capex_macro['Radio']['equipamentos']
            capex += ag.capex_macro['Radio']['instalacao']
            capex += ag.capex_macro['Transporte']['infraestrutura']
            capex += ag.capex_macro['Transporte']['equipamentos']
            capex += ag.capex_macro['Transporte']['instalacao']

            if self.sistema_fotovoltaico:
                capex_fotovoltaico_m = ag.capex_macro['Fotovoltaico']
                capex += list(capex_fotovoltaico_m.values())
                custo_energia_minima_rede_m = ag.custo_energia_minima_rede['macro']
                custo_aluguel_fotovoltaico_m = ag.custo_aluguel_fotovoltaico['macro']
                opex_fotovoltaico_m = ag.opex_macro['Fotovoltaico']
                opex += custo_energia_minima_rede_m
                opex += custo_aluguel_fotovoltaico_m
                opex_fv = Util().subtracao_lista(list(opex_fotovoltaico_m.values()), custo_energia_minima_rede_m)
                opex += Util().subtracao_lista(opex_fv, custo_aluguel_fotovoltaico_m)
            else:
                opex += ag.opex_macro['Radio']['energia']
                opex += ag.opex_macro['Transporte']['energia']

            opex += ag.opex_macro['Radio']['manutencao']
            opex += ag.opex_macro['Radio']['aluguel']
            opex += ag.opex_macro['Radio']['falhas']
            opex += ag.opex_macro['Transporte']['manutencao']
            opex += ag.opex_macro['Transporte']['aluguel']
            opex += ag.opex_macro['Transporte']['falhas']

        capex += self.municipio.capex_co['infraestrutura']
        capex += self.municipio.capex_co['equipamentos']
        capex += self.municipio.capex_co['instalacao']

        if self.sistema_fotovoltaico:
            capex += list(self.municipio.capex_fotovoltaico_co.values())
            opex += self.municipio.custo_energia_minima_rede_co
            opex += Util().subtracao_lista(list(self.municipio.opex_fotovoltaico_co.values()), self.municipio.custo_energia_minima_rede_co)
        else:
            opex += self.municipio.opex_co['energia']

        opex += self.municipio.opex_co['manutencao']
        opex += self.municipio.opex_co['aluguel']
        opex += self.municipio.opex_co['falhas']

        self.cf['Macro'] = self.income - (capex + opex)
        self.tco['Macro'] += (capex + opex)

        capex = np.zeros(self.tempo_analise)
        opex = np.zeros(self.tempo_analise)

        for ag in self.municipio.agrupamentos_territoriais:

            # Recupera CAPEX e OPEX do Aglomerado Hetnet
            capex += ag.capex_hetnet['Radio']['infraestrutura']
            capex += ag.capex_hetnet['Radio']['equipamentos']
            capex += ag.capex_hetnet['Radio']['instalacao']
            capex += ag.capex_hetnet['Transporte']['infraestrutura']
            capex += ag.capex_hetnet['Transporte']['equipamentos']
            capex += ag.capex_hetnet['Transporte']['instalacao']

            if self.sistema_fotovoltaico:
                capex_fotovoltaico_h = ag.capex_macro['Fotovoltaico']
                capex += list(capex_fotovoltaico_h.values())
                custo_energia_minima_rede_h = ag.custo_energia_minima_rede['hetnet']
                custo_aluguel_fotovoltaico_h = ag.custo_aluguel_fotovoltaico['hetnet']
                opex_fotovoltaico_h = ag.opex_hetnet['Fotovoltaico']
                opex += custo_energia_minima_rede_h
                opex += custo_aluguel_fotovoltaico_h
                opex_fv = Util().subtracao_lista(list(opex_fotovoltaico_h.values()), custo_energia_minima_rede_h)
                opex += Util().subtracao_lista(opex_fv, custo_aluguel_fotovoltaico_h)
            else:
                opex += ag.opex_hetnet['Radio']['energia']
                opex += ag.opex_hetnet['Transporte']['energia']

            opex += ag.opex_hetnet['Radio']['manutencao']
            opex += ag.opex_hetnet['Radio']['aluguel']
            opex += ag.opex_hetnet['Radio']['falhas']
            opex += ag.opex_hetnet['Transporte']['aluguel']
            opex += ag.opex_hetnet['Transporte']['falhas']

        capex += self.municipio.capex_co['infraestrutura']
        capex += self.municipio.capex_co['equipamentos']
        capex += self.municipio.capex_co['instalacao']

        # 2806
        opex += self.municipio.opex_co['energia']
        opex += self.municipio.opex_co['manutencao']
        opex += self.municipio.opex_co['aluguel']
        opex += self.municipio.opex_co['falhas']

        self.cf['Hetnet'] = self.income - (capex + opex)
        self.tco['Hetnet'] += (capex + opex)

        print('TCO:')
        print('Macro:')
        print(self.tco['Macro'])
        print(self.tco['Macro'].sum())
        print('CF:')
        print('Macro:')
        print(self.cf['Macro'])

        print('TCO:')
        print('Hetnet:')
        print(self.tco['Hetnet'])
        print(self.tco['Hetnet'].sum())
        print('CF:')
        print('Hetnet:')
        print(self.cf['Hetnet'])

    def get_npv(self):
        tipos_rede_radio = ['Macro', 'Hetnet']

        print('NPV')
        for tipo in tipos_rede_radio:
            npv = np.zeros(self.tempo_analise)
            for ano, cf_ano in enumerate(self.cf[tipo]):
                tma = (1 + CF.TAXA_DESCONTO.valor) ** ano
                npv[ano] += (cf_ano / tma)

            for ano, npv_ano in enumerate(npv):
                if npv_ano > 0:
                    self.payback[tipo] = ano
                    break

            if self.sistema_fotovoltaico:
                self.municipio.npv_sf[tipo] = npv.sum()
                print('Implantação {}:'.format(tipo))
                print(self.municipio.npv_sf[tipo])
                print()
            else:
                self.municipio.npv[tipo] = npv.sum()

                print('Implantação {}:'.format(tipo))
                print(self.municipio.npv[tipo])
                print()
