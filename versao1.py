# -*- coding: utf-8 -*-
"""versao1.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1bzvzKqmOpJ5hLkAu4ZAu9nUxk9Qd4Zxl
"""

class AgrupamentoTerritorial(object):
  
    def __init__(self, id, area, densidadePopulacional, demandaTrafego, temEnergia, temRedeMovel):
        self.id = id
        self.area = area
        self.densidadePopulacional = densidadePopulacional
        self.demandaTrafego = demandaTrafego
        self.temEnergia = temEnergia
        self.temRedeMovel = temRedeMovel
    
    @property
    def idSedeMunicipioOrigem(self):
        return self._idSedeMunicipioOrigem
    
    @idSedeMunicipioOrigem.setter
    def idSedeMunicipioOrigem(self, idSedeMunicipioOrigem):
        self._idSedeMunicipioOrigem = idSedeMunicipioOrigem
    
    @property
    def sedeMunicipio(self): 
        return self._sedeMunicipio
      
    @sedeMunicipio.setter
    def sedeMunicipio(self, sedeMunicipio):
        self._sedeMunicipio = sedeMunicipio
        
    @property
    def latitude(self): 
        return self._latitude
      
    @latitude.setter
    def latitude(self, latitude):
        self._latitude = latitude
    
    @property
    def longitude(self): 
        return self._longitude
      
    @longitude.setter
    def longitude(self, longitude):
        self._longitude = longitude
    
    @property
    def tipo(self): 
        return self._tipo
      
    @tipo.setter
    def tipo(self, tipo):
        self._tipo = tipo
        
    @property
    def vizinhos(self): 
        return self._vizinhos
      
    @vizinhos.setter
    def vizinhos(self, vizinhos):
        self._vizinhos = vizinhos
    
    def __repr__(self):
        return str(self.__dict__)


    
class Vizinho(object): 
  
    def __init__(self, id, tipoPercursoAcesso, tipoInfraExistente, distancia):
        self.id= id
        self.tipoPercursoAcesso = tipoPercursoAcesso
        self.tipoInfraExistente = tipoInfraExistente
        self.distancia = distancia
    
    def __repr__(self):
        return str(self.__dict__)


class Conexao(object):
   def __init__(self, idOrigem, idDestino, custo):
        self.idOrigem = idOrigem
        self.idDestino = idDestino
        self.custo = custo
    
   def __repr__(self):
        return str(self.__dict__)


import math
import random
from random import randint
from random import uniform


class Cenario:
  
    def criaAgrupamentosTerritoriais(idSedeMunicipioOrigem, idAgrupamentoInicial, sedeMunicipio):
        #gera distribuição gaussiana
        areasAgrupamentosTerritoriais = np.random.normal(0,1,5)
        
        listaAgrupamentosTerritoriais = []
       
        contador=0
      
        while ( contador < len(areasAgrupamentosTerritoriais)):
            # cria o id_agrupamento territorial,
            # a área corresponde ao valor absoluto da distribuiçãão gaussiana
            agupamentoTerritorial = AgrupamentoTerritorial(idAgrupamentoInicial+1, 
                                                           abs(areasAgrupamentosTerritoriais[contador]), 
                                                           Cenario.geraDensidadePopulacinal(), Cenario.geraDemandaTrafego(), 
                                                           Cenario.temEnergia(), Cenario.temInfraRede())
            #propriedade que define o CI (sede do município)
            agupamentoTerritorial.sedeMunicipio = sedeMunicipio
            #CI ao qual o id_agrupamento pertence
            agupamentoTerritorial.idSedeMunicipioOrigem = idSedeMunicipioOrigem
            #gera o tipo de id_agrupamento (Tabela 3) através de uma distribuição exponencial
            agupamentoTerritorial.tipoAgrupamento = Cenario.geraTipoAgrupamento()

            #coordenadas para geração da distância a ser usada no cálculo do custo
            coordenadas = Cenario.coordenadas(idAgrupamentoInicial)
            agupamentoTerritorial.latitude = coordenadas[0]
            agupamentoTerritorial.longitude = coordenadas[1]
          
            listaAgrupamentosTerritoriais.append(agupamentoTerritorial)
            contador += 1
            idAgrupamentoInicial+=1
            #há apenas um CI por lista de agrupamentos territorial
            sedeMunicipio = False
            
        return listaAgrupamentosTerritoriais, idAgrupamentoInicial
    
    
    def geraVizinhos(listaAgrupamentosTerritoriais):
      quantidadeAgrupamentos = len(listaAgrupamentosTerritoriais)

      listaTempAgrupamentos = Cenario.geraDicionarioAgrupamentos(listaAgrupamentosTerritoriais)
    
      for i in listaTempAgrupamentos.values():
        vizinhos = []
        #quantidade de caminhos aleatóória que cada id_agrupamento terá
        quantidadeVizinhos = randint(1, quantidadeAgrupamentos)
        #listaAleatoriosSemRepeticao = []
        # #gera uma lista de possíveis caminhos, sem repetição
        listaAleatoriosSemRepeticao = Cenario.geraAleatoriosSemRepeticao(quantidadeAgrupamentos,quantidadeVizinhos, i.id)
        x=0
        #
        while (x <= quantidadeVizinhos-1): 
          #primeiro nó vizinho
          id = listaAleatoriosSemRepeticao[x]

           #recupero o id_agrupamento correspondente ao id
          agrupamentoTemp =  listaTempAgrupamentos[id]
 
          #verifico se o id_agrupamento vizinho é o mesmo da origem
          if(agrupamentoTemp.id == i.id):
             if quantidadeVizinhos > 1:
                break
             else:
                agrupamentoTemp =  Cenario.verificaRepeticao(quantidadeVizinhos, i.id, quantidadeAgrupamentos, x, listaTempAgrupamentos)

          latitudeLongitudeOrigem = (i.latitude, i.longitude)
          latitudeLongitudeDestino = (agrupamentoTemp.latitude, agrupamentoTemp.longitude)
          #distancia entre dois vizinhos
          distancia = Cenario.distancia(latitudeLongitudeOrigem, latitudeLongitudeDestino )
          #gero o vizinho dos agrupamentos
          vizinho = Vizinho(agrupamentoTemp.id, Cenario.geraPercurso(), Cenario.geraInfra(), distancia)
          vizinhos.append(vizinho)
          x += 1
      
        i.vizinhos = vizinhos
        
      return  listaAgrupamentosTerritoriais

    def verificaRepeticao(quantidadeVizinhos, idAgrupamento, quantidadeAgrupamentos, posicao, listaTempAgrupamentos):
        lista2 = Cenario.geraAleatoriosSemRepeticao(quantidadeAgrupamentos, quantidadeVizinhos, idAgrupamento)
        id2 = lista2[posicao]
        agrupamentoTemp =  listaTempAgrupamentos[id2]
        if(agrupamentoTemp.id == idAgrupamento):
             verificaRepeticao(quantidadeVizinhos, idAgrupamento, quantidadeAgrupamentos)
        return agrupamentoTemp

    #tranformei a lista em dicionário para ficar melhor de usar, precisa melhorar    
    def geraDicionarioAgrupamentos(listaAgrupamentosTerritoriais):
      d={}
      index=1
      for i in listaAgrupamentosTerritoriais:
        d[index] = i
        index +=1
      #print (d) 
      return d
    
    #temporarias
    def coordenadas(posicao):
      coordenadas=[ [6.79053,50.4791], 
                    [1.94623,54.7384], 
                    [1.72183,48.8788],
                    [1.52904,52.5788],
                    [3.20407,52.2100],
                    [0.996811,49.9354],
                    [1.36391,48.3743],
                    [3.46985,51.2003],
                    [1.05109,46.6147],
                    [2.14898,47.5677],
                    [3.60841, 55.3199],#novos
                    [1.90057, 50.1987],
                    [2.79021, 49.6694],
                    [7.34779, 50.3959],
                    [1.51187, 48.6195],
                    [1.4554,  48.4898],
                    [2.63609, 54.9374],
                    [1.36183, 48.2434],
                    [5.0424,  48.6047],
                    [3.0424,  48.6048]
                   ]
      return coordenadas[posicao]
      
    #gera uma lista sem repetição    
    def geraAleatoriosSemRepeticao(intervalo, tamanhoLista, idAgrupamento):
      amostras=[]
      valor=0
      while(len(amostras) < tamanhoLista):
        valor=randint(1, intervalo)
        if valor in amostras:
          continue
        else:
          amostras.append(valor)

      return amostras

    #Tabela 1
    def geraPercurso():
       return randint(1, 4)

    #Tabela 2
    def geraInfra():
     return  randint(0, 3)     

    #gera um número aleatório entre 1 e 1000   
    def geraDensidadePopulacinal(): 
        return uniform(1, 1000)

    #gera um número aleatório entre 1 e 1000     
    def geraDemandaTrafego():
        return uniform(1, 1000)

    #gera aleatoriamente True ou False como resultado    
    def temEnergia():
        return random.choice([True, False])

    #gera aleatoriamente True ou False como resultado      
    def temInfraRede():
        return random.choice([True, False])

    #calcula a distância baseado na latitude/longitude origem e destino
    def distancia(origem, destino):
      lat1, lon1 = origem
      lat2, lon2 = destino
      radius = 6371 # km

      dlat = math.radians(lat2-lat1)
      dlon = math.radians(lon2-lon1)

      a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
      c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
      d = radius * c

      return d
    
    #Tabela 3
    def geraTipoAgrupamento():
        #distribuição exponencial para gerar o perfil do id_agrupamento
        distribuicao = np.random.exponential(7,7) 
        
        tipoAgrupamento = 0

        for x in distribuicao:
         # print (i)
          if x <= 1:
            tipoAgrupamento = 1
          elif x > 1 and x <= 2:
            tipoAgrupamento = 2
          elif x > 2 and x <= 3:
            tipoAgrupamento = 3
          elif x > 3 and x <= 4:
            tipoAgrupamento = 4
          elif x > 5 and x <= 6:
            tipoAgrupamento = 5
          elif x > 6 and x <= 7:
            tipoAgrupamento = 6
          elif x > 6:
            tipoAgrupamento = 7
          else:
            tipoAgrupamento = 0

        return tipoAgrupamento

    #calcula custo 
    def ponderacaoCustoDistancia(distancia, percurso, infra):
      #w=d*2^r*2^s
      w = distancia * (2 ** percurso) * (2 ** infra)
      return w
      

import matplotlib.pyplot as plt
from networkx import nx


class Grafo:
  
  def visualizaGrafoArvoreGeradora(grafo, idSedeMunicipio):
    grafoOtimizado = Grafo.criaArvoreGeradoraMinima(grafo)
    Grafo.geraGrafo(grafo, 'Árvore Geradora Mínima  - Município' + str(idSedeMunicipio))
  
  def visualizaGrafoInicial(grafo, idSedeMunicipio):
    Grafo.geraGrafo(grafo, 'Grafo Inicial - Município ' + str(idSedeMunicipio))
    return grafo

  def geraGrafo(grafo, titulo):
    nx.draw(grafo)
    plt.title(titulo,size=16)
    plt.show()

  #calcula e adiciona o custo entre um nó e outro
  def adicionaPeso(listaAgrupamentosTerrioriais):
     grafo = nx.Graph()
     for i in listaAgrupamentosTerrioriais:
      #cria arestas com peso
      for vizinho in i.vizinhos:
        custo = Grafo.calculaPeso(vizinho.distancia, vizinho.tipoPercursoAcesso, vizinho.tipoInfraExistente)
        #redução de casas decimais
        custo = round(custo, 2)
        grafo.add_edge(i.id, vizinho.id, weight=custo)

     return grafo

  #fórmula wpq
  def calculaPeso(distancia, tipoPercurso, tipoInfra):
    peso = Cenario.ponderacaoCustoDistancia(distancia, tipoPercurso, tipoInfra)
    return peso

  def criaArvoreGeradoraMinima(grafo):
    #gera o menor caminho
    mst = nx.minimum_spanning_tree(grafo)

    grafoOtimizado = nx.Graph()
    #gero um outro grafo com os novos caminhos
    for (u, v, d) in mst.edges(data=True):
      grafoOtimizado.add_edge(u,v,weight=d['weight'])
    return grafoOtimizado
  
  def candidatosPoda(grafo):
    #candidatos são nós que não possuem folha
    candidatos = {}
    for (u, v, d) in grafo.edges(data=True):
      if grafo.degree[u] <=1:
        #print( grafo.degree[u])
       # print(grafo.adj[u])
        candidatos[u]=d['weight']
    
    #print(candidatos)
    return candidatos
  
  def gerarAgrupamentosPoda(possiveisPodas, agrupamentosSede):

    agrupamentoASerPodado=[]
    # as possíveis podas são os nós sem folha das duas sedes de município
    for chave, valor in possiveisPodas.items():
       if chave + 1 <= len(agrupamentosSede):
         primeiroAgrupamento = agrupamentosSede[chave]
         segundoAgrupamento = agrupamentosSede[chave + 1]
        # a chave é o id da sede
        #o valor armazena as possíveis podas de determinanda sede, por exemplo {idAgrupamento: peso, idAgrupamento: peso }
         for idAresta, custoOrigem in valor.items():
           
           origem = Grafo.recuperaDadosAgrupamentoOrigem(primeiroAgrupamento,idAresta)
          # reposável por agrupar temporariamente os agrupamentos que possui um peso/custo menor que na Origem
           agrupamentoTemporario=[]
          
           latitudeLongitudeOrigem = (origem.latitude, origem.longitude)

           for t in segundoAgrupamento:
          
             latitudeLongitudeDestino = (t.latitude, t.longitude)
             #gera distância, percurso e infra em relação ao id_agrupamento da sede vizinha
             distancia = Cenario.distancia(latitudeLongitudeOrigem, latitudeLongitudeDestino )
             #gerados aleatoriamente
             percurso = Cenario.geraPercurso()
             infra = Cenario.geraInfra()

             custoAtual = Grafo.calculaPeso(distancia, percurso, infra)
             
             if custoAtual < custoOrigem:
                agrupamentoTemporario =  Conexao(origem.id, t.id, custoAtual )
           
           if agrupamentoTemporario != []:
             agrupamentoASerPodado.append(agrupamentoTemporario)

    return agrupamentoASerPodado

  #remove caminhos do grafo   
  def poda(grafo, podas):
    remover_itens = []
    for i in podas:
        remover_itens.append(i)

    for item in remover_itens:
      grafo.remove_node(item.idOrigem)
      
    Grafo.geraGrafo(grafo, 'Grafo com Árvore Geradora Mínima Podado - Município 1')
    return grafo

  #adiciona arestas a um determinado grafo
  def adicaoNo(grafo, nos):
    # adiciona o nó que foi podado no id_agrupamento anterior ao agrupmento do próximo CI
    for no in nos:
      grafo.add_edge(no.idOrigem, no.idDestino, weight=no.custo)

    #gera grafo com o nóó adicionado
    Grafo.geraGrafo(grafo, 'Grafo com Árvore Geradora Mínima com adição nós - Município 2')
    return grafo

  #recupera id_agrupamento respectivo ao id do grafo passado
  def recuperaDadosAgrupamentoOrigem(agrupamentos, idAgrupamentoOrigem):
     for agrupamento in agrupamentos:
        if agrupamento.id == idAgrupamentoOrigem:
          return agrupamento;
   
  def exibirInformacoesGrafo(grafosNaoOtimizado, grafosOtm):

        print('Informações dos Grafos Gerados')
        print('\n')
        print('Grafo Inicial Município 1')
        #nós, arestas e pesos grafos
        for (u, v, d) in grafosNaoOtimizado[1].edges(data=True):
           print(u,v,d)
        print('\n')

        print('Grafo com Árvore Geradora Mínima Município 1')
        for (u, v, d) in grafosOtm[1].edges(data=True):
           print(u,v,d)
        print('\n')

        print('Grafo Inicial Município 2')
        for (u, v, d) in grafosNaoOtimizado[2].edges(data=True):
           print(u,v,d)
        print('\n')

        print('Grafo com Árvore Geradora Mínima Município 2')
        for (u, v, d) in grafosOtm[2].edges(data=True):
           print(u,v,d)
        print('\n')    
import numpy as np 
class Main:
  
    if __name__ == '__main__':

        quantidadeSedeMunicipio = 2;
        idSedeMunicipio = 1;
        idAgrupamento = 0

        agrupamentosSede = {}
        possiveisPodas = {}
        grafosOtm = {}
        grafosNaoOtimizado = {}
        podas = {}

        sedeMunicipio = True
        #gera os agrupamentos por sede
        while idSedeMunicipio <= quantidadeSedeMunicipio:
          agrupamentos = []
          #cria a sede e seus agrupamentos
          agrupamentos, idAgrupamento = Cenario.criaAgrupamentosTerritoriais(idSedeMunicipio, idAgrupamento, sedeMunicipio)
          #gera o relacionamento entre agrupamentos
          Cenario.geraVizinhos(agrupamentos)
          #adiona o custo entre cada caminho
          grafoComPeso = Grafo.adicionaPeso(agrupamentos)
          
          grafoInicial = Grafo.visualizaGrafoInicial(grafoComPeso, idSedeMunicipio)
          
          grafoOtimizado = Grafo.criaArvoreGeradoraMinima(grafoInicial)
        
          Grafo.visualizaGrafoArvoreGeradora(grafoOtimizado, idSedeMunicipio)

          possiveisPodas[idSedeMunicipio] = Grafo.candidatosPoda(grafoOtimizado)

          #armazenar listas para a poda e exibição dos resultados
          agrupamentosSede[idSedeMunicipio] = agrupamentos
          grafosOtm[ idSedeMunicipio] = grafoOtimizado
          grafosNaoOtimizado[idSedeMunicipio] = grafoInicial

          idSedeMunicipio+=1
        #exibir informações Grafo Incial e Árvore Geradora
        Grafo.exibirInformacoesGrafo(grafosNaoOtimizado, grafosOtm)

        # se houver candidatos a serem podados realiza-se o passo a passo da poda: 
        # confirmar o se o candidato será podado, realizar a poda e adicionar o nó no próximo id_agrupamento sede
        if len(possiveisPodas) > 0:
          agrupamentosPoda = Grafo.gerarAgrupamentosPoda(possiveisPodas, agrupamentosSede)
          if len(agrupamentosPoda) > 0:
            grafoOtmAposPoda = Grafo.poda(grafosOtm[1], agrupamentosPoda)
            grafoComNoAdicionado = Grafo.adicaoNo(grafosOtm[2], agrupamentosPoda)
            #exibir informações

            print('Grafo após a Poda Município 1')
            for (u, v, d) in grafoOtmAposPoda.edges(data=True):
              print(u,v,d)
            print('\n')

            print('Grafo com Adição Nó Município 2')
            for (u, v, d) in grafoComNoAdicionado.edges(data=True):
              print(u,v,d)
            print('\n')
        
        #Município 1: id de 1 a 10
        print('Agrupamentos Município 1')
        print(agrupamentosSede[1])
        print('\n')
        #Município 2: id de 11 a 20
        print('Agrupamentos Município 2')
        print(agrupamentosSede[2])
        print('\n')
         #Reunião 17/10/2019
        # 1 - Adicionar Comentários ok
        # 2 - Verificar a adição de mais caminhos para uma mesma conexao ok
          # Em testes realizados constatou-se que embora se adicione os dois caminhos no id_agrupamento, no momento da geração do grafo
          # a biblioteca permite apenas 1, sobrescrevendo com o último peso adicionado. 
          # A minha sugestão seria gerar grafos separados para cada tipo de percurso, infra
       
        # 3 - exibir informações do grafo não otimizado e otimizado ok 
        
        #melhorar o código +ou -



"""italicized text"""







