import numpy as np
import math
from custos.radio_transporte.tco_co import TcoCO


class CenarioCentralOffice:

    def __init__(self, tempo_analise, municipio):
        self.tempo_analise = tempo_analise
        self.sw_agregacao = np.zeros(tempo_analise)
        self.antenas_pt_pt = np.zeros(tempo_analise)
        self.municipio = municipio
        self.tco_co = dict()
        self.municipio.co.qtd_fs = np.zeros(tempo_analise)

    def carrega_central_office(self):
        for key in self.municipio.cenarios:
            self.calcula_dimensionamento_centraloffice(self.municipio.cenarios[key])
            self.tco_co[key] = TcoCO(self.municipio.cenarios[key], self.tempo_analise)
            self.tco_co[key].get_tco()

    def calcula_dimensionamento_centraloffice(self, municipio):
        capacidade_atendimento_vs = 800
        print('Dimensionamento de Central Office')
        demanda_trafego_total = np.zeros(self.tempo_analise)

        # Calcula a quantidade de servidores de rede pra rodar as funções de SDN
        vs_por_ano = np.zeros(self.tempo_analise)
        for ag in municipio.agrupamentos_territoriais:
            demanda_trafego_total += ag.demanda_trafego_por_area * ag.area

        # A partir da demanda de tráfego total do município é necessário estimar a infraestrutura do micro datacenter
        # Assumindo a capacidade de atendimento de 800 Mbps por servidor
        for ano, demanda_ano in enumerate(demanda_trafego_total):
            if ano == 0:
                vs_por_ano[ano] = math.ceil(demanda_ano/capacidade_atendimento_vs)
            else:
                vs_por_ano[ano] = math.ceil(demanda_ano/capacidade_atendimento_vs) \
                                                                 - sum(vs_por_ano[:ano])

        # 2806
        print('Quantidade de Servidores Virtuais por Ano: ')
        print(vs_por_ano)

        # 2806
        n_vs_por_fs = 10
        soma = 0
        fs_por_ano = np.zeros(self.tempo_analise)
        for t, n_vs in enumerate(vs_por_ano):
            soma += n_vs
            if t == 0:
                fs_por_ano[t] = 1
            elif soma % n_vs_por_fs == 0:
                fs_por_ano[t] = 1

        # 2806
        print('Quantidade de Servidores Físicos por Ano:')
        print(fs_por_ano)

        '''
        Calcula a quantidade de switches de agregação na opção de transporte por microwave. 
        O total de switches é proporcional ao número de nós não folha, isto é, a qtd de aglomerados que conectam 
        diretamente na sede. Assumindo mais uma vez a topologia Sede -> Ubim -> Nova Maracanã temos apenas 01 aglomerado
        "Nao Folha". Vamos considerar que cada SW de Agregação possui 12 Portas.
        O cálculo é em função da AGM
        '''

        qtd_portas_sw_agregacao = 12
        qtd_aglomerados_nao_folha = 1 # A partir do pressuposto acima, esse aglomerado seria Ubim

        # Na equação abaixo é somado + 1 para considerar a porta utilizada pelas antenas agregadas da Sede
        # Considerar a variável qt_sw_agregacao como atributo da classe Municipio
        qt_sw_agregacao = math.ceil( (qtd_aglomerados_nao_folha + 1)/qtd_portas_sw_agregacao )

        # Todos os SW são implantados no Ano 0, pq dependem da topologia da AGM e não do quantitativo de BS
        # Assim o mesmo valor ou array (self.sw_agregacao) deve ser considerado para as opções de Macro Only e Hetnet
        self.sw_agregacao[0] = qt_sw_agregacao

        print('Quantidade de SW de Agregação MW:')
        print(self.sw_agregacao)

        '''
        Cada servidor físico ocupa cerca de 2U.
        Cada switch ocupa 1U
        Cada uDC possui espaco para 42U, e desta forma o total de uDC no CO fica proporcional ao número de equipamentos 
        '''
        espaco_fs = 2
        espaco_as = 1
        espaco_total_uDC = 42

        espaco_uDC = fs_por_ano * espaco_fs + self.sw_agregacao * espaco_as

        soma_udc = 0
        udc_por_ano = np.zeros(self.tempo_analise)
        for t, espaco_ano in enumerate(espaco_uDC):
            soma_udc += espaco_ano
            if t == 0:
                udc_por_ano[t] = 1
            elif soma_udc % espaco_total_uDC == 0:
                udc_por_ano[t] = 1

        print('Quantidade de uDC no CO:')
        print(udc_por_ano)

        municipio.co.qtd_as = self.sw_agregacao
        municipio.co.qtd_fs = fs_por_ano
        municipio.co.qtd_vs = vs_por_ano
        municipio.co.qtd_udc = udc_por_ano

