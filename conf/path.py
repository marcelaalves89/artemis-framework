import os
from enum import Enum

class Path(Enum):

    ARQUIVO_MUNICIPIO = ('', 'diretorio', os.path.abspath('../data/municipios/municipios.csv'))
    ARQUIVO_AGRUPAMENTO = ('', 'diretorio', os.path.abspath('../data/municipios/agrupamentos_municipio.csv'))
    ARQUIVO_SETORES= ('', 'diretorio',os.path.abspath('../data/municipios/setores_censitarios_municipio.csv'))
    ARQUIVO_PONTOS_RUA= ('', 'diretorio',os.path.abspath('../data/municipios/pontos_rua_agrupamento.csv'))
    ARQUIVO_ESTRADAS = ('', 'diretorio',os.path.abspath('../data/municipios/pontos_estradas_municipio.csv'))
    ARQUIVO_BS = ('', 'diretorio',os.path.abspath('../data/municipios/BS.csv'))
    ARQUIVO_APLICACOES = ('', 'diretorio',os.path.abspath('../data/municipios/aplicacoes.csv'))

    FIGURA_CENARIO = ('', 'diretorio',os.path.abspath('/output/figuras/cenario'))
    FIGURA_DEMANDA_TRAFEGO = ('', 'diretorio',os.path.abspath('/output/figuras/demanda_trafego'))
    
    MAPA = ('', 'diretorio',os.path.abspath('../output/mapas/municipios.html'))

    DIRETORIO_IMAGEM_RADIO = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/radio/')
    DIRETORIO_IMAGEM_TRANSPORTE = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/transporte/')
    DIRETORIO_IMAGEM_SISTEMA_FOTOVOLTAICO = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/sistema_fotovoltaico/')
    DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/radio_transporte_fotovoltaico/')
    DIRETORIO_IMAGEM_MUNICIPIO = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/radio_transporte_fotovoltaico/municipio/')
    DIRETORIO_IMAGEM_NPV = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/radio_transporte_fotovoltaico/municipio/')
    DIRETORIO_IMAGEM_FC = ('Diretório Export Gráficos', 'diretorio', '../output/figuras/custos/radio_transporte_fotovoltaico/municipio/')




    RESOLUCAO_IMAGEM = ('Resolução Export Gráficos', 'dpi', 600)

    def __init__(self, descricao, unidade_medida, valor):
        self.descricao = descricao
        self.unidade_medida = unidade_medida
        self.valor = valor

    def __str__(self):
        return "Param descricao={}, unidade_medida={}, valor={}".format(self.descricao, self.unidade_medida, self.valor)

