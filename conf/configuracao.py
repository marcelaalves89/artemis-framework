from conf.parametro import Parametro


class Configuracao:


    def __init__(self, media_area_sede=20.0, desvio_area_sede=10.0,numero_operadoras_sede=4):
        self._media_area_sede = media_area_sede
        self._desvio_area_sede = desvio_area_sede
        self._numero_operadoras_sede = numero_operadoras_sede

    def set(self, instance, value):
        if instance == Parametro.MEDIA_AREA_SEDE:
            self.media_area_sede = value
        elif instance == Parametro.DESVIO_AREA_SEDE:
            self.desvio_area_sede = value
        elif instance == Parametro.QUANTIDADE_MUNICIPIO:
            self._quantidade_municipio = value
        elif instance ==Parametro.PATH_ARQUIVO_MUNICIPIO:
            self._path_arquivo_municipio = value
        elif instance == Parametro.FIGURA_GEOMETRICA_POLYGON:
            self._figura_geometrica_polygon = value
        elif instance == Parametro.PATH_ARQUIVO_AGRUPAMENTOS_MUNICIPIO:
            self._path_arquivo_agrupamentos_municipio = value
        elif instance == Parametro.PATH_ARQUIVO_SETORES_CENSITARIOS_MUNICIPIO:
            self._path_arquivo_setores_municipio = value
        elif instance == Parametro.PATH_ARQUIVO_PONTOS_RUA_AGRUPAMENTO:
            self._path_arquivo_pontos_rua_agrupamento = value
        elif instance == Parametro.PATH_ARQUIVO_ESTRADAS:
            self._path_arquivo_pontos_estradas_municipio = value
        elif instance == Parametro.PATH_ARQUIVO_APLICACOES:
            self._path_arquivo_aplicacoes = value
        elif instance == Parametro.TEMPO_ANALISE:
            self._tempo_analise= value
        else: 0

    @property
    def media_area_sede(self):
        return self._media_area_sede

    @property
    def desvio_area_sede(self):
        return self._desvio_area_sede

    @media_area_sede.setter
    def media_area_sede(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else: self._media_area_sede = value

    @desvio_area_sede.setter
    def desvio_area_sede(self, desvio_area_sede):
        if desvio_area_sede < 0:
            raise RuntimeError('[ERROR]: O parametro desvio_area_sede deve ser um valor positivo')
        else: self._desvio_area_sede = desvio_area_sede

    @property
    def quantidade_municipio(self):
        return self._quantidade_municipio

    @quantidade_municipio.setter
    def quantidade_municipio(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._quantidade_municipio = value

    @property
    def path_arquivo_municipio(self):
        return self._path_arquivo_municipio

    @path_arquivo_municipio.setter
    def path_arquivo_municipio(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._path_arquivo_municipio = value

    @property
    def figura_geometrica_polygon(self):
        return self._figura_geometrica_polygon

    @figura_geometrica_polygon.setter
    def figura_geometrica_polygon(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._figura_geometrica_polygon = value

    @property
    def path_arquivo_agrupamentos_municipio(self):
        return self._path_arquivo_agrupamentos_municipio

    @path_arquivo_agrupamentos_municipio.setter
    def path_arquivo_agrupamentos_municipio(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._path_arquivo_agrupamentos_municipio = value

    @property
    def path_arquivo_setores_municipio(self):
        return self._path_arquivo_setores_municipio

    @path_arquivo_setores_municipio.setter
    def path_arquivo_setores_municipio(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._path_arquivo_setores_municipio = value

    @property
    def path_arquivo_pontos_rua_agrupamento(self):
        return self._path_arquivo_pontos_rua_agrupamento

    @path_arquivo_pontos_rua_agrupamento.setter
    def path_arquivo_pontos_rua_agrupamento(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._path_arquivo_pontos_rua_agrupamento = value

    #
    @property
    def path_arquivo_pontos_estradas_municipio(self):
        return self._path_arquivo_pontos_estradas_municipio

    @path_arquivo_pontos_estradas_municipio.setter
    def path_arquivo_pontos_estradas_municipio(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._path_arquivo_pontos_estradas_municipio = value

    @property
    def tempo_analise(self):
        return self._tempo_analise

    @tempo_analise.setter
    def tempo_analise(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._tempo_analise = value

    @property
    def path_arquivo_aplicacoes(self):
        return self._path_arquivo_aplicacoes

    @path_arquivo_aplicacoes.setter
    def path_arquivo_aplicacoes(self, value):
        if value < 0:
            raise RuntimeError('[ERROR]: O parametro media_area_sede deve ser um valor positivo')
        else:
            self._path_arquivo_aplicacoes = value

    def __repr__(self):
        return str(self.__dict__)


