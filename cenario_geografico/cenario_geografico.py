from conf.configuracao import Configuracao
from conf.parametro import Parametro
from util.util import Util
import pandas as pd
from geopandas import GeoDataFrame
from entidade.estrada import Estrada
from entidade.agrupamento_territorial import AgrupamentoTerritorial
from conf.path import Path
from entidade.rua import Rua
from entidade.vizinho import Vizinho
from entidade.setor_censitario import SetorCensitario
from entidade.situacao_regiao import SituacaoRegiao
from entidade.tipo_ponto import TipoPonto
from entidade.tipo_regiao import TipoRegiao
from entidade.municipio import Municipio
from entidade.tipo_municipio import TipoMunicipio
import numpy as np

class CenarioGeografico:

        def __init__(self, mapa, grafo):
            self.grafo = grafo
            self.mapa = mapa
            self.configuracao = Configuracao()
            self.configuracao.set(Parametro.FIGURA_GEOMETRICA_POLYGON, 'POLYGON')

        def gera_municipio(self):
            header_csv = ['cod_estado', 'cod_municipio', 'nome', 'tipo_municipio', 'geometry',
                          'n_alunos', 'perc_ead', 'n_docentes', 'n_servidores_publicos',
                          'n_veiculos', 'n_servidores_publico_saude', 'porcentagem_pop_economicamente_ativa',
                          'tempo_viagem', 'tempo_medio_disponibilidade']
            df, gdf = self.recupera_dados_geograficos(Path.ARQUIVO_MUNICIPIO.valor, header_csv)

            municipios = []  # Lista de municipios

            for index, row in df.iterrows():  # itera nas linhas do arquivo, por ex: cada linha é um município
                coordenadas_poligono = Util().gera_poligono(gdf.geometry[index],
                                                            self.configuracao.figura_geometrica_polygon)
                area_municipio = Util().calcula_area_km(coordenadas_poligono)
                municipio = Municipio(row['cod_municipio'], row['nome'], area_municipio, row['tempo_viagem'], row['tempo_medio_disponibilidade'])
                municipio.cod_estado = row['cod_estado']
                municipio.coordenadas_geograficas = row['geometry']
                municipio.tipo_municipio = TipoMunicipio(row['tipo_municipio']).name
                municipio.coordenadas_geograficas_obj = coordenadas_poligono
                municipio.centroide = coordenadas_poligono.centroid
                # dados_socio_economicos
                municipio.dados_socio_economico.total_alunos = row['n_alunos']
                municipio.dados_socio_economico.percentual_alunos_ead = row['perc_ead']
                municipio.dados_socio_economico.total_docentes = row['n_docentes']
                municipio.dados_socio_economico.total_servidores_publicos = row['n_servidores_publicos']
                municipio.dados_socio_economico.total_servidores_publicos_saude = row['n_servidores_publico_saude']
                municipio.dados_socio_economico.total_veiculos = row['n_veiculos']
                municipio.dados_socio_economico.percentual_pop_ativa = row['porcentagem_pop_economicamente_ativa']
                municipio.dados_socio_economico.percentual_pop_inativa = 1.0 - municipio.dados_socio_economico.percentual_pop_ativa

                municipios.append(municipio)

            return municipios

        def popula_municipio(self, municipio):
            self.gera_agrupamentos(municipio)
            for agrupamento in municipio.agrupamentos_territoriais:
                self.popula_agrupamento(agrupamento)
            self.adiciona_estradas_municipios(municipio)
            self.mapa.adiciona(municipio, self.mapa.style_municipio())
            self.mapa.adiciona_lista(municipio.agrupamentos_territoriais, self.mapa.style_agrupamentos())
            self.exibe_info_geograficas(municipio)

        def popula_agrupamento(self, agrupamento):
            self.gera_setores_censitarios(agrupamento)
            self.mapa.adiciona_lista(agrupamento.setores_censitarios, self.mapa.style_setores())
            self.gera_ruas(agrupamento)
            self.gera_vizinhos_ruas(agrupamento)


        def gera_agrupamentos(self, municipio):
            header_csv = ['cod_municipio', 'cod_agrupamento', 'nome', 'tipo_regiao', 'situacao_regiao',
                          'possui_energia',
                          'geometry', 'vizinhos',
                          'n_agencias_bancarias', 'total_cruzamentos']
            df, gdf = self.recupera_dados_geograficos(Path.ARQUIVO_AGRUPAMENTO.valor, header_csv)
            agrupamentos = []
            for index, row in df.iterrows():
                if (row['cod_municipio'] == municipio.id_municipio):
                    # Salva o polígono como objeto
                    coordenadas_poligono = Util().gera_poligono(gdf.geometry[index],
                                                                self.configuracao.figura_geometrica_polygon)
                    area_agrupamento = Util().calcula_area_km(coordenadas_poligono)
                    agrupamento = AgrupamentoTerritorial(row['cod_agrupamento'], row['nome'], area_agrupamento)
                    agrupamento.tipo_regiao = TipoRegiao(row['tipo_regiao']).name
                    agrupamento.situacao = SituacaoRegiao().situacao(row['tipo_regiao'], row['situacao_regiao'])
                    agrupamento.coordenadas_geograficas = row['geometry']
                    agrupamento.municipio = row['cod_municipio']
                    agrupamento.coordenadas_geograficas_obj = coordenadas_poligono  # A partir do poligono ser um objeto eu consigo gerar o centróide, etc
                    agrupamento.centroide = coordenadas_poligono.centroid
                    agrupamento.possui_energia = row['possui_energia']
                    agrupamento.vizinhos = row['vizinhos']
                    # dados_socio_economicos
                    agrupamento.dados_socio_economico.total_agencias_bancarias = row['n_agencias_bancarias']
                    agrupamento.dados_socio_economico.total_cruzamentos = row['total_cruzamentos']
                    agrupamentos.append(agrupamento)
                else:
                    continue
                municipio.agrupamentos_territoriais = agrupamentos

            return municipio

        def gera_setores_censitarios(self, agrupamento):

            header_csv = ['cod_municipio', 'cod_setor', 'cod_agrupamento', 'n_habintates', 'n_domicilios',
                          'n_habitantes(5/60)', 'tipo_regiao', 'situacao_regiao', 'geometry']
            df, gdf = self.recupera_dados_geograficos(Path.ARQUIVO_SETORES.valor, header_csv)
            setores = []
            numero_habitantes = 0
            numero_domicilios = 0
            for index, row in df.iterrows():
                if row['cod_agrupamento'] == agrupamento.id_agrupamento_territorial:
                    coordenadas_poligono = Util().gera_poligono(gdf.geometry[index],
                                                                self.configuracao.figura_geometrica_polygon)
                    area_setor = Util().calcula_area_km(coordenadas_poligono)
                    setor = SetorCensitario(row['cod_setor'], 'setor_' + str(row['cod_setor']), area_setor)
                    setor.tipo_regiao = TipoRegiao(row['tipo_regiao']).name
                    setor.situacao = SituacaoRegiao().situacao(row['tipo_regiao'], row['situacao_regiao'])
                    setor.coordenadas_geograficas = row['geometry']
                    setor.coordenadas_geograficas_obj = coordenadas_poligono
                    setor.centroide = coordenadas_poligono.centroid
                    setor.municipio = row['cod_municipio']
                    setor.numero_habitantes = int(row['n_habintates'])
                    setor.total_domicilios = int(row['n_domicilios'])
                    setor.agrupamento_territorial = int(row['cod_agrupamento'])
                    setores.append(setor)
                    numero_habitantes = numero_habitantes + setor.numero_habitantes
                    numero_domicilios = numero_domicilios + setor.total_domicilios
                else:
                    continue
            agrupamento.setores_censitarios = setores
            agrupamento.numero_habitantes = numero_habitantes
            agrupamento.total_domicilios = numero_domicilios

            return agrupamento

        def gera_ruas(self, agrupamento):
            header_csv = ['id', 'id_municipio', 'id_agrupamento', 'tipo', 'geometry', 'vizinho', 'possivel_tipo_ponto']
            df, gdf = self.recupera_dados_geograficos(Path.ARQUIVO_PONTOS_RUA.valor, header_csv)

            # for territorio in territorios:
            ruas = []  # Vou add todos os pontos ao final de cada iteração do "for"
            for index, row in df.iterrows():  # iterar nas linhas do .csv
                if (row[
                    'id_agrupamento'] == agrupamento.id_agrupamento_territorial):  # Verifico se o id_municipio do .csv = ao id_municipio do território
                    coordenadas_ponto = Util().gera_ponto(
                        gdf.geometry[index])  # Recebo a coordenada do tipo objeto geoDataFrame
                    nome_ponto = 'ponto_' + str(row['id'])  # add nome ao ponto concatenando o id

                    rua = Rua(row['id'], nome_ponto, row['geometry'], row[
                        'id_municipio'])  # Crio um objeto ponto_rua que é do tipo da classe PontoRua e add os dados do .csv ao objeto
                    # rua.municipio = row['id_municipio']
                    rua.id_agrupamento = row['id_agrupamento']
                    rua.vizinhos = row['vizinho']  # encapsulo os vizinhos
                    rua.coordenadas_geograficas_obj = coordenadas_ponto  # encapsulo a coordenada como objeto
                    rua.tipo_ponto = row['tipo']
                    if np.isnan(row['possivel_tipo_ponto']) == False:
                        rua.possivel_tipo_ponto = int(row['possivel_tipo_ponto'])
                    else:
                        rua.possivel_tipo_ponto =''
                    agrupamento.ruas.append(rua)
                    ruas.append(rua)  # add todas as coordenadas a cada iteração do "for"
                else:
                    continue

            return agrupamento

        def gera_vizinhos_ruas(self, agrupamento):
            if len(agrupamento.ruas) > 0:
                agrupamento.ruas, self.vizinhos_ruas = self.gera_vizinhos_pontos_rua(agrupamento.ruas)

                #  ruas_com_pontos_vizinhos.append(vizinhos_ruas)
                self.mapa.adiciona_lista_line(self.vizinhos_ruas, self.mapa.style_line_ruas())

                if len(self.vizinhos_ruas) > 0:
                    pontos_central, pontos_rede, pontos_hub = self.recupera_pontos_rede(agrupamento.ruas)

                    # grafo inicial com todas as possibilidades de caminho
                    grafo_inicial = self.grafo.gera_arestas(self.vizinhos_ruas)
                    # self.grafo.gera_visualiza_grafo(grafo_inicial, 'Grafo Inicial - ID Agrupamento: ' + str(
                    #     agrupamento.id_agrupamento_territorial),
                    #                                 'grafo_inicial_agrupamento_' + str(
                    #                                     agrupamento.id_agrupamento_territorial))
                    # gera menor caminho entre central office e pontos de rede
                    self.gera_menor_caminho_co_rede(pontos_central, pontos_rede, self.grafo, self.mapa,
                                                    agrupamento.ruas,
                                                    grafo_inicial)
                    # gera menor caminho entre o hub e pontos de rede
                    self.gera_menor_caminho_hub_rede(pontos_hub, pontos_rede, self.grafo, self.mapa,
                                                     agrupamento.ruas,
                                                     grafo_inicial)

        def adiciona_estradas_municipios(self, municipio):

            municipio = self.gera_estradas(municipio)
            municipio.estradas, self.estradas_viz = self.gera_vizinhos_pontos_estradas(municipio.estradas)
            vizinhos_agrupamento = self.gera_agrupamentos_estradas(municipio.agrupamentos_territoriais,
                                                                   municipio.estradas)
            municipio.vizinhos = vizinhos_agrupamento
            self.mapa.adiciona_lista_line(self.estradas_viz, self.mapa.style_line_ruas())

            # distancia entre agrupamentos considerando estradas
            if len(vizinhos_agrupamento and self.estradas_viz) > 0:
                self.popula_distancia_com_fio_agrupamento(vizinhos_agrupamento, municipio)
                self.popula_distancia_sem_fio_agrupamento(vizinhos_agrupamento, municipio)

        def gera_estradas(self,municipio):
            header_csv = ['id', 'id_municipio', 'id_agrupamento_origem', 'id_agrupamento_destino', 'id_tipo_percurso',
                          'id_tipo_infraestrutura', 'tipo_ponto', 'geometry', 'vizinho']
            df, gdf = self.recupera_dados_geograficos(Path.ARQUIVO_ESTRADAS.valor,
                                                      header_csv)
            # for territorio in territorios:
            estradas = []  # Vou add todos os pontos ao final de cada iteração do "for"
            for index, row in df.iterrows():  # iterar nas linhas do .csv
                if (row[
                    'id_municipio'] == municipio.id_municipio):  # Verifico se o id_municipio do .csv = ao id_municipio do território
                    coordenadas_ponto = Util().gera_ponto(
                        gdf.geometry[index])  # Recebo a coordenada do tipo objeto geoDataFrame
                    nome_ponto = 'ponto_' + str(row['id'])  # add nome ao ponto concatenando o id
                    estrada = Estrada(row['id'], nome_ponto, row['geometry'], row['id_municipio'])
                    estrada.id_agrupamento_origem = row['id_agrupamento_origem']
                    estrada.id_agrupamento_destino = row['id_agrupamento_destino']
                    estrada.vizinhos = row['vizinho']  # encapsulo os vizinhos
                    estrada.coordenadas_geograficas_obj = coordenadas_ponto  # encapsulo a coordenada como objeto
                    estrada.tipo = row['tipo_ponto']
                    estrada.id_tipo_percurso = row['id_tipo_percurso']
                    estrada.id_tipo_infraestrutura = row['id_tipo_infraestrutura']
                    estradas.append(estrada)  # add todas as coordenadas a cada iteração do "for"
                else:
                    continue
                municipio.estradas = estradas

            return municipio

        def gera_agrupamentos_estradas(self, agrupamentos, estradas):
            agrupamentos, vizinhos_agrupamento = self.vizinhos_agrupamentos(agrupamentos)
            for ag in vizinhos_agrupamento:
                coordenadas_estrada_ponto = []
                coordenadas_estrada_line = []
                tipo_percurso = 0
                tipo_infra = 0
                for pt in estradas:
                    # recupera coordenadas, tipo infra e percurso dos pontos da estrada
                    if (
                            pt.id_agrupamento_origem == ag.id_ponto_origem and pt.id_agrupamento_destino == ag.id_ponto_vizinho
                            or pt.id_agrupamento_origem == ag.id_ponto_vizinho and pt.id_agrupamento_destino == ag.id_ponto_origem):
                        coordenadas_estrada_line.append(pt.coordenadas_obj_line)
                        # usada para calcular a distancia entre os pontos
                        coordenadas_estrada_ponto.append(pt.coordenadas_geograficas_obj)
                        tipo_infra = pt.id_tipo_infraestrutura
                        tipo_percurso = pt.id_tipo_percurso

                ag.coordenadas_obj_line = coordenadas_estrada_line
                ag.tipo_infra = tipo_infra
                ag.tipo_percurso = tipo_percurso
                ag.distancia = Util().gera_distancia_pontos(coordenadas_estrada_ponto)
                if len(coordenadas_estrada_ponto) > 0:
                    origem = (coordenadas_estrada_ponto[0].x, coordenadas_estrada_ponto[0].y)
                    destino = (coordenadas_estrada_ponto[len(coordenadas_estrada_ponto) - 1].x,
                                   coordenadas_estrada_ponto[len(coordenadas_estrada_ponto) - 1].y)
                    ag.distancia_simples = Util().distancia(origem, destino)
                    lista_pontos = []
                    lista_pontos.append(origem)
                    lista_pontos.append(destino)
                    ag.coordenadas_obj_line_dist_simples = Util().gera_linha(lista_pontos)

            return vizinhos_agrupamento

        def gera_vizinhos_pontos_rua(self, ruas_agrupamento):
            coordenadas_rua = {}
            v = []
            vizinhos_obj = []
            # gerar dicionário pontos rua
            # chave: id_ponto, value: coordenadas
            for r in ruas_agrupamento:  # iteração nos pontos da rua do território
                coordenadas_rua[r.id] = r.coordenadas_geograficas_obj  # recebe a coordenada

            index = 1
            for rua in ruas_agrupamento:
                coordenada_ponto_origem = (coordenadas_rua[rua.id].x, coordenadas_rua[
                    rua.id].y)  # utiliza a classe Point para separar a coordenada(2 tuplas) para calcular a distancia
                listaVizinhos = rua.vizinhos.split(
                    ",")  # separa em tuplas os vizinhos para iterar em cada vizinho de forma singular

                for vizinho in listaVizinhos:
                    lista = []
                    pontoVizinho = Vizinho(index, int(vizinho),
                                           rua.id)  # o pontoVizinho recebe o id_ponto_vizinho e id_ponto_rua
                    coordenada_ponto_destino = (coordenadas_rua[int(vizinho)].x,
                                                coordenadas_rua[int(vizinho)].y)  # Separo a coordenada destino
                    pontoVizinho.distancia = round(Util().distancia(coordenada_ponto_origem, coordenada_ponto_destino),
                                                   2)  # a minha distancia é calculada usando as coordenadas separadas
                    pontoVizinho.id_agrupamento = rua.id_agrupamento

                    lista.append(coordenada_ponto_origem)
                    lista.append(coordenada_ponto_destino)
                    # transforma a corrdenada do ponto da rua em LineString, que necessita de no mínimo dois pontos
                    # (coordenda_ponto_rua, coordenda_ponto_vizinho)
                    rua.coordenadas_obj_line = Util().gera_linha(lista)
                    pontoVizinho.coordenadas_obj_line = rua.coordenadas_obj_line
                    vizinhos_obj.append(pontoVizinho)
                    rua.vizinhos_obj.append(pontoVizinho)
                    index = index + 1

            return ruas_agrupamento, vizinhos_obj

        def recupera_pontos_rede(self, ruas_vizinhos):
            pontos_central = []
            pontos_rede = []
            pontos_hub = []

            # for a in municipio.agrupamentos_territoriais:
            listPontosRede = []
            listPontosRedeCO = []
            listPontosHub = []
            for p in ruas_vizinhos:
                if TipoPonto().isRede(p.tipo_ponto):
                    listPontosRede.append(p)  # Coloca aqui todos os pontos da ruas ou os pontos da Rede?
                if TipoPonto().isRedeCO(p.tipo_ponto):
                    listPontosRedeCO.append(p)
                if TipoPonto().isRedeHub(p.tipo_ponto):
                    listPontosHub.append(p)

            if (len(listPontosRedeCO) > 0):
                pontos_central = listPontosRedeCO
            if (len(listPontosRede) > 0):
                pontos_rede = listPontosRede
            if (len(listPontosHub) > 0):
                pontos_hub = listPontosHub

            return pontos_central, pontos_rede, pontos_hub

        def gera_menor_caminho_co_rede(self, pontos_central, pontos_rede, grafo, mapa, ruas, grafo_inicial):
            if len(pontos_central) > 0:
                for c in pontos_central:
                    for pt in pontos_rede:
                        distancia, caminho = grafo.calcula_distancia_pontos(grafo_inicial, c.id, pt.id)
                        # print(caminho)
                        # print(distancia)
                        pontos_rua = self.recupera_ponto_rua(caminho, ruas)
                        coordenadas_pontos_rua_line = self.gera_linha_otm(pontos_rua)
                        mapa.adiciona_coordenadas(coordenadas_pontos_rua_line,
                                                  mapa.style_line_ruas_otm())

        def gera_menor_caminho_hub_rede(self, pontos_hub, pontos_rede, grafo, mapa, ruas, grafo_inicial):
            if len(pontos_hub) > 0:
                for ph in pontos_hub:
                    for pt in pontos_rede:
                        coordenadas = []
                        coordenada_origem = (ph.coordenadas_geograficas_obj.x, ph.coordenadas_geograficas_obj.y)
                        coordenada_destino = (
                            pt.coordenadas_geograficas_obj.x, pt.coordenadas_geograficas_obj.y)
                        distancia = Util().distancia(coordenada_origem, coordenada_destino)
                        print(distancia)
                        coordenadas.append(coordenada_origem)
                        coordenadas.append(coordenada_destino)
                        mapa.adiciona_coordenadas(Util().gera_linha(coordenadas),
                                                  mapa.style_line_ruas_sem_fio())

        def gera_vizinhos_pontos_estradas(self, estradas):
            coordenadas_pontos_estrada = {}
            # gerar dicionário pontos rua
            # chave: id_ponto, value: coordenadas
            #  for territorio in territorios:  # iteração nos territórios
            for t in estradas:  # iteração nos pontos da rua do território
                coordenadas_pontos_estrada[
                    t.id] = t.coordenadas_geograficas_obj  # recebe a coordenada objeto e seu id //dicionario de dados
            index = 1
            v = []
            for ponto in estradas:
                lista = []
                coordenada_ponto_origem = (coordenadas_pontos_estrada[ponto.id].x, coordenadas_pontos_estrada[
                    ponto.id].y)  # utiliza a classe Point para separar a coordenada(2 tuplas) para calcular a distancia
                listaVizinhos = ponto.vizinhos.split(
                    ",")  # separa em tuplas os vizinhos para iterar em cada vizinho de forma singular
                for vizinho in listaVizinhos:
                    pontoVizinho = Vizinho(index, int(vizinho),
                                           ponto.id)  # o pontoVizinho recebe o id_ponto_vizinho e id_ponto_rua
                    coordenada_ponto_destino = (coordenadas_pontos_estrada[int(vizinho)].x, coordenadas_pontos_estrada[
                        int(vizinho)].y)  # Separo a coordenada destino
                    pontoVizinho.distancia = round(
                        Util().distancia(coordenada_ponto_origem, coordenada_ponto_destino),
                        2)  # a minha distancia é calculada usando as coordenadas separadas

                    lista.append(coordenada_ponto_origem)
                    lista.append(coordenada_ponto_destino)
                    # transforma a corrdenada do ponto da rua em LineString, que necessita de no mínimo dois pontos
                    # (coordenda_ponto_rua, coordenda_ponto_vizinho)
                    ponto.coordenadas_obj_line = Util().gera_linha(lista)
                    pontoVizinho.coordenadas_obj_line = ponto.coordenadas_obj_line
                    v.append(pontoVizinho)
                    ponto.vizinhos_obj.append(pontoVizinho)
                    index = index + 1

                ponto.vizinhos_obj = v

            return estradas, v

        def vizinhos_agrupamentos(self, agrupamentos):
            index = 1
            vizinhos_temp = []
            for agrupamento in agrupamentos:
                listaVizinhos = agrupamento.vizinhos.split(
                    ",")  # separa em tuplas os vizinhos para iterar em cada vizinho de forma singular
                for vizinho in listaVizinhos:
                    pontoVizinho = Vizinho(index, int(vizinho),
                                           agrupamento.id_agrupamento_territorial)  # o pontoVizinho recebe o id_ponto_vizinho e id_ponto_rua
                    pontoVizinho.id_municipio = agrupamento.municipio
                    vizinhos_temp.append(pontoVizinho)
                    index = index + 1
                    agrupamento.vizinhos_obj.append(pontoVizinho)

            return agrupamentos, vizinhos_temp

        def recupera_ponto_rua(self, ids_pontos, ruas):
            pontos = []
            for id in ids_pontos:
                for ponto in ruas:
                    if (id == ponto.id):
                        pontos.append(ponto)
            return pontos

        def gera_linha_otm(self, pontos_rua):
            coordenadas = []
            for i in pontos_rua:
                coordenadas.append(i.coordenadas_geograficas_obj)
            return Util().gera_linha(coordenadas)

        def popula_distancia_com_fio_agrupamento(self, vizinhos_agrupamento, municipio):
            grafo_agrupamento_arvore_minima = self.grafo.gera_arvore_minima_conexao_com_fio(vizinhos_agrupamento,
                                                                                            municipio)

            agrupamentos_recuperados = self.recupera_caminhos_otimizados(grafo_agrupamento_arvore_minima,
                                                                         vizinhos_agrupamento)
            if len(agrupamentos_recuperados) > 0:
                self.mapa.adiciona_lista_line_estrada(agrupamentos_recuperados, self.mapa.style_line_arvore_minima())

        def popula_distancia_sem_fio_agrupamento(self, vizinhos_agrupamento, municipio):
            # distancia simples entre agrupamentos
            grafo_agrupamento_arvore_minima_distancia_simples = self.grafo.gera_arvore_minima_conexao_sem_fio(
                vizinhos_agrupamento, municipio)
            agrupamentos_dist_simples = self.recupera_caminhos_otimizados(
                grafo_agrupamento_arvore_minima_distancia_simples,
                vizinhos_agrupamento)
            if len(agrupamentos_dist_simples) > 0:
                self.mapa.adiciona_lista_line_dist_simples(agrupamentos_dist_simples,
                                                           self.mapa.style_line_arvore_minima_dist_simples())

        def recupera_caminhos_otimizados(self, grafo_otm, pontos_vizinhos):
            list = []
            for (u, v, d) in grafo_otm.edges(data=True):
                for ponto in pontos_vizinhos:
                    if (
                            ponto.id_ponto_origem == u and ponto.id_ponto_vizinho == v or ponto.id_ponto_origem == v and ponto.id_ponto_vizinho == u):
                        list.append(ponto)

            return list


        def exibe_info_geograficas(self, municipio):
            print('Município')
            print(municipio)
            print('Agrupamentos')
            print(municipio.agrupamentos_territoriais)
            print('Setores')
            for ag in municipio.agrupamentos_territoriais:
                for setor in ag.setores_censitarios:
                    print(setor)
                print('Pontos Rua')
                print(ag.ruas)

        def recupera_dados_geograficos(self, path_arquivo, header):
            df = pd.read_csv(path_arquivo, delimiter=";",
                             usecols=header)  # O panda tem a funcao "read.csv"que vai lê o arquivo csv
            gdf = GeoDataFrame(
                df)  # Transforma as colunas do arquivo em um dado do tipo GeoDataFrama, que é como é usado na biblioteca GeoPanda
            return df, gdf

