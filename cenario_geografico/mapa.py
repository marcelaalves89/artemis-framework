
import folium
from conf.path import Path

class Mapa:

    def __init__(self):
        #centroide area brasileira
        self.mapa = folium.Map(location=[-15.788497, -47.879873], zoom_start=0o4)

    def style_municipio(self):
        return lambda feature: {'fillColor': 'green', 'color': 'blue', 'weight': 0.5}

    def style_agrupamentos(self):
        #     'fillColor': 'yellow',
        return lambda feature: {'color': 'yellow', 'weight': 0.9}

    def style_setores(self):
        return lambda feature: {'fillColor': 'red', 'color': 'darkred', 'weight': 0.5}

    def style_line_ruas(self):
        return lambda feature: {'fillColor': 'green', 'color': 'green', 'weight': 2}

    def style_line_ruas_otm(self):
        return lambda feature: {'fillColor': 'black', 'color': 'black', 'weight': 2}

    def style_line_arvore_minima(self):
        return lambda feature: {'color': 'pink', 'weight': 2}

    def style_line_arvore_minima_dist_simples(self):
        return lambda feature: {'color': 'yellow', 'weight': 2}

    def style_line_ruas_sem_fio(self):
        return lambda feature: {'fillColor': 'blue', 'color': 'blue', 'weight': 2}

    def adiciona_lista(self, obj, field_style):
       for x in obj:
            self.adiciona(x, field_style)
       self.salvar_mapa()

    def adiciona_lista_line(self, obj, field_style):
       for x in obj:
           folium.GeoJson(x.coordenadas_obj_line, style_function=field_style).add_to(self.mapa)
       self.salvar_mapa()

    def adiciona_lista_line_dist_simples(self, obj, field_style):
       for x in obj:
           folium.GeoJson(x.coordenadas_obj_line_dist_simples, style_function=field_style).add_to(self.mapa)
       self.salvar_mapa()

    def adiciona_lista_line_estrada(self, obj, field_style):
       for x in obj:
           for t in x.coordenadas_obj_line:
                folium.GeoJson(t, style_function=field_style).add_to(self.mapa)
       self.salvar_mapa()

    def adiciona(self, obj, field_style):
        folium.GeoJson(obj.coordenadas_geograficas_obj, name=obj.nome, style_function=field_style).add_to(self.mapa)

    def adiciona_coordenadas(self, obj, field_style):
        folium.GeoJson(obj, style_function=field_style).add_to(self.mapa)

    def salvar_mapa(self):
        self.mapa.save(Path.MAPA.valor)


