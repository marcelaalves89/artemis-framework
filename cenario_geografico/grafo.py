from networkx import nx
import matplotlib.pyplot as plt
from conf.path import Path
from util.util import Util
from entidade.tipo_infraestratura import TipoInfraEstrutura
from entidade.tipo_percurso import TipoPercurso


class Grafo:

    def gera_arestas_pontos_ruas(self, pontos):
        grafo = nx.Graph()
        for ponto in pontos:
            # cria arestas com peso
            grafo.add_node(ponto.id)
            for vizinho in ponto.vizinhos_obj:
                grafo.add_edge(ponto.id, vizinho.id_ponto_vizinho, weight=vizinho.distancia)
        return grafo

    def gera_arestas(self, pontos):
        grafo = nx.Graph()
        # cria arestas com peso
        for ponto in pontos:
            grafo.add_edge(ponto.id_ponto_origem, ponto.id_ponto_vizinho, weight=ponto.distancia)
        return grafo

    def gera_arestas_custo(self, pontos, usar_peso_percurso = True, usar_peso_infra = True):
        grafo = nx.Graph()
        peso_percurso = 0
        peso_infra = 0
        # cria arestas com peso
        for ponto in pontos:
            if usar_peso_percurso:
                peso_percurso = TipoPercurso().peso(ponto.tipo_percurso)
            if usar_peso_infra:
                peso_infra = TipoInfraEstrutura().peso(ponto.tipo_infra)
            if usar_peso_percurso and usar_peso_infra:
                peso = Util().ponderacao_custo_distancia(ponto.distancia, peso_percurso, peso_infra)
            else:
                peso = ponto.distancia
            grafo.add_edge(ponto.id_ponto_origem, ponto.id_ponto_vizinho, weight=peso)
        return grafo

    def gera_arestas_custo_distancia_simples(self, pontos, usar_peso_percurso = True, usar_peso_infra = True):
        grafo = nx.Graph()
        peso_percurso = 0
        peso_infra = 0
        # cria arestas com peso
        for ponto in pontos:
            if usar_peso_percurso:
                peso_percurso = TipoPercurso().peso(ponto.tipo_percurso)
            if usar_peso_infra:
                peso_infra = TipoInfraEstrutura().peso(ponto.tipo_infra)
            if usar_peso_percurso and usar_peso_infra:
                peso = Util().ponderacao_custo_distancia(ponto.distancia_simples, peso_percurso, peso_infra)
            else:
                peso = ponto.distancia_simples
            grafo.add_edge(ponto.id_ponto_origem, ponto.id_ponto_vizinho, weight=peso)
        return grafo

    def visualiza_grafo(self, grafo, titulo_grafo, nome_arquivo_figura):
        self.geraGrafo(grafo, titulo_grafo, nome_arquivo_figura)
        return grafo

    def geraGrafo(self, grafo, titulo, nome_arquivo_figura):
        pos = nx.spring_layout(grafo)
        labels = nx.draw_networkx_labels(grafo, pos)
        nx.draw_networkx_nodes(grafo, pos, node_size=100, node_color='r', label=labels)
        nx.draw_networkx_edges(grafo, pos)
        #labels = nx.get_edge_attributes(grafo, 'weight')
        #nx.draw_networkx_edge_labels(grafo, pos, edge_labels=labels)
        plt.title(titulo, size=16)
        self.save(nome_arquivo_figura)
        plt.show()

    def save(self,nome):
        plt.savefig(Path.FIGURA_CENARIO + '/' + nome + '.png', format='png')

    def cria_arvore_geradora_minima(self,grafo):
        # gera o menor caminho
        mst = nx.minimum_spanning_tree(grafo)
        grafo = nx.Graph()
        # gero um outro grafo com os novos caminhos
        for (u, v, d) in mst.edges(data=True):
            grafo.add_edge(u, v, weight=d['weight'])
        return grafo

    def exibir_informacoes_grafo(self, grafo, titulo):
        print('\n')
        print(titulo)
        # nós, arestas e pesos grafos
        for (u, v, d) in grafo.edges(data=True):
            print(u, v, d)
        print('\n')

    def calcula_distancia_pontos(self, grafo, ponto_origem, ponto_destino):
        distances, paths = nx.single_source_dijkstra(grafo, ponto_origem, ponto_destino, weight='weight')
        return distances, paths

    def gera_visualiza_grafo (self, obj,  titulo_grafo, nome_arquivo):
        self.visualiza_grafo(obj, titulo_grafo, nome_arquivo)
        self.exibir_informacoes_grafo(obj, titulo_grafo)

    #distancia simples
    def gera_arvore_minima_conexao_sem_fio(self, obj, municipio, usar_peso_percurso = True, usar_peso_infra = True):
        grafo_agrupamento_distancia_simples = self.gera_arestas_custo_distancia_simples(obj, usar_peso_percurso, usar_peso_infra)
        # Descomentar
        #self.gera_visualiza_grafo(grafo_agrupamento_distancia_simples,
        #                           'Grafo Inicial Distância Simples Agrupamento Município: ' + str(
        #                               municipio.id_municipio),
        #                           'agrupamentos_distancia_simples_inicial_municipio_' + str(
        #                               municipio.id_municipio))
        # caminhos usando a árvore geradora mínima
        grafo_agrupamento_arvore_minima_distancia_simples = self.cria_arvore_geradora_minima(
            grafo_agrupamento_distancia_simples)
        # Descomentar
        #self.gera_visualiza_grafo(grafo_agrupamento_arvore_minima_distancia_simples,
        #                           'Grafo Árvore Geradora Mínima Distância Simples Município: ' + str(
        #                               municipio.id_municipio),
        #                           'agrupamento_distancia_simples_arvore_municipio_' + str(
        #                               municipio.id_municipio))
        return grafo_agrupamento_arvore_minima_distancia_simples

    def gera_arvore_minima_conexao_com_fio(self, obj, municipio, usar_peso_percurso = True, usar_peso_infra = True):
        grafo_agrupamento = self.gera_arestas_custo(obj, usar_peso_percurso, usar_peso_infra)
        # Descomentar
        #self.gera_visualiza_grafo( grafo_agrupamento, 'Grafo Inicial Agrupamento Município: ' + str(
        #    municipio.id_municipio), 'agrupamentos_estradas_inicial_municipio_' + str(
        #     municipio.id_municipio))
        # caminhos usando a árvore geradora mínima
        grafo_agrupamento_arvore_minima = self.cria_arvore_geradora_minima(grafo_agrupamento)
        # Descomentar
        #self.gera_visualiza_grafo(grafo_agrupamento_arvore_minima,
        #                           'Grafo Árvore Geradora Mínima Município: ' + str(
        #                               municipio.id_municipio),
        #                           'agrupamento_estradas_arvore_municipio_' + str(
        #                               municipio.id_municipio))
        return grafo_agrupamento_arvore_minima