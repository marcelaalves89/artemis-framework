import pandas as pd
import matplotlib.pyplot as plt
from conf.configuracao import Configuracao
from conf.parametro import Parametro
from conf.path import Path
from entidade.aplicacacao import Aplicacao
import numpy as np
from entidade.tipo_servico_aplicacao import TipoServicoAplicacao
from previsao_de_trafego.demanda_trafego import DemandaTrafego


class CenarioDemandaTrafego:

    def __init__(self, tempo_analise):
        self.tempo_analise = tempo_analise

    def carrega_aplicacoes(self):

        header_csv = ['id', 'id_tipo_servico', 'nome', 'descricao', 'vazao', 'taxa_final','start_adoption',
                      'adoption_rate', 'alpha']
        aplicacoes = []
        df = pd.read_csv(Path.ARQUIVO_APLICACOES.valor, delimiter=";", usecols=header_csv)
        for index, row in df.iterrows():
            aplicacao = Aplicacao(row['id'], row['nome'], row['vazao'], row['taxa_final'],
                                  row['start_adoption'], row['adoption_rate'], row['alpha'], TipoServicoAplicacao(row['id_tipo_servico']).name)
            aplicacoes.append(aplicacao)
        return aplicacoes

    def carrega_demanda_trafego_agrupamento(self, municipio, aplicacoes):

        total_habitantes_municipio = municipio.total_habitantes()

        for agrupamento in municipio.agrupamentos_territoriais:
            agrupamento = self.carrega_totais_socio_economico_agrupamento(agrupamento, municipio,
                                                                      total_habitantes_municipio)
            demanda_trafego = DemandaTrafego(agrupamento, self.tempo_analise, aplicacoes)
            demanda_trafego.calcula_demanda_trafego()

        self.debug(municipio.agrupamentos_territoriais, municipio.id_municipio)


    def carrega_totais_socio_economico_agrupamento(self, agrupamento, municipio, total_habitantes_municipio):
        agrupamento.dados_socio_economico.percentual_habitantes = agrupamento.numero_habitantes / total_habitantes_municipio
        agrupamento.dados_socio_economico.total_servidores_publicos = agrupamento.dados_socio_economico.percentual_habitantes * municipio.dados_socio_economico.total_servidores_publicos
        agrupamento.dados_socio_economico.total_servidores_publicos_saude = np.ceil(
        agrupamento.dados_socio_economico.percentual_habitantes * municipio.dados_socio_economico.total_servidores_publicos_saude)
        agrupamento.dados_socio_economico.total_pop_ativa = municipio.dados_socio_economico.percentual_pop_ativa * agrupamento.numero_habitantes
        agrupamento.dados_socio_economico.total_trabalhadores_informais = agrupamento.dados_socio_economico.total_pop_ativa - agrupamento.dados_socio_economico.total_servidores_publicos - agrupamento.dados_socio_economico.total_servidores_publicos_saude
        agrupamento.dados_socio_economico.total_veiculos = agrupamento.dados_socio_economico.percentual_habitantes * municipio.dados_socio_economico.total_veiculos
        agrupamento.dados_socio_economico.total_alunos = agrupamento.dados_socio_economico.percentual_habitantes * municipio.dados_socio_economico.total_alunos
        agrupamento.dados_socio_economico.total_docentes = agrupamento.dados_socio_economico.percentual_habitantes * municipio.dados_socio_economico.total_docentes
        agrupamento.dados_socio_economico.percentual_alunos_ead = municipio.dados_socio_economico.percentual_alunos_ead
        agrupamento.dados_socio_economico.percentual_pop_ativa = municipio.dados_socio_economico.percentual_pop_ativa

        return agrupamento

    def debug(self, agrupamentos, id_municipio):
        time = np.arange(self.tempo_analise)
        demanda_trafego_total = np.zeros(self.tempo_analise)
        demanda_usuarios_total = np.zeros(self.tempo_analise)
        demanda_aplicacoes_total = np.zeros(self.tempo_analise)
        densidade_usuarios_total = np.zeros(self.tempo_analise)
        # for ag in agrupamentos:
        #     print('Dados do Aglomerado {}:'.format(ag.id_agrupamento_territorial))
        #     print('User Fraction')
        #     print(ag.user_fraction)
        #     print('\n')
        #     print('Densidade Usuarios')
        #     print(ag.densidade_usuarios)
        #     densidade_usuarios_total += ag.densidade_usuarios
        #     print('\n')
        #     print('Demanda Tráfego por Terminais')
        #     print(ag.demanda_trafego_terminais)
        #     print('\n')
        #     print('Demanda de Usuários de Internet')
        #     print(ag.demanda_usuarios)
        #     demanda_usuarios_total += ag.demanda_usuarios
        #     print('\n')
        #     print('Demanda Aplicações')
        #     print(ag.demanda_aplicacoes)
        #     demanda_aplicacoes_total += ag.demanda_aplicacoes
        #     print('\n')
        #     print('Demanda de Trafego')
        #     print(ag.demanda_trafego_por_area)
        #     demanda_trafego_total += ag.demanda_trafego_por_area
        #     print('\n')
        #     #Plotando gráficos somente sobre os Agrupamentos
        #     plt.title('Demanda de Tráfego Total por Área - Agrupamento {}'.format(ag.id_agrupamento_territorial))
        #     plt.plot(ag.densidade_usuarios, ag.demanda_trafego_por_area, '-*')
        #     plt.xlabel('Densidade de Usuários (usuários/km2)')
        #     plt.ylabel('Demanda de Tráfego [Mbps/km2]')
        #     plt.grid(linestyle=':')
        #     plt.legend(loc='upper left')
        #
        #     plt.figure()
        #     plt.title('Demanda de Tráfego por Área - Agrupamento {}'.format(ag.id_agrupamento_territorial))
        #     plt.plot(time, ag.demanda_trafego_por_area, '-*', label='Demada de Tráfego Total')
        #     plt.plot(time, ag.demanda_aplicacoes, '-.', label='Demanda Aplicações IoT/M2M')
        #     plt.plot(time, ag.demanda_usuarios, '-o', label='Demanda Usuários Comuns')
        #     plt.xlabel('Unidade de Tempo (t)')
        #     plt.ylabel('Demanda de Tráfego [Mbps/km2]')
        #     plt.grid(linestyle=':')
        #     plt.legend(loc='upper left')
        #     plt.figure()
        #
        #
        #
        # #####Em função da densidade de usuários
        # plt.plot(densidade_usuarios_total, demanda_trafego_total, '-*', label='Demanda Total')
        # plt.title('Demanda de Tráfego Total por Área do Município: ' + str(id_municipio))
        # plt.xlabel('Densidade de Usuários [usuários/km2]')
        # plt.ylabel('Demanda de Tráfego [Mbps/km2]')
        # plt.grid(linestyle=':')
        # nome = 'demanda_densidade_periodo_agrupamento_municipio_'+str(id_municipio)
        # plt.legend(loc='upper left')
        # plt.savefig(Path.FIGURA_DEMANDA_TRAFEGO + '/' + nome + '.png', format='png')
        # plt.show()
        #
        # ######Em função do período de análise
        # plt.plot(time, demanda_trafego_total, '-*', label='Demanda Total')
        # plt.plot(time, demanda_aplicacoes_total, '-.', label='Demanda Aplicações e IoT/M2M')
        # plt.plot(time, demanda_usuarios_total, '-o', label='Demanda Usuários Comuns')
        # plt.title('Demanda de Tráfego Total por Área do Município: ' + str(id_municipio))
        # plt.xlabel('Unidade de Tempo (t)')
        # plt.ylabel('Demanda de Tráfego [Mbps/km2]')
        # plt.grid(linestyle=':')
        # nome = 'demanda_densidade_agrupamentos_municipio_'+str(id_municipio)
        # plt.legend(loc='upper left')
        # plt.savefig(Path.FIGURA_DEMANDA_TRAFEGO + '/' + nome + '.png', format='png')
        # plt.show()
