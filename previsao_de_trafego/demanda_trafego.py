import numpy as np
from entidade.trafego_usuarios_moveis import TrafegoUsuariosMoveis as TUM
from util.util import Util

class DemandaTrafego:


    def __init__(self, agrupamento, tempo_analise, aplicacoes):
        self.agrupamento = agrupamento
        self.tempo_analise = tempo_analise
        self.aplicacoes = aplicacoes
        self.densidade_demografica = Util().densidade_demografica_total(self.agrupamento.numero_habitantes, self.agrupamento.area)
        self.agrupamento.total_terminais =np.zeros(self.tempo_analise)

    def calcula_demanda_aplicacoes(self):
        demanda_aplicacoes = np.zeros(self.tempo_analise)
        for app in self.aplicacoes:
            qtd_terminais = 0.0
            if app.id == 1:
                qtd_terminais = self.agrupamento.numero_habitantes
            elif app.id == 2:
                qtd_terminais = np.ceil(self.agrupamento.numero_habitantes/1000.0)
            elif app.id == 3:
                qtd_terminais = self.agrupamento.dados_socio_economico.total_servidores_publicos_saude
            elif app.id == 4:
                qtd_terminais = np.ceil(self.agrupamento.dados_socio_economico.total_alunos * self.agrupamento.dados_socio_economico.percentual_alunos_ead)
            elif app.id == 5:
                qtd_terminais = np.ceil(self.agrupamento.dados_socio_economico.total_alunos + self.agrupamento.dados_socio_economico.total_docentes)
            elif app.id == 6:
                qtd_terminais = self.agrupamento.dados_socio_economico.total_cruzamentos
            elif app.id == 7:
                qtd_terminais = np.ceil(self.agrupamento.dados_socio_economico.total_servidores_publicos)
            elif app.id == 8:
                qtd_terminais = self.agrupamento.dados_socio_economico.total_agencias_bancarias * 5
            elif app.id ==9:
                qtd_terminais = np.ceil(self.agrupamento.dados_socio_economico.total_pop_ativa -
                                                                        self.agrupamento.dados_socio_economico.total_servidores_publicos - self.agrupamento.dados_socio_economico.total_servidores_publicos_saude)
            elif app.id == 10 or app.id == 11:
                qtd_terminais = self.agrupamento.total_domicilios
            else:
                qtd_terminais = np.ceil(self.agrupamento.dados_socio_economico.total_veiculos)

            c = Util().get_gompertz(app.mu, app.beta, app.gamma, self.tempo_analise)
            self.agrupamento.total_terminais += np.ceil(c * qtd_terminais)
            c = (qtd_terminais/self.agrupamento.area) * app.alpha * c * app.vazao

            #Descomentar
            #print('Dados de Aplicações de IoT/M2M para o Aglomerado {}'.format(self.agrupamento.id_agrupamento_territorial))
            #print('Aplicação IoT: {} (alpha={}, beta={}, mu={}, gamma={}, terminais={}, vazao={})'.format(app.nome, app.alpha, app.beta, app.mu, app.gamma, qtd_terminais, app.vazao))
            demanda_aplicacoes += c
        self.agrupamento.demanda_aplicacoes = demanda_aplicacoes
        print('\n')

    def calcula_densidade_usuarios(self):
        densidade_usarios = Util().get_gompertz(TUM.CONFIG_DEFAULT.proporcao_final_usuario_internet,
                                         TUM.CONFIG_DEFAULT.inicio_adocao,
                                         TUM.CONFIG_DEFAULT.taxa_crescimento_usuarios_internet,
                                         self.tempo_analise)
        self.agrupamento.densidade_usuarios =  densidade_usarios * self.densidade_demografica * self.agrupamento.dados_socio_economico.percentual_pop_ativa

    def calcula_trafego_terminal(self):
        # Inicializa duas matrizes com 3 linhas (tipos de terminais) x 'tempo_analise' colunas
        r_j = np.zeros((3, self.tempo_analise))
        rs_j = np.zeros((3, self.tempo_analise))

        heavy_users = Util().get_gompertz(TUM.CONFIG_DEFAULT.proporcao_final_terminais_heavy,
                                   TUM.CONFIG_DEFAULT.inicio_adocao,
                                   TUM.CONFIG_DEFAULT.taxa_crescimento_terminais_heavy,
                                   self.tempo_analise)

        # ela era um array e converteu ele em uma matriz de uma linha
        heavy_users = np.array([heavy_users])

        # ele cria uma matriz ordinary para receber o resultado da subtracao entre 1 e o primeiro, segundo...
        # elemento da matriz heavy e coloca o resultado na respectiva posicao da matriz ordinary
        ordinary_users = 1 - heavy_users

        self.agrupamento.user_fraction = np.concatenate((heavy_users, ordinary_users), axis=0)

        # Linhas: r_heavy/rordinary; Colunas: PC/Tablets/Smartphones
        # Valores expressos em Mbps
        # demanda_trafego_terminais = np.array([[0.125, 0.06244444, 0.01562222], [0.031, 0.00780556, 0.00195278]])
        demanda_trafego_terminais = np.array([[25.0, 7.0, 5.0], [3.125, 1.625, 1.25]])  # valores aumentados

        for i in range(self.tempo_analise):
            r_j[:, i] = np.asmatrix(self.agrupamento.user_fraction[:, i].T).dot(demanda_trafego_terminais)

        # Proporção entre terminais PC/Tablets/Smartphones
        # Valores: 20%/10%/70%
        s_j = np.array([[0.2, 0.1, 0.7]])

        # Multiplica ponto-a-ponto cada coluna de r_j por s_j
        for i in range(self.tempo_analise):
            rs_j[:, i] = np.multiply(r_j[:, i], s_j)

        self.agrupamento.demanda_trafego_terminais = np.sum(rs_j,
                                                 axis=0)  # achata somando as linhas, resultado em 1 linha e 15 colunas

    def calcula_demada_usuarios(self):
        self.agrupamento.demanda_usuarios = self.agrupamento.densidade_usuarios * TUM.CONFIG_DEFAULT.taxa_usuarios_ativos \
                                 * self.agrupamento.demanda_trafego_terminais

    def calcula_demanda_trafego(self):
        self.calcula_demanda_aplicacoes()
        self.calcula_densidade_usuarios()
        self.calcula_trafego_terminal()
        self.calcula_demada_usuarios()
        self.agrupamento.demanda_trafego_por_area = np.add(self.agrupamento.demanda_aplicacoes, self.agrupamento.demanda_usuarios)  # faco um chuveirinho somando






