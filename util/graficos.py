import matplotlib.pyplot as plt
import numpy as np

from util.util import Util
from conf.path import Path


class Grafico:

    def __init__(self, tempo_analise, municipio):
        self.tempo_analise = tempo_analise
        self.municipio = municipio

    def get_graficos(self):
        self.__get_graficos_municipio()
        self.__get_graficos_aglomerados()

    def __get_graficos_municipio(self):
        self.tco_simples_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                   Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, False)
        self.tco_simples_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                   Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, True)

        self.composicao_tco_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                      Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, False)
        self.composicao_tco_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                      Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, True)
        self.composicao_tco_porcentagem_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                                  Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, False)
        self.composicao_tco_porcentagem_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                                  Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, True)
        self.evolucao_tco_municipio(self.municipio.cenarios, self.tempo_analise,
                                    Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, False)
        self.evolucao_tco_municipio(self.municipio.cenarios, self.tempo_analise,
                                    Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, True)
        self.composicao_tco100_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                      Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, False)
        self.composicao_tco100_municipio(self.municipio.cenarios, 'Rede', self.tempo_analise,
                                      Path.DIRETORIO_IMAGEM_MUNICIPIO.valor, True)

        self.npv_municipio(self.municipio.cenarios, Path.DIRETORIO_IMAGEM_NPV.valor, False)
        self.npv_municipio(self.municipio.cenarios, Path.DIRETORIO_IMAGEM_NPV.valor, True)

        self.fluxo_caixa_municipio(self.municipio.cenarios, Path.DIRETORIO_IMAGEM_FC.valor, False)
        self.fluxo_caixa_municipio(self.municipio.cenarios, Path.DIRETORIO_IMAGEM_FC.valor, True)



    def __get_graficos_aglomerados(self):
        # Recupera os cenários e suas versões alternativas
        cenarios = Util().get_cenarios_alternativos(self.municipio.cenarios)

        # Gera Gráficos de Radio para Todos os Aglomerados
        for c in cenarios:
            # Radio
            self.tco_simples(c, 'Radio', self.tempo_analise, Path.DIRETORIO_IMAGEM_RADIO.valor)
            self.composicao_tco(c, 'Radio', Path.DIRETORIO_IMAGEM_RADIO.valor)
            self.composicao_tco_porcentagem(c, 'Radio', Path.DIRETORIO_IMAGEM_RADIO.valor)
            self.evolucao_tco(c, 'Radio', self.tempo_analise, Path.DIRETORIO_IMAGEM_RADIO.valor)

            # Transporte
            self.tco_simples(c, 'Transporte', self.tempo_analise, Path.DIRETORIO_IMAGEM_TRANSPORTE.valor)
            self.composicao_tco(c, 'Transporte', Path.DIRETORIO_IMAGEM_TRANSPORTE.valor)
            self.composicao_tco_porcentagem(c, 'Transporte', Path.DIRETORIO_IMAGEM_TRANSPORTE.valor)
            self.evolucao_tco(c, 'Transporte', self.tempo_analise, Path.DIRETORIO_IMAGEM_TRANSPORTE.valor)

            # Fotovoltaico
            self.tco_simples(c, 'Fotovoltaico', self.tempo_analise,
                             Path.DIRETORIO_IMAGEM_SISTEMA_FOTOVOLTAICO.valor)
            self.evolucao_tco(c, 'Fotovoltaico', self.tempo_analise,
                              Path.DIRETORIO_IMAGEM_SISTEMA_FOTOVOLTAICO.valor)

            # TCO do Radio, Transporte e Fotovoltaico
            self.tco_simples_total_agrupamento(c, 'Rede', self.tempo_analise,
                                               Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor, True)
            self.tco_simples_total_agrupamento(c, 'Rede', self.tempo_analise,
                                               Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor, False)
            self.composicao_tco_total_agrupamento(c, 'Rede', Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor, False)
            self.composicao_tco_total_agrupamento(c, 'Rede', Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor, True)
            self.composicao_tco_porcentagem_total_agrupamento(c, 'Rede', Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor,
                                                              False)
            self.composicao_tco_porcentagem_total_agrupamento(c, 'Rede', Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor,
                                                              True)
            self.evolucao_tco_total_agrupamento(c, self.tempo_analise, Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor,
                                                False)
            self.evolucao_tco_total_agrupamento(c, self.tempo_analise, Path.DIRETORIO_IMAGEM_RAD_TRANSP_FOTOV.valor,
                                                True)

    def tco_simples(self, cenarios, tipo_grafico, tempo_analise, path_imagem):
        global id_aglomerado

        plt.figure(figsize=(9.0, 5.5))

        capex = list()
        opex = list()
        rotulos = list()

        # Legendas e Largura das Barras
        legenda_ = ['CAPEX', 'OPEX']
        line_width = 0.5
        bar_width = 1.5

        if tipo_grafico == 'Radio':
            label = 'RAN'
        elif tipo_grafico == 'Transporte':
            label = 'Backhaul'
        else:
            label = 'Photovoltaic'

        for c in cenarios:

            capex_macro = cenarios[c].capex_macro[tipo_grafico]
            capex_hetnet = cenarios[c].capex_hetnet[tipo_grafico]
            opex_macro = cenarios[c].opex_macro[tipo_grafico]
            opex_hetnet = cenarios[c].opex_hetnet[tipo_grafico]

            if tipo_grafico != 'Fotovoltaico':
                capex_m = np.zeros(tempo_analise)
                capex_m += capex_macro['infraestrutura']
                capex_m += capex_macro['equipamentos']
                capex_m += capex_macro['instalacao']

                capex_h = np.zeros(tempo_analise)
                capex_h += capex_hetnet['infraestrutura']
                capex_h += capex_hetnet['equipamentos']
                capex_h += capex_hetnet['instalacao']

                opex_m = np.zeros(tempo_analise)
                opex_m += opex_macro['energia']
                opex_m += opex_macro['manutencao']
                opex_m += opex_macro['aluguel']
                opex_m += opex_macro['falhas']

                opex_h = np.zeros(tempo_analise)
                opex_h += opex_hetnet['energia']
                opex_h += opex_hetnet['manutencao']
                opex_h += opex_hetnet['aluguel']
                opex_h += opex_hetnet['falhas']

                capex.append(capex_m.sum())
                capex.append(capex_h.sum())
                opex.append(opex_m.sum())
                opex.append(opex_h.sum())
            else:
                capex.append(sum(capex_macro.values()))
                capex.append(sum(capex_hetnet.values()))
                opex.append(sum(opex_macro.values()))
                opex.append(sum(opex_hetnet.values()))

            rotulos.append('Macro')
            rotulos.append('\n{}'.format(c))
            rotulos.append('Hetnet')

            if cenarios[c].tipo_cenario == 'Original':
                id_aglomerado = cenarios[c].id_agrupamento_territorial

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])

        plt.bar(posicao, capex, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, opex, bottom=capex, color='#cf4d4f', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        # Alterações nas propriedades dos Eixos X e Y
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda_, loc='best')
        plt.ylabel('{} TCO - Monetary Units ($)'.format(label))

        plt.savefig('{}TCO-{}-Simples-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def tco_simples_total_agrupamento(self, cenarios, tipo_grafico, tempo_analise, path_imagem, sistema_fotovoltaico):
        global id_aglomerado

        plt.figure(figsize=(9.0, 5.5))

        capex = list()
        opex = list()
        rotulos = list()

        # Legendas e Largura das Barras
        legenda_ = ['CAPEX', 'OPEX']
        line_width = 0.5
        bar_width = 1.5

        for c in cenarios:

            capex_macro_r = cenarios[c].capex_macro['Radio']
            capex_hetnet_r = cenarios[c].capex_hetnet['Radio']
            opex_macro_r = cenarios[c].opex_macro['Radio']
            opex_hetnet_r = cenarios[c].opex_hetnet['Radio']

            capex_macro_t = cenarios[c].capex_macro['Transporte']
            capex_hetnet_t = cenarios[c].capex_hetnet['Transporte']
            opex_macro_t = cenarios[c].opex_macro['Transporte']
            opex_hetnet_t = cenarios[c].opex_hetnet['Transporte']

            if sistema_fotovoltaico:
                capex_macro_f = cenarios[c].capex_macro['Fotovoltaico']
                capex_hetnet_f = cenarios[c].capex_hetnet['Fotovoltaico']
                opex_macro_f = cenarios[c].opex_macro['Fotovoltaico']
                opex_hetnet_f = cenarios[c].opex_hetnet['Fotovoltaico']
                custo_energia_minima_rede_m = cenarios[c].custo_energia_minima_rede['macro']
                custo_energia_minima_rede_h = cenarios[c].custo_energia_minima_rede['hetnet']
                custo_aluguel_fotovoltaico_m = cenarios[c].custo_aluguel_fotovoltaico['macro']
                custo_aluguel_fotovoltaico_h = cenarios[c].custo_aluguel_fotovoltaico['hetnet']

            capex_m = np.zeros(tempo_analise)
            capex_m += capex_macro_r['infraestrutura']
            capex_m += capex_macro_r['equipamentos']
            capex_m += capex_macro_r['instalacao']

            capex_m += capex_macro_t['infraestrutura']
            capex_m += capex_macro_t['equipamentos']
            capex_m += capex_macro_t['instalacao']

            capex_h = np.zeros(tempo_analise)
            capex_h += capex_hetnet_r['infraestrutura']
            capex_h += capex_hetnet_r['equipamentos']
            capex_h += capex_hetnet_r['instalacao']

            capex_h += capex_hetnet_t['infraestrutura']
            capex_h += capex_hetnet_t['equipamentos']
            capex_h += capex_hetnet_t['instalacao']

            opex_m = np.zeros(tempo_analise)
            if sistema_fotovoltaico:
                opex_m += custo_energia_minima_rede_m
                opex_m += custo_aluguel_fotovoltaico_m
            else:
                opex_m += opex_macro_r['energia']
                opex_m += opex_macro_t['energia']

            opex_m += opex_macro_r['aluguel']
            opex_m += opex_macro_t['aluguel']

            opex_m += opex_macro_r['manutencao']
            opex_m += opex_macro_r['falhas']

            opex_m += opex_macro_t['manutencao']
            opex_m += opex_macro_t['falhas']

            opex_h = np.zeros(tempo_analise)
            if sistema_fotovoltaico:
                opex_h += custo_energia_minima_rede_h
                opex_h += custo_aluguel_fotovoltaico_h
            else:
                opex_h += opex_hetnet_r['energia']
                opex_h += opex_hetnet_t['energia']

            opex_h += opex_hetnet_r['aluguel']
            opex_h += opex_hetnet_t['aluguel']

            opex_h += opex_hetnet_r['manutencao']
            opex_h += opex_hetnet_r['falhas']

            opex_h += opex_hetnet_t['manutencao']
            opex_h += opex_hetnet_t['falhas']

            if sistema_fotovoltaico:
                capex_m += list(capex_macro_f.values())
                capex_h += list(capex_hetnet_f.values())

                opex_fv_m = Util().subtracao_lista(list(opex_macro_f.values()), custo_energia_minima_rede_m)
                opex_fv_h = Util().subtracao_lista(list(opex_hetnet_f.values()), custo_energia_minima_rede_h)
                opex_m += Util().subtracao_lista(opex_fv_m, custo_aluguel_fotovoltaico_m)
                opex_h += Util().subtracao_lista(opex_fv_h, custo_aluguel_fotovoltaico_h)

            capex.append(capex_m.sum())
            capex.append(capex_h.sum())
            opex.append(opex_m.sum())
            opex.append(opex_h.sum())

            rotulos.append('Macro')
            rotulos.append('\n{}'.format(c))
            rotulos.append('Hetnet')

            if cenarios[c].tipo_cenario == 'Original':
                if sistema_fotovoltaico:
                    id_aglomerado = str(cenarios[c].id_agrupamento_territorial) + '_com_SF'
                else:
                    id_aglomerado = str(cenarios[c].id_agrupamento_territorial) + '_sem_SF'

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])

        plt.bar(posicao, capex, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, opex, bottom=capex, color='#cf4d4f', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        # Alterações nas propriedades dos Eixos X e Y
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda_, loc='best')
        plt.ylabel('{} TCO - Monetary Units ($)'.format(tipo_grafico))

        plt.savefig('{}TCO-{}-Simples-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def composicao_tco(self, cenarios, tipo_grafico, path_imagem):
        global id_aglomerado

        plt.figure(figsize=(9.0, 5.5))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Legendas e Largura das Barras
        line_width = 0.5
        bar_width = 1.5

        if tipo_grafico == 'Radio':
            legenda = ['Infra.', 'Equip.', 'Instl.', 'En.', 'Mnt.', 'FS', 'FM']
            label = 'RAN'
        else:
            legenda = ['Infra.', 'Equip.', 'Instl.', 'En.', 'Mnt.', 'SL', 'FM']
            label = 'Backhaul'

        for key in cenarios:
            capex_macro = cenarios[key].capex_macro[tipo_grafico]
            capex_hetnet = cenarios[key].capex_hetnet[tipo_grafico]
            opex_macro = cenarios[key].opex_macro[tipo_grafico]
            opex_hetnet = cenarios[key].opex_hetnet[tipo_grafico]

            infraestrutura.append(capex_macro['infraestrutura'].sum())
            infraestrutura.append(capex_hetnet['infraestrutura'].sum())

            equipamentos.append(capex_macro['equipamentos'].sum())
            equipamentos.append(capex_hetnet['equipamentos'].sum())

            instalacao.append(capex_macro['instalacao'].sum())
            instalacao.append(capex_hetnet['instalacao'].sum())

            energia.append(opex_macro['energia'].sum())
            energia.append(opex_hetnet['energia'].sum())

            manutencao.append(opex_macro['manutencao'].sum())
            manutencao.append(opex_hetnet['manutencao'].sum())

            aluguel.append(opex_macro['aluguel'].sum())
            aluguel.append(opex_hetnet['aluguel'].sum())

            falhas.append(opex_macro['falhas'].sum())
            falhas.append(opex_hetnet['falhas'].sum())

            rotulos.append('Macro')
            rotulos.append('\n{}'.format(key))
            rotulos.append('Hetnet')

            if cenarios[key].tipo_cenario == 'Original':
                id_aglomerado = cenarios[key].id_agrupamento_territorial

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])

        plt.bar(posicao, infraestrutura, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        plt.bar(posicao, equipamentos, bottom=infraestrutura, color='#cf4d4f', edgecolor='black', width=bar_width,
                zorder=3, linewidth=line_width)

        plt.bar(posicao, instalacao, bottom=[i + j for i, j in zip(infraestrutura, equipamentos)], color='#88a54f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, energia, bottom=[i + j + k for i, j, k in zip(infraestrutura, equipamentos, instalacao)],
                color='#72578f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, manutencao,
                bottom=[i + j + k + l for i, j, k, l in zip(infraestrutura, equipamentos, instalacao, energia)],
                color='#4298ae', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, aluguel, bottom=[i + j + k + l + m for i, j, k, l, m in
                                          zip(infraestrutura, equipamentos, instalacao, energia, manutencao)],
                color='#da8436', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, falhas, bottom=[i + j + k + l + m + n for i, j, k, l, m, n in
                                         zip(infraestrutura, equipamentos, instalacao, energia, manutencao, aluguel)],
                color='#93a9cf', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        # Custom X an Y axis
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda, loc='upper center', bbox_to_anchor=(0.5, 1.09), fancybox=True, shadow=True, ncol=7)
        plt.ylabel('{} TCO - Monetary Units ($)'.format(label))

        plt.savefig('{}TCO-{}-Composicao-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def composicao_tco_total_agrupamento(self, cenarios, tipo_grafico, path_imagem, sistema_fotovoltaico):
        global id_aglomerado

        plt.figure(figsize=(9.0, 5.5))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Legendas e Largura das Barras
        line_width = 0.5
        bar_width = 1.5

        legenda = ['Infra.', 'Equip.', 'Instl.', 'En.', 'Mnt.', 'FS&SL', 'FM']
        label = 'Rede'

        for key in cenarios:
            capex_macro_r = cenarios[key].capex_macro['Radio']
            capex_hetnet_r = cenarios[key].capex_hetnet['Radio']
            opex_macro_r = cenarios[key].opex_macro['Radio']
            opex_hetnet_r = cenarios[key].opex_hetnet['Radio']

            capex_macro_t = cenarios[key].capex_macro['Transporte']
            capex_hetnet_t = cenarios[key].capex_hetnet['Transporte']
            opex_macro_t = cenarios[key].opex_macro['Transporte']
            opex_hetnet_t = cenarios[key].opex_hetnet['Transporte']

            if sistema_fotovoltaico:
                capex_macro_f = cenarios[key].capex_macro['Fotovoltaico']
                capex_hetnet_f = cenarios[key].capex_hetnet['Fotovoltaico']
                opex_macro_f = cenarios[key].opex_macro['Fotovoltaico']
                opex_hetnet_f = cenarios[key].opex_hetnet['Fotovoltaico']
                custo_energia_minima_rede_m = sum(cenarios[key].custo_energia_minima_rede['macro'])
                custo_energia_minima_rede_h = sum(cenarios[key].custo_energia_minima_rede['hetnet'])
                custo_aluguel_fotovoltaico_m = sum(cenarios[key].custo_aluguel_fotovoltaico['macro'])
                custo_aluguel_fotovoltaico_h = sum(cenarios[key].custo_aluguel_fotovoltaico['hetnet'])

            capex_infra_m = capex_macro_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()
            capex_infra_h = capex_hetnet_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()

            infraestrutura.append(capex_infra_m)
            infraestrutura.append(capex_infra_h)

            capex_eq_m = capex_macro_r['equipamentos'].sum() + capex_macro_t['equipamentos'].sum()
            capex_eq_h = capex_hetnet_r['equipamentos'].sum() + capex_hetnet_t['equipamentos'].sum()
            if sistema_fotovoltaico:
                capex_eq_m += sum(list(capex_macro_f.values()))
                capex_eq_h += sum(list(capex_hetnet_f.values()))

            equipamentos.append(capex_eq_m)
            equipamentos.append(capex_eq_h)

            capex_inst_m = capex_macro_r['instalacao'].sum() + capex_macro_t['instalacao'].sum()
            capex_inst_h = capex_hetnet_r['instalacao'].sum() + capex_hetnet_t['instalacao'].sum()
            instalacao.append(capex_inst_m)
            instalacao.append(capex_inst_h)

            if sistema_fotovoltaico:
                opex_energia_m = custo_energia_minima_rede_m
                opex_energia_h = custo_energia_minima_rede_h
            else:
                opex_energia_m = opex_macro_r['energia'].sum() + opex_macro_t['energia'].sum()
                opex_energia_h = opex_hetnet_r['energia'].sum() + opex_hetnet_t['energia'].sum()

            energia.append(opex_energia_m)
            energia.append(opex_energia_h)

            opex_manut_m = opex_macro_r['manutencao'].sum() + opex_macro_t['manutencao'].sum()
            opex_manut_h = opex_hetnet_r['manutencao'].sum() + opex_hetnet_t['manutencao'].sum()

            if sistema_fotovoltaico:
                opex_fv_m = sum(
                    list(opex_macro_f.values())) - custo_energia_minima_rede_m - custo_aluguel_fotovoltaico_m
                opex_fv_h = sum(
                    list(opex_hetnet_f.values())) - custo_energia_minima_rede_h - custo_aluguel_fotovoltaico_h
                opex_manut_m += opex_fv_m
                opex_manut_h += opex_fv_h

            manutencao.append(opex_manut_m)
            manutencao.append(opex_manut_h)

            opex_al_m = opex_macro_r['aluguel'].sum() + opex_macro_t['aluguel'].sum()
            opex_al_h = opex_hetnet_r['aluguel'].sum() + opex_hetnet_t['aluguel'].sum()

            if sistema_fotovoltaico:
                opex_al_m += custo_aluguel_fotovoltaico_m
                opex_al_h += custo_aluguel_fotovoltaico_h

            aluguel.append(opex_al_m)
            aluguel.append(opex_al_h)

            opex_fl_m = opex_macro_r['falhas'].sum() + opex_macro_t['falhas'].sum()
            opex_fl_h = opex_hetnet_r['falhas'].sum() + opex_hetnet_t['falhas'].sum()
            falhas.append(opex_fl_m)
            falhas.append(opex_fl_h)

            rotulos.append('Macro')
            rotulos.append('\n{}'.format(key))
            rotulos.append('Hetnet')

            if cenarios[key].tipo_cenario == 'Original':
                if sistema_fotovoltaico:
                    id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_com_SF'
                else:
                    id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_sem_SF'

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])

        plt.bar(posicao, infraestrutura, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        plt.bar(posicao, equipamentos, bottom=infraestrutura, color='#cf4d4f', edgecolor='black', width=bar_width,
                zorder=3, linewidth=line_width)

        plt.bar(posicao, instalacao, bottom=[i + j for i, j in zip(infraestrutura, equipamentos)], color='#88a54f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, energia, bottom=[i + j + k for i, j, k in zip(infraestrutura, equipamentos, instalacao)],
                color='#72578f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, manutencao,
                bottom=[i + j + k + l for i, j, k, l in zip(infraestrutura, equipamentos, instalacao, energia)],
                color='#4298ae', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, aluguel, bottom=[i + j + k + l + m for i, j, k, l, m in
                                          zip(infraestrutura, equipamentos, instalacao, energia, manutencao)],
                color='#da8436', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, falhas, bottom=[i + j + k + l + m + n for i, j, k, l, m, n in
                                         zip(infraestrutura, equipamentos, instalacao, energia, manutencao, aluguel)],
                color='#93a9cf', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        # Custom X an Y axis
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda, loc='upper center', bbox_to_anchor=(0.5, 1.09), fancybox=True, shadow=True, ncol=7)
        plt.ylabel('{} TCO - Monetary Units ($)'.format(label))

        plt.savefig('{}TCO-{}-Composicao-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def composicao_tco_porcentagem(self, cenarios, tipo_grafico, path_imagem):
        global ax, id_aglomerado, mypie2

        fig, axes = plt.subplots(2, len(cenarios), figsize=(12, 8))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Cores dos Gráficos
        a, b = [plt.cm.Blues, plt.cm.Reds]

        # Grupos
        group_names = ['CAPEX', 'OPEX']

        # Subgrupos
        if tipo_grafico == 'Radio':
            subgroup_names = ['Equip.', 'Instl.', 'Infra.', 'Mnt.', 'FS', 'FM', 'En.']
            label = 'RAN'
        else:
            subgroup_names = ['Equip.', 'Instl.', 'Infra.', 'Mnt.', 'SL', 'FM', 'En.']
            label = 'Backhaul'

        for key in cenarios:
            capex_macro = cenarios[key].capex_macro[tipo_grafico]
            capex_hetnet = cenarios[key].capex_hetnet[tipo_grafico]
            opex_macro = cenarios[key].opex_macro[tipo_grafico]
            opex_hetnet = cenarios[key].opex_hetnet[tipo_grafico]

            infraestrutura.append(capex_macro['infraestrutura'].sum())
            infraestrutura.append(capex_hetnet['infraestrutura'].sum())

            equipamentos.append(capex_macro['equipamentos'].sum())
            equipamentos.append(capex_hetnet['equipamentos'].sum())

            instalacao.append(capex_macro['instalacao'].sum())
            instalacao.append(capex_hetnet['instalacao'].sum())

            energia.append(opex_macro['energia'].sum())
            energia.append(opex_hetnet['energia'].sum())

            manutencao.append(opex_macro['manutencao'].sum())
            manutencao.append(opex_hetnet['manutencao'].sum())

            aluguel.append(opex_macro['aluguel'].sum())
            aluguel.append(opex_hetnet['aluguel'].sum())

            falhas.append(opex_macro['falhas'].sum())
            falhas.append(opex_hetnet['falhas'].sum())

            rotulos.append('Macro-C' + str(cenarios[key].id_agrupamento_territorial))
            rotulos.append('Hetnet-C' + str(cenarios[key].id_agrupamento_territorial))

            if cenarios[key].tipo_cenario == 'Original':
                id_aglomerado = cenarios[key].id_agrupamento_territorial

        for index in range(len(infraestrutura)):
            soma = infraestrutura[index] + equipamentos[index] + instalacao[index] + energia[index] + \
                   manutencao[index] + aluguel[index] + falhas[index]
            infraestrutura[index] = (infraestrutura[index] / soma) * 100
            equipamentos[index] = (equipamentos[index] / soma) * 100
            instalacao[index] = (instalacao[index] / soma) * 100
            energia[index] = (energia[index] / soma) * 100
            manutencao[index] = (manutencao[index] / soma) * 100
            aluguel[index] = (aluguel[index] / soma) * 100
            falhas[index] = (falhas[index] / soma) * 100

        for i, ax in enumerate(axes.flatten()):
            group_size = [infraestrutura[i] + equipamentos[i] + instalacao[i],
                          energia[i] + manutencao[i] + aluguel[i] + falhas[i]]
            subgroup_size = [equipamentos[i], instalacao[i], infraestrutura[i], manutencao[i], aluguel[i], falhas[i],
                             energia[i]]

            ax.axis('equal')

            mypie, _ = ax.pie(group_size, radius=1.3, labels=group_names, colors=[a(0.8), b(0.8)], startangle=180)
            plt.setp(mypie, width=0.5, edgecolor='white')

            mypie2, plt_labels, junk = ax.pie(subgroup_size, radius=1.3 - 0.3, autopct='%1.1f%%', labeldistance=0.9,
                                              colors=[a(0.65), a(0.5), a(0.3), b(0.65), b(0.5), b(0.3), b(0.1)],
                                              startangle=180)
            plt.setp(mypie2, width=0.7, edgecolor='white')
            plt.margins(50, 50)
            ax.set_title(rotulos[i])
            plt.setp(junk, size=8, weight='bold')

        plt.legend(mypie2, subgroup_names, loc='center left', bbox_to_anchor=(1, 0, 0.5, 1))

        plt.savefig('{}TCO-{}-Porcentagem-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def composicao_tco_porcentagem_total_agrupamento(self, cenarios, tipo_grafico, path_imagem, sistema_fotovoltaico):
        global ax, id_aglomerado, mypie2

        fig, axes = plt.subplots(2, len(cenarios), figsize=(12, 8))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Cores dos Gráficos
        a, b = [plt.cm.Blues, plt.cm.Reds]

        # Grupos
        group_names = ['CAPEX', 'OPEX']

        # Subgrupos
        subgroup_names = ['Equip.', 'Instl.', 'Infra.', 'Mnt.', 'FS&SL', 'FM', 'En.']
        label = 'Rede'

        for key in cenarios:
            capex_macro_r = cenarios[key].capex_macro['Radio']
            capex_hetnet_r = cenarios[key].capex_hetnet['Radio']
            opex_macro_r = cenarios[key].opex_macro['Radio']
            opex_hetnet_r = cenarios[key].opex_hetnet['Radio']

            capex_macro_t = cenarios[key].capex_macro['Transporte']
            capex_hetnet_t = cenarios[key].capex_hetnet['Transporte']
            opex_macro_t = cenarios[key].opex_macro['Transporte']
            opex_hetnet_t = cenarios[key].opex_hetnet['Transporte']

            if sistema_fotovoltaico:
                capex_macro_f = cenarios[key].capex_macro['Fotovoltaico']
                capex_hetnet_f = cenarios[key].capex_hetnet['Fotovoltaico']
                opex_macro_f = cenarios[key].opex_macro['Fotovoltaico']
                opex_hetnet_f = cenarios[key].opex_hetnet['Fotovoltaico']
                custo_energia_minima_rede_m = sum(cenarios[key].custo_energia_minima_rede['macro'])
                custo_energia_minima_rede_h = sum(cenarios[key].custo_energia_minima_rede['hetnet'])
                custo_aluguel_fotovoltaico_m = sum(cenarios[key].custo_aluguel_fotovoltaico['macro'])
                custo_aluguel_fotovoltaico_h = sum(cenarios[key].custo_aluguel_fotovoltaico['hetnet'])

            capex_infra_m = capex_macro_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()
            capex_infra_h = capex_hetnet_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()

            infraestrutura.append(capex_infra_m)
            infraestrutura.append(capex_infra_h)

            capex_eq_m = capex_macro_r['equipamentos'].sum() + capex_macro_t['equipamentos'].sum()
            capex_eq_h = capex_hetnet_r['equipamentos'].sum() + capex_hetnet_t['equipamentos'].sum()
            if sistema_fotovoltaico:
                capex_eq_m += sum(list(capex_macro_f.values()))
                capex_eq_h += sum(list(capex_hetnet_f.values()))

            equipamentos.append(capex_eq_m)
            equipamentos.append(capex_eq_h)

            capex_inst_m = capex_macro_r['instalacao'].sum() + capex_macro_t['instalacao'].sum()
            capex_inst_h = capex_hetnet_r['instalacao'].sum() + capex_hetnet_t['instalacao'].sum()
            instalacao.append(capex_inst_m)
            instalacao.append(capex_inst_h)
            if sistema_fotovoltaico:
                opex_energia_m = custo_energia_minima_rede_m
                opex_energia_h = custo_energia_minima_rede_h
            else:
                opex_energia_m = opex_macro_r['energia'].sum() + opex_macro_t['energia'].sum()
                opex_energia_h = opex_hetnet_r['energia'].sum() + opex_hetnet_t['energia'].sum()

            energia.append(opex_energia_m)
            energia.append(opex_energia_h)

            opex_manut_m = opex_macro_r['manutencao'].sum() + opex_macro_t['manutencao'].sum()
            opex_manut_h = opex_hetnet_r['manutencao'].sum() + opex_hetnet_t['manutencao'].sum()

            if sistema_fotovoltaico:
                opex_fv_m = sum(
                    list(opex_macro_f.values())) - custo_energia_minima_rede_m - custo_aluguel_fotovoltaico_m
                opex_fv_h = sum(
                    list(opex_hetnet_f.values())) - custo_energia_minima_rede_h - custo_aluguel_fotovoltaico_h
                opex_manut_m += opex_fv_m
                opex_manut_h += opex_fv_h

            manutencao.append(opex_manut_m)
            manutencao.append(opex_manut_h)

            opex_al_m = opex_macro_r['aluguel'].sum() + opex_macro_t['aluguel'].sum()
            opex_al_h = opex_hetnet_r['aluguel'].sum() + opex_hetnet_t['aluguel'].sum()

            if sistema_fotovoltaico:
                opex_al_m += custo_aluguel_fotovoltaico_m
                opex_al_h += custo_aluguel_fotovoltaico_h

            aluguel.append(opex_al_m)
            aluguel.append(opex_al_h)

            opex_fl_m = opex_macro_r['falhas'].sum() + opex_macro_t['falhas'].sum()
            opex_fl_h = opex_hetnet_r['falhas'].sum() + opex_hetnet_t['falhas'].sum()
            falhas.append(opex_fl_m)
            falhas.append(opex_fl_h)

            rotulos.append('Macro-C' + str(cenarios[key].id_agrupamento_territorial))
            rotulos.append('Hetnet-C' + str(cenarios[key].id_agrupamento_territorial))

            if cenarios[key].tipo_cenario == 'Original':
                if sistema_fotovoltaico:
                    id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_com_SF'
                else:
                    id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_sem_SF'

        for index in range(len(infraestrutura)):
            soma = infraestrutura[index] + equipamentos[index] + instalacao[index] + energia[index] + \
                   manutencao[index] + aluguel[index] + falhas[index]
            infraestrutura[index] = (infraestrutura[index] / soma) * 100
            equipamentos[index] = (equipamentos[index] / soma) * 100
            instalacao[index] = (instalacao[index] / soma) * 100
            energia[index] = (energia[index] / soma) * 100
            manutencao[index] = (manutencao[index] / soma) * 100
            aluguel[index] = (aluguel[index] / soma) * 100
            falhas[index] = (falhas[index] / soma) * 100

        for i, ax in enumerate(axes.flatten()):
            group_size = [infraestrutura[i] + equipamentos[i] + instalacao[i],
                          energia[i] + manutencao[i] + aluguel[i] + falhas[i]]
            subgroup_size = [equipamentos[i], instalacao[i], infraestrutura[i], manutencao[i], aluguel[i], falhas[i],
                             energia[i]]

            ax.axis('equal')

            mypie, _ = ax.pie(group_size, radius=1.3, labels=group_names, colors=[a(0.8), b(0.8)], startangle=180)
            plt.setp(mypie, width=0.5, edgecolor='white')

            mypie2, plt_labels, junk = ax.pie(subgroup_size, radius=1.3 - 0.3, autopct='%1.1f%%', labeldistance=0.9,
                                              colors=[a(0.65), a(0.5), a(0.3), b(0.65), b(0.5), b(0.3), b(0.1)],
                                              startangle=180)
            plt.setp(mypie2, width=0.7, edgecolor='white')
            plt.margins(50, 50)
            ax.set_title(rotulos[i])
            plt.setp(junk, size=8, weight='bold')

        plt.legend(mypie2, subgroup_names, loc='center left', bbox_to_anchor=(1, 0, 0.5, 1))

        plt.savefig('{}TCO-{}-Porcentagem-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def evolucao_tco(self, cenarios, tipo_grafico, tempo_analise, path_imagem):
        global id_aglomerado

        chave_primeiro = list(cenarios)[0]
        tipo_rede_radio = ['Macro', 'Hetnet']

        # Legendas
        rotulos = np.arange(tempo_analise)
        legenda = list()

        posicao_macro = np.arange(0, 90, step=9)
        posicao_hetnet = posicao_macro + 2.3
        posicao_rotulos = (posicao_macro + posicao_hetnet) / 2

        bar_width = 2.0
        line_width = 0.6

        # Cores dos Gráficos
        a, b = [plt.cm.Blues, plt.cm.Reds]

        if tipo_grafico == 'Radio':
            label = 'RAN'
        elif tipo_grafico == 'Transporte':
            label = 'Backhaul'
        else:
            label = 'Photovoltaic'

        for key in cenarios:
            plt.figure(figsize=(8.0, 5.5))
            if tipo_grafico == 'Fotovoltaico':
                print()
            for tipo in tipo_rede_radio:
                if tipo == 'Macro':
                    posicao = posicao_macro
                    cor_capex = a(0.6)
                    cor_opex = b(0.6)
                    capex = cenarios[key].capex_macro[tipo_grafico]
                    opex = cenarios[key].opex_macro[tipo_grafico]
                else:
                    posicao = posicao_hetnet
                    cor_capex = a(0.3)
                    cor_opex = b(0.3)
                    capex = cenarios[key].capex_hetnet[tipo_grafico]
                    opex = cenarios[key].opex_hetnet[tipo_grafico]

                legenda.append('CAPEX {}'.format(tipo))
                legenda.append('OPEX {}'.format(tipo))
                if tipo_grafico != 'Fotovoltaico':
                    # tempo_analise = cenarios[key].tempo_analise
                    capex_ = np.zeros(tempo_analise)
                    capex_ += capex['infraestrutura']
                    capex_ += capex['equipamentos']
                    capex_ += capex['instalacao']

                    opex_ = np.zeros(tempo_analise)
                    opex_ += opex['energia']
                    opex_ += opex['manutencao']
                    opex_ += opex['aluguel']
                    opex_ += opex['falhas']
                else:
                    opex_ = list(opex.values())
                    capex_ = list(capex.values())

                plt.bar(posicao, capex_, color=cor_capex, width=bar_width, zorder=3, linewidth=line_width,
                        edgecolor='black')
                plt.bar(posicao, opex_, bottom=capex_, color=cor_opex, width=bar_width, zorder=3, linewidth=line_width,
                        edgecolor='black')

                id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_' + str(key)

            # Custom X axis
            plt.xticks(posicao_rotulos, rotulos)
            plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
            plt.legend(legenda, loc='best')
            plt.ylabel('{} TCO - Monetary Units ($)'.format(label))
            plt.xlabel('Units of Time (t)')

            plt.savefig('{}TCO-Evolucao-{}-{}.eps'.format(path_imagem, tipo_grafico, id_aglomerado),
                        dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
            plt.close()

    def evolucao_tco_total_agrupamento(self, cenarios, tempo_analise, path_imagem, sistema_fotovoltaico):
        global id_aglomerado

        tipo_rede_radio = ['Macro', 'Hetnet']

        # Legendas
        rotulos = np.arange(tempo_analise)
        legenda = list()

        posicao_macro = np.arange(0, 90, step=9)
        posicao_hetnet = posicao_macro + 2.3
        posicao_rotulos = (posicao_macro + posicao_hetnet) / 2

        bar_width = 2.0
        line_width = 0.6

        # Cores dos Gráficos
        a, b = [plt.cm.Blues, plt.cm.Reds]

        for key in cenarios:
            plt.figure(figsize=(8.0, 5.5))
            custo_energia_minima_rede = list()
            for tipo in tipo_rede_radio:
                capex = 0.0
                opex = 0.0
                if tipo == 'Macro':
                    posicao = posicao_macro
                    cor_capex = a(0.6)
                    cor_opex = b(0.6)
                    capex_radio = cenarios[key].capex_macro['Radio']
                    opex_radio = cenarios[key].opex_macro['Radio']
                    capex_transporte = cenarios[key].capex_macro['Transporte']
                    opex_transporte = cenarios[key].opex_macro['Transporte']
                    if sistema_fotovoltaico:
                        capex_fotovoltaico = cenarios[key].capex_macro['Fotovoltaico']
                        opex_fotovoltaico = cenarios[key].opex_macro['Fotovoltaico']
                        custo_energia_minima_rede = cenarios[key].custo_energia_minima_rede['macro']
                        custo_aluguel_fotovoltaico = cenarios[key].custo_aluguel_fotovoltaico['macro']
                else:
                    posicao = posicao_hetnet
                    cor_capex = a(0.3)
                    cor_opex = b(0.3)
                    capex_radio = cenarios[key].capex_hetnet['Radio']
                    opex_radio = cenarios[key].opex_hetnet['Radio']
                    capex_transporte = cenarios[key].capex_hetnet['Transporte']
                    opex_transporte = cenarios[key].opex_hetnet['Transporte']
                    if sistema_fotovoltaico:
                        capex_fotovoltaico = cenarios[key].capex_hetnet['Fotovoltaico']
                        opex_fotovoltaico = cenarios[key].opex_hetnet['Fotovoltaico']
                        custo_energia_minima_rede = cenarios[key].custo_energia_minima_rede['hetnet']
                        custo_aluguel_fotovoltaico = cenarios[key].custo_aluguel_fotovoltaico['hetnet']

                legenda.append('CAPEX {}'.format(tipo))
                legenda.append('OPEX {}'.format(tipo))

                capex_ = np.zeros(tempo_analise)
                capex_ += capex_radio['infraestrutura']
                capex_ += capex_radio['equipamentos']
                capex_ += capex_radio['instalacao']

                capex_ += capex_transporte['infraestrutura']
                capex_ += capex_transporte['equipamentos']
                capex_ += capex_transporte['instalacao']

                if sistema_fotovoltaico:
                    capex_ += list(capex_fotovoltaico.values())

                opex_ = np.zeros(tempo_analise)
                if sistema_fotovoltaico:
                    opex_ += custo_energia_minima_rede
                    opex_ += custo_aluguel_fotovoltaico
                else:
                    opex_ += opex_transporte['energia']
                    opex_ += opex_radio['energia']

                opex_ += opex_transporte['manutencao']
                opex_ += opex_transporte['aluguel']
                opex_ += opex_transporte['falhas']

                opex_ += opex_radio['manutencao']
                opex_ += opex_radio['aluguel']
                opex_ += opex_radio['falhas']

                if sistema_fotovoltaico:
                    opex_fv = Util().subtracao_lista(list(opex_fotovoltaico.values()), custo_energia_minima_rede)
                    opex_ += Util().subtracao_lista(opex_fv, custo_aluguel_fotovoltaico)

                plt.bar(posicao, capex_, color=cor_capex, width=bar_width, zorder=3, linewidth=line_width,
                        edgecolor='black')
                plt.bar(posicao, opex_, bottom=capex_, color=cor_opex, width=bar_width, zorder=3, linewidth=line_width,
                        edgecolor='black')
                if sistema_fotovoltaico:
                    id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_' + str(key) + '_com_SF'
                else:
                    id_aglomerado = str(cenarios[key].id_agrupamento_territorial) + '_' + str(key) + '_sem_SF'

            # Custom X axis
            plt.xticks(posicao_rotulos, rotulos)
            plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
            plt.legend(legenda, loc='best')
            plt.ylabel('TCO - Monetary Units ($)')
            plt.xlabel('Units of Time (t)')

            plt.savefig('{}TCO-RT-Evolucao-{}-{}.eps'.format(path_imagem, '', id_aglomerado),
                        dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
            plt.close()

    def npv_municipio(self, cenarios, path_imagem, sistema_fotvoltaico):

        global legenda

        # Inicialização
        names = list()
        valores_macro = list()
        valores_hetnet = list()

        # Visual do Gráfico
        bar_width = 1.5
        line_width = 0.6

        # Cores
        a, b = [plt.cm.Blues, plt.cm.Reds]

        # Posicao das Barras
        posicao_macro = np.array([0.0, 3.6, 7.2, 10.8])
        posicao_hetnet = np.array([1.6, 5.2, 8.8, 12.4])
        posicao_legenda = (posicao_macro + posicao_hetnet) / 2

        plt.figure()
        id_municipio =0
        for m in cenarios:
            id_municipio = cenarios[m].id_municipio
            if sistema_fotvoltaico:
                valores_macro.append(cenarios[m].npv_sf['Macro'].sum())
                valores_hetnet.append(cenarios[m].npv_sf['Hetnet'].sum())
            else:
                valores_macro.append(cenarios[m].npv['Macro'].sum())
                valores_hetnet.append(cenarios[m].npv['Hetnet'].sum())
            legenda = cenarios[m].tipos_rede_radio
            names.append(m)

        plt.bar(posicao_macro, valores_macro, color=a(0.6), width=bar_width, zorder=3, linewidth=line_width,
                edgecolor='black')
        plt.bar(posicao_hetnet, valores_hetnet, color=b(0.6), width=bar_width, zorder=3, linewidth=line_width,
                edgecolor='black')

        # Custom X axis
        plt.xticks(posicao_legenda, names)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda, loc='lower left')
        plt.ylabel('NPV - Moneraty Units ($)')

        if sistema_fotvoltaico:
            id_municipio = str(id_municipio)+'_com_SF'
        else:
            id_municipio = str(id_municipio)+'_sem_SF'

        # 2806
        # plt.ylim(0, 18.0)

        plt.savefig('{}NPV-{}.eps'.format(path_imagem, id_municipio), dpi=Path.RESOLUCAO_IMAGEM.valor,
                    bbox_inches='tight')
        plt.close()

    def fluxo_caixa_municipio(self, cenarios, path_imagem, sistema_fotvoltaico):

        # Inicialização
        names = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14']

        # Visual do Gráfico
        bar_width = 2.0
        line_width = 0.6

        # Cores
        a, b = [plt.cm.Blues, plt.cm.Reds]

        # Posicao das Barras
        posicao_macro = np.arange(0, 60, step=6)
        posicao_hetnet = posicao_macro + 2.3
        posicao_legenda = (posicao_macro + posicao_hetnet) / 2

        # Legenda
        legenda = ['Macro', 'HetNet']

        plt.figure()
        id_municipio = 0
        for m in cenarios:
            id_municipio = cenarios[m].id_municipio
            if sistema_fotvoltaico:
                valores_macro = cenarios[m].npv_sf[m].cf['Macro']
                valores_hetnet = cenarios[m].npv_sf[m].cf['Hetnet']
                income = cenarios[m].npv_sf[m].income
            else:
                valores_macro = cenarios[m].npv[m].cf['Macro']
                valores_hetnet = cenarios[m].npv[m].cf['Hetnet']
                income = cenarios[m].npv[m].income

            plt.bar(posicao_macro, valores_macro, color=a(0.6), width=bar_width, zorder=3, linewidth=line_width,
                    edgecolor='black')
            plt.bar(posicao_hetnet, valores_hetnet, color=b(0.6), width=bar_width, zorder=3, linewidth=line_width,
                    edgecolor='black')
            # plt.plot(posicao_legenda, income, '-*', label='Income', color='g')

            # Custom X axis
            plt.xticks(posicao_legenda, names)
            plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
            plt.legend(legenda, loc='best')
            plt.ylabel('CF - Moneraty Units ($)')
            plt.xlabel('Units od Time (t)')

            if sistema_fotvoltaico:
                id_municipio = str(id_municipio)+'_com_SF'
            else:
                id_municipio = str(id_municipio)+'_sem_SF'

            # 2806
            plt.ylim(-4.1, 3.1)
            # plt.show()
            plt.savefig('{}CF-{}-{}.eps'.format(path_imagem, id_municipio, m), dpi=Path.RESOLUCAO_IMAGEM.valor,
                        bbox_inches='tight')
            plt.close()

    def tco_municipio(self, municipios):
        global legenda

        # Inicialização
        names = list()
        valores_macro = list()
        valores_hetnet = list()

        # Visual do Gráfico
        bar_width = 1.5
        line_width = 0.6

        # Cores
        a, b = [plt.cm.Blues, plt.cm.Reds]

        # Posicao das Barras
        posicao_macro = np.array([0.0, 3.6, 7.2, 10.8])
        posicao_hetnet = np.array([1.6, 5.2, 8.8, 12.4])
        posicao_legenda = (posicao_macro + posicao_hetnet) / 2

        plt.figure()

        for m in municipios:
            valores_macro.append(municipios[m].tco['Macro'].sum())
            valores_hetnet.append(municipios[m].tco['Hetnet'].sum())
            legenda = municipios[m].municipio.tipos_rede_radio
            names.append(m)

        plt.bar(posicao_macro, valores_macro, color=a(0.6), width=bar_width, zorder=3, linewidth=line_width,
                edgecolor='black')
        plt.bar(posicao_hetnet, valores_hetnet, color=b(0.6), width=bar_width, zorder=3, linewidth=line_width,
                edgecolor='black')

        # Custom X axis
        plt.xticks(posicao_legenda, names)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda, loc='best')
        plt.ylabel('TCO (Unidades Monetárias $)')

        plt.savefig('{}TCO.eps'.format(Path.DIRETORIO_IMAGEM_MUNICIPIO.valor), dpi=Path.RESOLUCAO_IMAGEM.valor,
                    bbox_inches='tight')
        plt.close()

    def tco_simples_municipio(self, cenarios, tipo_grafico, tempo_analise, path_imagem, sistema_fotovoltaico):

        global id_municipio

        plt.figure(figsize=(9.0, 5.5))

        capex = list()
        opex = list()
        rotulos = list()

        # Legendas e Largura das Barras
        legenda_ = ['CAPEX', 'OPEX']
        line_width = 0.5
        bar_width = 1.5

        label = 'Rede'

        for c in cenarios:
            capex_m = np.zeros(tempo_analise)
            capex_h = np.zeros(tempo_analise)
            opex_m = np.zeros(tempo_analise)
            opex_h = np.zeros(tempo_analise)

            for ag in cenarios[c].agrupamentos_territoriais:
                id_municipio = ag.municipio
                capex_macro_r = ag.capex_macro['Radio']
                capex_hetnet_r = ag.capex_hetnet['Radio']
                opex_macro_r = ag.opex_macro['Radio']
                opex_hetnet_r = ag.opex_hetnet['Radio']

                capex_macro_t = ag.capex_macro['Transporte']
                capex_hetnet_t = ag.capex_hetnet['Transporte']
                opex_macro_t = ag.opex_macro['Transporte']
                opex_hetnet_t = ag.opex_hetnet['Transporte']

                if sistema_fotovoltaico:
                    capex_macro_f = ag.capex_macro['Fotovoltaico']
                    capex_hetnet_f = ag.capex_hetnet['Fotovoltaico']
                    opex_macro_f = ag.opex_macro['Fotovoltaico']
                    opex_hetnet_f = ag.opex_hetnet['Fotovoltaico']
                    custo_energia_minima_rede_m = ag.custo_energia_minima_rede['macro']
                    custo_energia_minima_rede_h = ag.custo_energia_minima_rede['hetnet']
                    custo_aluguel_fotovoltaico_m = ag.custo_aluguel_fotovoltaico['macro']
                    custo_aluguel_fotovoltaico_h = ag.custo_aluguel_fotovoltaico['hetnet']

                capex_m += capex_macro_r['infraestrutura']
                capex_m += capex_macro_r['equipamentos']
                capex_m += capex_macro_r['instalacao']

                capex_m += capex_macro_t['infraestrutura']
                capex_m += capex_macro_t['equipamentos']
                capex_m += capex_macro_t['instalacao']

                capex_h += capex_hetnet_r['infraestrutura']
                capex_h += capex_hetnet_r['equipamentos']
                capex_h += capex_hetnet_r['instalacao']

                capex_h += capex_hetnet_t['infraestrutura']
                capex_h += capex_hetnet_t['equipamentos']
                capex_h += capex_hetnet_t['instalacao']

                if sistema_fotovoltaico:
                    opex_m += custo_energia_minima_rede_m
                    opex_h += custo_energia_minima_rede_h
                    opex_m += custo_aluguel_fotovoltaico_m
                    opex_h += custo_aluguel_fotovoltaico_h
                else:
                    opex_m += opex_macro_r['energia']
                    opex_m += opex_macro_t['energia']
                    opex_h += opex_hetnet_r['energia']
                    opex_h += opex_hetnet_t['energia']

                opex_m += opex_macro_r['manutencao']
                opex_m += opex_macro_r['aluguel']
                opex_m += opex_macro_r['falhas']

                opex_m += opex_macro_t['manutencao']
                opex_m += opex_macro_t['aluguel']
                opex_m += opex_macro_t['falhas']

                opex_h += opex_hetnet_r['manutencao']
                opex_h += opex_hetnet_r['aluguel']
                opex_h += opex_hetnet_r['falhas']

                opex_h += opex_hetnet_t['manutencao']
                opex_h += opex_hetnet_t['aluguel']
                opex_h += opex_hetnet_t['falhas']
                if sistema_fotovoltaico:
                    capex_m += list(capex_macro_f.values())
                    capex_h += list(capex_hetnet_f.values())
                    opex_fv_m = Util().subtracao_lista(list(opex_macro_f.values()), custo_energia_minima_rede_m)
                    opex_fv_h = Util().subtracao_lista(list(opex_hetnet_f.values()), custo_energia_minima_rede_h)
                    opex_m += Util().subtracao_lista(opex_fv_m, custo_aluguel_fotovoltaico_m)
                    opex_h += Util().subtracao_lista(opex_fv_h, custo_aluguel_fotovoltaico_h)


            #central office
            capex_m += cenarios[c].capex_co['infraestrutura']
            capex_m += cenarios[c].capex_co['equipamentos']
            capex_m += cenarios[c].capex_co['instalacao']

            if sistema_fotovoltaico:
                opex_m += cenarios[c].custo_energia_minima_rede_co
                capex_m += list(cenarios[c].capex_fotovoltaico_co.values())
                opex_fv_co = Util().subtracao_lista(list(cenarios[c].opex_fotovoltaico_co.values()), cenarios[c].custo_energia_minima_rede_co)
                opex_m += opex_fv_co
                opex_h += opex_fv_co
                capex_h += list(cenarios[c].capex_fotovoltaico_co.values())
            else:
                opex_m += cenarios[c].opex_co['energia']
                opex_h += cenarios[c].opex_co['energia']

            opex_m += cenarios[c].opex_co['manutencao']
            opex_m += cenarios[c].opex_co['aluguel']
            opex_m += cenarios[c].opex_co['falhas']

            capex_h += cenarios[c].capex_co['infraestrutura']
            capex_h += cenarios[c].capex_co['equipamentos']
            capex_h += cenarios[c].capex_co['instalacao']

            opex_h += cenarios[c].opex_co['manutencao']
            opex_h += cenarios[c].opex_co['aluguel']
            opex_h += cenarios[c].opex_co['falhas']

            capex.append(capex_m.sum())
            capex.append(capex_h.sum())
            opex.append(opex_m.sum())
            opex.append(opex_h.sum())

            rotulos.append('Macro')
            rotulos.append('\n{}'.format(c))
            rotulos.append('Hetnet')

            if sistema_fotovoltaico:
                id_municipio = str(id_municipio) + '_com_SF'
            else:
                id_municipio = str(id_municipio) + '_sem_SF'

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])

        plt.bar(posicao, capex, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, opex, bottom=capex, color='#cf4d4f', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        # Alterações nas propriedades dos Eixos X e Y
        plt.ylim(0, 35)
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda_, loc='best')
        plt.ylabel('TCO - Monetary Units ($)'.format(label))

        plt.savefig('{}TCO-{}-Simples-{}.eps'.format(path_imagem, tipo_grafico, id_municipio),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def composicao_tco_municipio(self, cenarios, tipo_grafico, tempo_analise, path_imagem, sistema_fotovoltaico):
        global id_municipio

        plt.figure(figsize=(9.0, 5.5))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Legendas e Largura das Barras
        line_width = 0.5
        bar_width = 1.5

        legenda = ['Infra.', 'Equip.', 'Instl.', 'En.', 'Mnt.', 'FS&SL.', 'FM']
        label = 'Rede'
        for key in cenarios:
            capex_infra_m = 0.0
            capex_infra_h = 0.0
            capex_eq_m = 0.0
            capex_eq_h = 0.0
            capex_inst_m = 0.0
            capex_inst_h = 0.0
            opex_energia_m = 0.0
            opex_energia_h = 0.0
            opex_manut_m = 0.0
            opex_manut_h = 0.0
            opex_al_m = 0.0
            opex_al_h = 0.0
            opex_fl_m = 0.0
            opex_fl_h = 0.0

            for ag in cenarios[key].agrupamentos_territoriais:
                id_municipio = ag.municipio

                capex_macro_r = ag.capex_macro['Radio']
                capex_hetnet_r = ag.capex_hetnet['Radio']
                opex_macro_r = ag.opex_macro['Radio']
                opex_hetnet_r = ag.opex_hetnet['Radio']

                capex_macro_t = ag.capex_macro['Transporte']
                capex_hetnet_t = ag.capex_hetnet['Transporte']
                opex_macro_t = ag.opex_macro['Transporte']
                opex_hetnet_t = ag.opex_hetnet['Transporte']

                if sistema_fotovoltaico:
                    capex_macro_f = ag.capex_macro['Fotovoltaico']
                    capex_hetnet_f = ag.capex_hetnet['Fotovoltaico']
                    opex_macro_f = ag.opex_macro['Fotovoltaico']
                    opex_hetnet_f = ag.opex_hetnet['Fotovoltaico']
                    custo_energia_minima_rede_m = sum(ag.custo_energia_minima_rede['macro'])
                    custo_energia_minima_rede_h = sum(ag.custo_energia_minima_rede['hetnet'])
                    custo_aluguel_fotovoltaico_m = sum(ag.custo_aluguel_fotovoltaico['macro'])
                    custo_aluguel_fotovoltaico_h = sum(ag.custo_aluguel_fotovoltaico['hetnet'])

                capex_infra_m += capex_macro_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()
                capex_infra_h += capex_hetnet_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()

                capex_eq_m += capex_macro_r['equipamentos'].sum() + capex_macro_t['equipamentos'].sum()
                capex_eq_h += capex_hetnet_r['equipamentos'].sum() + capex_hetnet_t['equipamentos'].sum()
                if sistema_fotovoltaico:
                    capex_eq_m += sum(list(capex_macro_f.values()))
                    capex_eq_h += sum(list(capex_hetnet_f.values()))

                capex_inst_m += capex_macro_r['instalacao'].sum() + capex_macro_t['instalacao'].sum()
                capex_inst_h += capex_hetnet_r['instalacao'].sum() + capex_hetnet_t['instalacao'].sum()

                if sistema_fotovoltaico:
                    opex_energia_m += custo_energia_minima_rede_m
                    opex_energia_h += custo_energia_minima_rede_h
                else:
                    opex_energia_m += opex_macro_r['energia'].sum() + opex_macro_t['energia'].sum()
                    opex_energia_h += opex_hetnet_r['energia'].sum() + opex_hetnet_t['energia'].sum()

                opex_manut_m += opex_macro_r['manutencao'].sum() + opex_macro_t['manutencao'].sum()
                opex_manut_h += opex_hetnet_r['manutencao'].sum() + opex_hetnet_t['manutencao'].sum()

                if sistema_fotovoltaico:
                    opex_fv_m = sum(
                        list(opex_macro_f.values())) - custo_energia_minima_rede_m - custo_aluguel_fotovoltaico_m
                    opex_fv_h = sum(
                        list(opex_hetnet_f.values())) - custo_energia_minima_rede_h - custo_aluguel_fotovoltaico_h
                    opex_manut_m += opex_fv_m
                    opex_manut_h += opex_fv_h
                    # aluguel
                    opex_al_m += custo_aluguel_fotovoltaico_m
                    opex_al_h += custo_aluguel_fotovoltaico_h

                opex_al_m += opex_macro_r['aluguel'].sum() + opex_macro_t['aluguel'].sum()
                opex_al_h += opex_hetnet_r['aluguel'].sum() + opex_hetnet_t['aluguel'].sum()

                opex_fl_m += opex_macro_r['falhas'].sum() + opex_macro_t['falhas'].sum()
                opex_fl_h += opex_hetnet_r['falhas'].sum() + opex_hetnet_t['falhas'].sum()

            # central office
            capex_infra_m += cenarios[key].capex_co['infraestrutura'].sum()
            capex_eq_m += cenarios[key].capex_co['equipamentos'].sum()
            capex_inst_m += cenarios[key].capex_co['instalacao'].sum()

            if sistema_fotovoltaico:
                opex_energia_m += sum(cenarios[key].custo_energia_minima_rede_co)
                opex_energia_h += sum(cenarios[key].custo_energia_minima_rede_co)
            else:
                opex_energia_m += cenarios[key].opex_co['energia'].sum()
                opex_energia_h += cenarios[key].opex_co['energia'].sum()

            opex_manut_m += cenarios[key].opex_co['manutencao'].sum()
            opex_al_m += cenarios[key].opex_co['aluguel'].sum()
            opex_fl_m += cenarios[key].opex_co['falhas'].sum()

            capex_infra_h += cenarios[key].capex_co['infraestrutura'].sum()
            capex_eq_h += cenarios[key].capex_co['equipamentos'].sum()

            if sistema_fotovoltaico:
                capex_eq_m += sum(list(cenarios[key].capex_fotovoltaico_co.values()))
                capex_eq_h += sum(list(cenarios[key].capex_fotovoltaico_co.values()))

            capex_inst_h += cenarios[key].capex_co['instalacao'].sum()

            if sistema_fotovoltaico:
                opex_fv_co = sum(
                    list(cenarios[key].opex_fotovoltaico_co.values())) - sum(cenarios[key].custo_energia_minima_rede_co) \
                            - sum(cenarios[key].custo_aluguel_fotovoltaico_co)

                opex_manut_m += opex_fv_co
                opex_manut_h += opex_fv_co
                # aluguel
                opex_al_m += sum(cenarios[key].custo_aluguel_fotovoltaico_co)
                opex_al_h += sum(cenarios[key].custo_aluguel_fotovoltaico_co)

            opex_manut_h += cenarios[key].opex_co['manutencao'].sum()
            opex_al_h += cenarios[key].opex_co['aluguel'].sum()
            opex_fl_h += cenarios[key].opex_co['falhas'].sum()

            infraestrutura.append(capex_infra_m)
            infraestrutura.append(capex_infra_h)

            equipamentos.append(capex_eq_m)
            equipamentos.append(capex_eq_h)

            instalacao.append(capex_inst_m)
            instalacao.append(capex_inst_h)

            energia.append(opex_energia_m)
            energia.append(opex_energia_h)

            manutencao.append(opex_manut_m)
            manutencao.append(opex_manut_h)

            aluguel.append(opex_al_m)
            aluguel.append(opex_al_h)

            falhas.append(opex_fl_m)
            falhas.append(opex_fl_h)

        rotulos.append('Macro')
        rotulos.append('\n{}'.format(key))
        rotulos.append('Hetnet')

        if sistema_fotovoltaico:
            id_municipio = str(id_municipio) + '_com_SF'
        else:
            id_municipio = str(id_municipio) + '_sem_SF'

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])

        plt.bar(posicao, infraestrutura, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        plt.bar(posicao, equipamentos, bottom=infraestrutura, color='#cf4d4f', edgecolor='black', width=bar_width,
                zorder=3, linewidth=line_width)

        plt.bar(posicao, instalacao, bottom=[i + j for i, j in zip(infraestrutura, equipamentos)], color='#88a54f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, energia, bottom=[i + j + k for i, j, k in zip(infraestrutura, equipamentos, instalacao)],
                color='#72578f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, manutencao,
                bottom=[i + j + k + l for i, j, k, l in zip(infraestrutura, equipamentos, instalacao, energia)],
                color='#4298ae', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, aluguel, bottom=[i + j + k + l + m for i, j, k, l, m in
                                          zip(infraestrutura, equipamentos, instalacao, energia, manutencao)],
                color='#da8436', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, falhas, bottom=[i + j + k + l + m + n for i, j, k, l, m, n in
                                         zip(infraestrutura, equipamentos, instalacao, energia, manutencao,
                                             aluguel)],
                color='#93a9cf', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        # Custom X an Y axis
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda, loc='upper center', bbox_to_anchor=(0.5, 1.09), fancybox=True, shadow=True, ncol=7)
        plt.ylabel('{} TCO - Monetary Units ($)'.format(label))

        plt.savefig('{}TCO-{}-Composicao-{}.eps'.format(path_imagem, tipo_grafico, id_municipio),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def composicao_tco_porcentagem_municipio(self, cenarios, tipo_grafico, tempo_analise, path_imagem,
                                             sistema_fotovoltaico):
        global ax, id_municipio, mypie2

        fig, axes = plt.subplots(2, len(cenarios), figsize=(12, 8))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Cores dos Gráficos
        a, b = [plt.cm.Blues, plt.cm.Reds]

        # Grupos
        group_names = ['CAPEX', 'OPEX']

        # Subgrupos
        subgroup_names = ['Equip.', 'Instl.', 'Infra.', 'Mnt.', 'FS&SL', 'FM', 'En.']
        label = 'Rede'

        for key in cenarios:
            capex_infra_m = 0.0
            capex_infra_h = 0.0
            capex_eq_m = 0.0
            capex_eq_h = 0.0
            capex_inst_m = 0.0
            capex_inst_h = 0.0
            opex_energia_m = 0.0
            opex_energia_h = 0.0
            opex_manut_m = 0.0
            opex_manut_h = 0.0
            opex_al_m = 0.0
            opex_al_h = 0.0
            opex_fl_m = 0.0
            opex_fl_h = 0.0

            for ag in cenarios[key].agrupamentos_territoriais:
                id_municipio = ag.municipio

                capex_macro_r = ag.capex_macro['Radio']
                capex_hetnet_r = ag.capex_hetnet['Radio']
                opex_macro_r = ag.opex_macro['Radio']
                opex_hetnet_r = ag.opex_hetnet['Radio']

                capex_macro_t = ag.capex_macro['Transporte']
                capex_hetnet_t = ag.capex_hetnet['Transporte']
                opex_macro_t = ag.opex_macro['Transporte']
                opex_hetnet_t = ag.opex_hetnet['Transporte']

                if sistema_fotovoltaico:
                    capex_macro_f = ag.capex_macro['Fotovoltaico']
                    capex_hetnet_f = ag.capex_hetnet['Fotovoltaico']
                    opex_macro_f = ag.opex_macro['Fotovoltaico']
                    opex_hetnet_f = ag.opex_hetnet['Fotovoltaico']
                    custo_energia_minima_rede_m = sum(ag.custo_energia_minima_rede['macro'])
                    custo_energia_minima_rede_h = sum(ag.custo_energia_minima_rede['hetnet'])
                    custo_aluguel_fotovoltaico_m = sum(ag.custo_aluguel_fotovoltaico['macro'])
                    custo_aluguel_fotovoltaico_h = sum(ag.custo_aluguel_fotovoltaico['hetnet'])

                capex_infra_m += capex_macro_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()
                capex_infra_h += capex_hetnet_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()

                capex_eq_m += capex_macro_r['equipamentos'].sum() + capex_macro_t['equipamentos'].sum()
                capex_eq_h += capex_hetnet_r['equipamentos'].sum() + capex_hetnet_t['equipamentos'].sum()
                if sistema_fotovoltaico:
                    capex_eq_m += sum(list(capex_macro_f.values()))
                    capex_eq_h += sum(list(capex_hetnet_f.values()))

                capex_inst_m += capex_macro_r['instalacao'].sum() + capex_macro_t['instalacao'].sum()
                capex_inst_h += capex_hetnet_r['instalacao'].sum() + capex_hetnet_t['instalacao'].sum()

                if sistema_fotovoltaico:
                    opex_energia_m += custo_energia_minima_rede_m
                    opex_energia_h += custo_energia_minima_rede_h
                else:
                    opex_energia_m += opex_macro_r['energia'].sum() + opex_macro_t['energia'].sum()
                    opex_energia_h += opex_hetnet_r['energia'].sum() + opex_hetnet_t['energia'].sum()

                opex_manut_m += opex_macro_r['manutencao'].sum() + opex_macro_t['manutencao'].sum()
                opex_manut_h += opex_hetnet_r['manutencao'].sum() + opex_hetnet_t['manutencao'].sum()

                if sistema_fotovoltaico:
                    opex_fv_m = sum(
                        list(opex_macro_f.values())) - custo_energia_minima_rede_m - custo_aluguel_fotovoltaico_m
                    opex_fv_h = sum(
                        list(opex_hetnet_f.values())) - custo_energia_minima_rede_h - custo_aluguel_fotovoltaico_h
                    opex_manut_m += opex_fv_m
                    opex_manut_h += opex_fv_h
                    # aluguel
                    opex_al_m += custo_aluguel_fotovoltaico_m
                    opex_al_h += custo_aluguel_fotovoltaico_h

                opex_al_m += opex_macro_r['aluguel'].sum() + opex_macro_t['aluguel'].sum()
                opex_al_h += opex_hetnet_r['aluguel'].sum() + opex_hetnet_t['aluguel'].sum()

                opex_fl_m += opex_macro_r['falhas'].sum() + opex_macro_t['falhas'].sum()
                opex_fl_h += opex_hetnet_r['falhas'].sum() + opex_hetnet_t['falhas'].sum()

            # central office
            capex_infra_m += cenarios[key].capex_co['infraestrutura'].sum()
            capex_eq_m += cenarios[key].capex_co['equipamentos'].sum()
            capex_inst_m += cenarios[key].capex_co['instalacao'].sum()

            if sistema_fotovoltaico:
                opex_energia_m += sum(cenarios[key].custo_energia_minima_rede_co)
                opex_energia_h += sum(cenarios[key].custo_energia_minima_rede_co)
            else:
                opex_energia_m += cenarios[key].opex_co['energia'].sum()
                opex_energia_h += cenarios[key].opex_co['energia'].sum()

            opex_manut_m += cenarios[key].opex_co['manutencao'].sum()
            opex_al_m += cenarios[key].opex_co['aluguel'].sum()
            opex_fl_m += cenarios[key].opex_co['falhas'].sum()

            capex_infra_h += cenarios[key].capex_co['infraestrutura'].sum()
            capex_eq_h += cenarios[key].capex_co['equipamentos'].sum()

            if sistema_fotovoltaico:
                capex_eq_m += sum(list(cenarios[key].capex_fotovoltaico_co.values()))
                capex_eq_h += sum(list(cenarios[key].capex_fotovoltaico_co.values()))

            capex_inst_h += cenarios[key].capex_co['instalacao'].sum()

            if sistema_fotovoltaico:
                opex_fv_co = sum(
                    list(cenarios[key].opex_fotovoltaico_co.values())) - sum(cenarios[key].custo_energia_minima_rede_co) \
                             - sum(cenarios[key].custo_aluguel_fotovoltaico_co)

                opex_manut_m += opex_fv_co
                opex_manut_h += opex_fv_co
                # aluguel
                opex_al_m += sum(cenarios[key].custo_aluguel_fotovoltaico_co)
                opex_al_h += sum(cenarios[key].custo_aluguel_fotovoltaico_co)

            opex_manut_h += cenarios[key].opex_co['manutencao'].sum()
            opex_al_h += cenarios[key].opex_co['aluguel'].sum()
            opex_fl_h += cenarios[key].opex_co['falhas'].sum()

            infraestrutura.append(capex_infra_m)
            infraestrutura.append(capex_infra_h)

            equipamentos.append(capex_eq_m)
            equipamentos.append(capex_eq_h)

            instalacao.append(capex_inst_m)
            instalacao.append(capex_inst_h)

            energia.append(opex_energia_m)
            energia.append(opex_energia_h)

            manutencao.append(opex_manut_m)
            manutencao.append(opex_manut_h)

            aluguel.append(opex_al_m)
            aluguel.append(opex_al_h)

            falhas.append(opex_fl_m)
            falhas.append(opex_fl_h)

            rotulos.append('Macro-C' + str(id_municipio))
            rotulos.append('Hetnet-C' + str(id_municipio))

        if sistema_fotovoltaico:
            id_municipio = str(id_municipio) + '_com_SF'
        else:
            id_municipio = str(id_municipio) + '_sem_SF'

        for index in range(len(infraestrutura)):
            soma = infraestrutura[index] + equipamentos[index] + instalacao[index] + energia[index] + \
                   manutencao[index] + aluguel[index] + falhas[index]
            infraestrutura[index] = (infraestrutura[index] / soma) * 100
            equipamentos[index] = (equipamentos[index] / soma) * 100
            instalacao[index] = (instalacao[index] / soma) * 100
            energia[index] = (energia[index] / soma) * 100
            manutencao[index] = (manutencao[index] / soma) * 100
            aluguel[index] = (aluguel[index] / soma) * 100
            falhas[index] = (falhas[index] / soma) * 100

        for i, ax in enumerate(axes.flatten()):
            group_size = [infraestrutura[i] + equipamentos[i] + instalacao[i],
                          energia[i] + manutencao[i] + aluguel[i] + falhas[i]]
            subgroup_size = [equipamentos[i], instalacao[i], infraestrutura[i], manutencao[i], aluguel[i],
                             falhas[i],
                             energia[i]]

            ax.axis('equal')

            mypie, _ = ax.pie(group_size, radius=1.3, labels=group_names, colors=[a(0.8), b(0.8)], startangle=180)
            plt.setp(mypie, width=0.5, edgecolor='white')

            mypie2, plt_labels, junk = ax.pie(subgroup_size, radius=1.3 - 0.3, autopct='%1.1f%%', labeldistance=0.9,
                                              colors=[a(0.65), a(0.5), a(0.3), b(0.65), b(0.5), b(0.3), b(0.1)],
                                              startangle=180)
            plt.setp(mypie2, width=0.7, edgecolor='white')
            plt.margins(50, 50)
            ax.set_title(rotulos[i])
            plt.setp(junk, size=8, weight='bold')

        plt.legend(mypie2, subgroup_names, loc='center left', bbox_to_anchor=(1, 0, 0.5, 1))

        plt.savefig('{}TCO-{}-Porcentagem-{}.eps'.format(path_imagem, tipo_grafico, id_municipio),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()

    def evolucao_tco_municipio(self, cenarios, tempo_analise, path_imagem, sistema_fotovoltaico):
        global id_municipio

        tipo_rede_radio = ['Macro', 'Hetnet']

        # Legendas
        rotulos = np.arange(tempo_analise)
        legenda = list()
        #posicao_macro = np.arange(0, 90, step=9)
        posicao_macro = np.arange(0, 60, step=6)
        posicao_hetnet = posicao_macro + 2.3
        posicao_rotulos = (posicao_macro + posicao_hetnet) / 2

        bar_width = 2.0
        line_width = 0.6

        # Cores dos Gráficos
        a, b = [plt.cm.Blues, plt.cm.Reds]

        for key in cenarios:
            plt.figure(figsize=(8.0, 5.5))
            capex_m = 0.0
            capex_h = 0.0
            opex_m = 0.0
            opex_h = 0.0

            for ag in cenarios[key].agrupamentos_territoriais:
                id_municipio = ag.municipio
                cor_capex_m = a(0.6)
                cor_opex_m = b(0.6)
                capex_radio = ag.capex_macro['Radio']
                opex_radio = ag.opex_macro['Radio']
                capex_transporte = ag.capex_macro['Transporte']
                opex_transporte = ag.opex_macro['Transporte']
                if sistema_fotovoltaico:
                    capex_fotovoltaico = ag.capex_macro['Fotovoltaico']
                    opex_fotovoltaico = ag.opex_macro['Fotovoltaico']
                    custo_energia_minima_rede_m = ag.custo_energia_minima_rede['macro']
                    custo_aluguel_fotovoltaico_m = ag.custo_aluguel_fotovoltaico['macro']

                legenda.append('CAPEX {}'.format('Macro'))
                legenda.append('OPEX {}'.format('Macro'))

                capex_m += capex_radio['infraestrutura']
                capex_m += capex_radio['equipamentos']
                capex_m += capex_radio['instalacao']

                capex_m += capex_transporte['infraestrutura']
                capex_m += capex_transporte['equipamentos']
                capex_m += capex_transporte['instalacao']

                if sistema_fotovoltaico:
                    capex_m += list(capex_fotovoltaico.values())
                    opex_m += sum(custo_energia_minima_rede_m)
                    opex_m += sum(custo_aluguel_fotovoltaico_m)
                else:
                    opex_m += opex_transporte['energia']
                    opex_m += opex_radio['energia']

                opex_m += opex_radio['manutencao']
                opex_m += opex_radio['aluguel']
                opex_m += opex_radio['falhas']

                opex_m += opex_transporte['manutencao']
                opex_m += opex_transporte['aluguel']
                opex_m += opex_transporte['falhas']

                if sistema_fotovoltaico:
                    opex_fv_m = Util().subtracao_lista(list(opex_fotovoltaico.values()), custo_energia_minima_rede_m)
                    opex_m += Util().subtracao_lista(opex_fv_m, custo_aluguel_fotovoltaico_m)

                cor_capex_h = a(0.3)
                cor_opex_h = b(0.3)
                capex_radio = ag.capex_hetnet['Radio']
                opex_radio = ag.opex_hetnet['Radio']
                capex_transporte = ag.capex_hetnet['Transporte']
                opex_transporte = ag.opex_hetnet['Transporte']
                if sistema_fotovoltaico:
                    capex_fotovoltaico = ag.capex_hetnet['Fotovoltaico']
                    opex_fotovoltaico = ag.opex_hetnet['Fotovoltaico']
                    custo_energia_minima_rede_h = ag.custo_energia_minima_rede['hetnet']
                    custo_aluguel_fotovoltaico_h = ag.custo_aluguel_fotovoltaico['hetnet']

                legenda.append('CAPEX {}'.format('Hetnet'))
                legenda.append('OPEX {}'.format('Hetnet'))

                capex_h += capex_radio['infraestrutura']
                capex_h += capex_radio['equipamentos']
                capex_h += capex_radio['instalacao']

                capex_h += capex_transporte['infraestrutura']
                capex_h += capex_transporte['equipamentos']
                capex_h += capex_transporte['instalacao']

                if sistema_fotovoltaico:
                    capex_h += list(capex_fotovoltaico.values())
                    opex_h += sum(custo_energia_minima_rede_h)
                    opex_h += sum(custo_aluguel_fotovoltaico_h)
                else:
                    opex_h += opex_transporte['energia']
                    opex_h += opex_radio['energia']

                opex_h += opex_radio['manutencao']
                opex_h += opex_radio['aluguel']
                opex_h += opex_radio['falhas']

                opex_h += opex_transporte['manutencao']
                opex_h += opex_transporte['aluguel']
                opex_h += opex_transporte['falhas']

                if sistema_fotovoltaico:
                    opex_fv_h = Util().subtracao_lista(list(opex_fotovoltaico.values()), custo_energia_minima_rede_h)
                    opex_h += Util().subtracao_lista(opex_fv_h, custo_aluguel_fotovoltaico_h)

            # central office
            capex_m += cenarios[key].capex_co['infraestrutura']
            capex_m += cenarios[key].capex_co['equipamentos']
            capex_m += cenarios[key].capex_co['instalacao']

            if sistema_fotovoltaico:
                capex_m += list(cenarios[key].capex_fotovoltaico_co.values())
                capex_h += list(cenarios[key].capex_fotovoltaico_co.values())
                opex_m += cenarios[key].custo_energia_minima_rede_co
                opex_fv_co = Util().subtracao_lista(list(cenarios[key].opex_fotovoltaico_co.values()),
                                    cenarios[key].custo_energia_minima_rede_co)
                opex_m += opex_fv_co
                opex_h += opex_fv_co
            else:
                opex_m += cenarios[key].opex_co['energia']
                opex_h += cenarios[key].opex_co['energia']

            opex_m += cenarios[key].opex_co['manutencao']
            opex_m += cenarios[key].opex_co['aluguel']
            opex_m += cenarios[key].opex_co['falhas']

            capex_h += cenarios[key].capex_co['infraestrutura']
            capex_h += cenarios[key].capex_co['equipamentos']
            capex_h += cenarios[key].capex_co['instalacao']

            opex_h += cenarios[key].opex_co['manutencao']
            opex_h += cenarios[key].opex_co['aluguel']
            opex_h += cenarios[key].opex_co['falhas']

            # 2806
            if sistema_fotovoltaico:
                income = cenarios[key].npv_sf[key].income
            else:
                income = cenarios[key].npv[key].income

            legenda.append('Income')
            plt.plot(posicao_rotulos, income, '-.', color='g', zorder=4, label='Income')

            plt.bar(posicao_macro, capex_m, color=cor_capex_m, width=bar_width, zorder=3, linewidth=line_width,
                    edgecolor='black',
                    label='CAPEX Macro')
            plt.bar(posicao_macro, opex_m, bottom=capex_m, color=cor_opex_m, width=bar_width, zorder=3,
                    linewidth=line_width,
                    edgecolor='black',
                    label='OPEX Macro')
            plt.bar(posicao_hetnet, capex_h, color=cor_capex_h, width=bar_width, zorder=3, linewidth=line_width,
                    edgecolor='black',
                    label='CAPEX HetNet')
            plt.bar(posicao_hetnet, opex_h, bottom=capex_h, color=cor_opex_h, width=bar_width, zorder=3,
                    linewidth=line_width,
                    edgecolor='black',
                    label='OPEX HetNet')

            if sistema_fotovoltaico:
                id_municipio = str(id_municipio) + '_' + str(key) + '_com_SF'
            else:
                id_municipio = str(id_municipio) + '_' + str(key) + '_sem_SF'

            # Custom X axis
            plt.xticks(posicao_rotulos, rotulos)
            plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
            # plt.legend(legenda, loc='best')
            plt.legend(loc='best')
            plt.ylabel('TCO - Monetary Units ($)')
            plt.xlabel('Units of Time (t)')
            plt.ylim(0, 5.0)

            plt.savefig('{}TCO-RT-Evolucao-{}-{}.eps'.format(path_imagem, '', id_municipio),
                        dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
            plt.close()

    # gráfico que mostra a escala Y em 100%
    def composicao_tco100_municipio(self, cenarios, tipo_grafico, tempo_analise, path_imagem,
                                                sistema_fotovoltaico):
        global id_municipio

        plt.figure(figsize=(9.0, 5.5))

        infraestrutura = list()
        equipamentos = list()
        instalacao = list()
        energia = list()
        manutencao = list()
        aluguel = list()
        falhas = list()
        rotulos = list()

        # Legendas e Largura das Barras
        line_width = 0.5
        bar_width = 1.5

        legenda = ['Infra.', 'Equip.', 'Instl.', 'En.', 'Mnt.', 'FS&SL', 'FM']
        label = 'Rede'
        for key in cenarios:
            capex_infra_m = 0.0
            capex_infra_h = 0.0
            capex_eq_m = 0.0
            capex_eq_h = 0.0
            capex_inst_m = 0.0
            capex_inst_h = 0.0
            opex_energia_m = 0.0
            opex_energia_h = 0.0
            opex_manut_m = 0.0
            opex_manut_h = 0.0
            opex_al_m = 0.0
            opex_al_h = 0.0
            opex_fl_m = 0.0
            opex_fl_h = 0.0

            for ag in cenarios[key].agrupamentos_territoriais:
                id_municipio = ag.municipio

                capex_macro_r = ag.capex_macro['Radio']
                capex_hetnet_r = ag.capex_hetnet['Radio']
                opex_macro_r = ag.opex_macro['Radio']
                opex_hetnet_r = ag.opex_hetnet['Radio']

                capex_macro_t = ag.capex_macro['Transporte']
                capex_hetnet_t = ag.capex_hetnet['Transporte']
                opex_macro_t = ag.opex_macro['Transporte']
                opex_hetnet_t = ag.opex_hetnet['Transporte']

                if sistema_fotovoltaico:
                    capex_macro_f = ag.capex_macro['Fotovoltaico']
                    capex_hetnet_f = ag.capex_hetnet['Fotovoltaico']
                    opex_macro_f = ag.opex_macro['Fotovoltaico']
                    opex_hetnet_f = ag.opex_hetnet['Fotovoltaico']
                    custo_energia_minima_rede_m = sum(ag.custo_energia_minima_rede['macro'])
                    custo_energia_minima_rede_h = sum(ag.custo_energia_minima_rede['hetnet'])
                    custo_aluguel_fotovoltaico_m = sum(ag.custo_aluguel_fotovoltaico['macro'])
                    custo_aluguel_fotovoltaico_h = sum(ag.custo_aluguel_fotovoltaico['hetnet'])

                capex_infra_m += capex_macro_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()
                capex_infra_h += capex_hetnet_r['infraestrutura'].sum() + capex_macro_t['infraestrutura'].sum()

                capex_eq_m += capex_macro_r['equipamentos'].sum() + capex_macro_t['equipamentos'].sum()
                capex_eq_h += capex_hetnet_r['equipamentos'].sum() + capex_hetnet_t['equipamentos'].sum()
                if sistema_fotovoltaico:
                    capex_eq_m += sum(list(capex_macro_f.values()))
                    capex_eq_h += sum(list(capex_hetnet_f.values()))

                capex_inst_m += capex_macro_r['instalacao'].sum() + capex_macro_t['instalacao'].sum()
                capex_inst_h += capex_hetnet_r['instalacao'].sum() + capex_hetnet_t['instalacao'].sum()

                if sistema_fotovoltaico:
                    opex_energia_m += custo_energia_minima_rede_m
                    opex_energia_h += custo_energia_minima_rede_h
                else:
                    opex_energia_m += opex_macro_r['energia'].sum() + opex_macro_t['energia'].sum()
                    opex_energia_h += opex_hetnet_r['energia'].sum() + opex_hetnet_t['energia'].sum()

                opex_manut_m += opex_macro_r['manutencao'].sum() + opex_macro_t['manutencao'].sum()
                opex_manut_h += opex_hetnet_r['manutencao'].sum() + opex_hetnet_t['manutencao'].sum()

                if sistema_fotovoltaico:
                    opex_fv_m = sum(
                        list(opex_macro_f.values())) - custo_energia_minima_rede_m - custo_aluguel_fotovoltaico_m
                    opex_fv_h = sum(
                        list(opex_hetnet_f.values())) - custo_energia_minima_rede_h - custo_aluguel_fotovoltaico_h
                    opex_manut_m += opex_fv_m
                    opex_manut_h += opex_fv_h
                    # aluguel
                    opex_al_m += custo_aluguel_fotovoltaico_m
                    opex_al_h += custo_aluguel_fotovoltaico_h

                opex_al_m += opex_macro_r['aluguel'].sum() + opex_macro_t['aluguel'].sum()
                opex_al_h += opex_hetnet_r['aluguel'].sum() + opex_hetnet_t['aluguel'].sum()

                opex_fl_m += opex_macro_r['falhas'].sum() + opex_macro_t['falhas'].sum()
                opex_fl_h += opex_hetnet_r['falhas'].sum() + opex_hetnet_t['falhas'].sum()

            # central office
            capex_infra_m += cenarios[key].capex_co['infraestrutura'].sum()
            capex_eq_m += cenarios[key].capex_co['equipamentos'].sum()
            capex_inst_m += cenarios[key].capex_co['instalacao'].sum()

            if sistema_fotovoltaico:
                opex_energia_m += sum(cenarios[key].custo_energia_minima_rede_co)
                opex_energia_h += sum(cenarios[key].custo_energia_minima_rede_co)
            else:
                opex_energia_m += cenarios[key].opex_co['energia'].sum()
                opex_energia_h += cenarios[key].opex_co['energia'].sum()

            opex_manut_m += cenarios[key].opex_co['manutencao'].sum()
            opex_al_m += cenarios[key].opex_co['aluguel'].sum()
            opex_fl_m += cenarios[key].opex_co['falhas'].sum()

            capex_infra_h += cenarios[key].capex_co['infraestrutura'].sum()
            capex_eq_h += cenarios[key].capex_co['equipamentos'].sum()

            if sistema_fotovoltaico:
                capex_eq_m += sum(list(cenarios[key].capex_fotovoltaico_co.values()))
                capex_eq_h += sum(list(cenarios[key].capex_fotovoltaico_co.values()))

            capex_inst_h += cenarios[key].capex_co['instalacao'].sum()

            if sistema_fotovoltaico:
                opex_fv_co = sum(
                    list(cenarios[key].opex_fotovoltaico_co.values())) - sum(cenarios[key].custo_energia_minima_rede_co) \
                            - sum(cenarios[key].custo_aluguel_fotovoltaico_co)

                opex_manut_m += opex_fv_co
                opex_manut_h += opex_fv_co
                # aluguel
                opex_al_m += sum(cenarios[key].custo_aluguel_fotovoltaico_co)
                opex_al_h += sum(cenarios[key].custo_aluguel_fotovoltaico_co)

            opex_manut_h += cenarios[key].opex_co['manutencao'].sum()
            opex_al_h += cenarios[key].opex_co['aluguel'].sum()
            opex_fl_h += cenarios[key].opex_co['falhas'].sum()

            infraestrutura.append(capex_infra_m)
            infraestrutura.append(capex_infra_h)

            equipamentos.append(capex_eq_m)
            equipamentos.append(capex_eq_h)

            instalacao.append(capex_inst_m)
            instalacao.append(capex_inst_h)

            energia.append(opex_energia_m)
            energia.append(opex_energia_h)

            manutencao.append(opex_manut_m)
            manutencao.append(opex_manut_h)

            aluguel.append(opex_al_m)
            aluguel.append(opex_al_h)

            falhas.append(opex_fl_m)
            falhas.append(opex_fl_h)

            rotulos.append('Macro')
            rotulos.append('\n{}'.format(key))
            rotulos.append('Hetnet')

            if sistema_fotovoltaico:
                id_municipio = str(id_municipio) + '_com_SF'
            else:
                id_municipio = str(id_municipio) + '_sem_SF'

            for index in range(len(infraestrutura)):
                soma = infraestrutura[index] + equipamentos[index] + instalacao[index] + energia[index] + \
                       manutencao[index] + aluguel[index] + falhas[index]
                infraestrutura[index] = (infraestrutura[index] / soma) * 100
                equipamentos[index] = (equipamentos[index] / soma) * 100
                instalacao[index] = (instalacao[index] / soma) * 100
                energia[index] = (energia[index] / soma) * 100
                manutencao[index] = (manutencao[index] / soma) * 100
                aluguel[index] = (aluguel[index] / soma) * 100
                falhas[index] = (falhas[index] / soma) * 100

        # Posicao das Barras
        posicao = np.array([0.0, 1.8, 4.3, 6.1, 8.6, 10.4, 12.9, 14.7])
        posicao_rotulo = np.array([0.0, 0.9, 1.8, 4.3, 5.2, 6.1, 8.6, 9.5, 10.4, 12.9, 13.8, 14.7])


        plt.bar(posicao, infraestrutura, color='#4f82bd', edgecolor='black', width=bar_width, zorder=3,
                linewidth=line_width)

        plt.bar(posicao, equipamentos, bottom=infraestrutura, color='#cf4d4f', edgecolor='black', width=bar_width,
                zorder=3, linewidth=line_width)

        plt.bar(posicao, instalacao, bottom=[i + j for i, j in zip(infraestrutura, equipamentos)], color='#88a54f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, energia, bottom=[i + j + k for i, j, k in zip(infraestrutura, equipamentos, instalacao)],
                color='#72578f',
                edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, manutencao,
                bottom=[i + j + k + l for i, j, k, l in zip(infraestrutura, equipamentos, instalacao, energia)],
                color='#4298ae', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        plt.bar(posicao, aluguel, bottom=[i + j + k + l + m for i, j, k, l, m in
                                          zip(infraestrutura, equipamentos, instalacao, energia, manutencao)],
                color='#da8436', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)
        plt.bar(posicao, falhas, bottom=[i + j + k + l + m + n for i, j, k, l, m, n in
                                         zip(infraestrutura, equipamentos, instalacao, energia, manutencao,
                                             aluguel)],
                color='#93a9cf', edgecolor='black', width=bar_width, zorder=3, linewidth=line_width)

        # Custom X an Y axis
        plt.xticks(posicao_rotulo, rotulos)
        plt.grid(linestyle='-', linewidth=1, zorder=0, axis='y', color='#C0C0C0')
        plt.legend(legenda, loc='upper center', bbox_to_anchor=(0.5, 1.09), fancybox=True, shadow=True, ncol=7)
        plt.ylabel('TCO (%)')

        plt.savefig('{}TCO-{}-Composicao100-{}.eps'.format(path_imagem, tipo_grafico, id_municipio),
                    dpi=Path.RESOLUCAO_IMAGEM.valor, bbox_inches='tight')
        plt.close()