from shapely.geometry import Polygon, Point, LineString
import ast
import re
import math
import numpy as np
import random

from entidade.ponto import Ponto
#from cenario_geografico.grafo import Grafo
from sistema_fotovoltaico.inversor import Inversor
from sistema_fotovoltaico.painel import Painel
from sistema_fotovoltaico.radiacao import Radiacao


class Util:

    def gera_poligono(self, geometry, tipo_area_geografica):
        pontos_coordenadas = self.prepara_coordenadas(geometry, tipo_area_geografica)
        poligono = Polygon(pontos_coordenadas)
        return poligono

    def gera_ponto(self, geometry):#Passo a coluna geometry, mas ela está em String
        valor_temp = re.sub('Point ', '[', geometry)#Formato transformando em lista
        valor_temp = re.sub("\)", ')]', valor_temp)#Formato transformando em lista
        pontos_coordenadas = ast.literal_eval(valor_temp)#Preparo para o uso do método Point
        ponto = Point(pontos_coordenadas)#O método Point/Polygon só trabalha com um a lista de pontos
        return ponto

    def calcula_area_km(self, coordenadas):
        m_km = 10 ** 6
        area_km = coordenadas.area / m_km
        return area_km

    def prepara_coordenadas(self, geometry, tipo_area_geografica):
        valor_temp = re.sub(tipo_area_geografica+"\(", "[", geometry)
        valor_temp = re.sub("\)\)", ")]", valor_temp)
        pontos_coordenadas = ast.literal_eval(valor_temp)#Faz a conversão de string para objeto
        return pontos_coordenadas

    def distancia(self,origem, destino):
        lat1, lon1 = origem
        lat2, lon2 = destino
        radius = 6371  # km

        dlat = math.radians(lat2 - lat1)
        dlon = math.radians(lon2 - lon1)

        a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(math.radians(lat1)) \
            * math.cos(math.radians(lat2)) * math.sin(dlon / 2) * math.sin(dlon / 2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = radius * c

        return d

    def gera_linha(self, pontos_coordenadas):
        ponto = LineString(pontos_coordenadas)
        return ponto


    def gera_distancia_pontos(self, coordenadas):
        ponto_temp = 0
        distancia = 0
       # d = 0
        for ponto in coordenadas:
            if ponto_temp != 0:
              # distancia antiga
              #  distancia = distancia + ponto.distance(ponto_temp)
                origem = (ponto.x, ponto.y)
                destino =(ponto_temp.x, ponto_temp.y)
                distancia += self.distancia(origem, destino)

            ponto_temp = ponto
        # print('Distancia Ponto')
        # print(distancia)

        return distancia

    def ponderacao_custo_distancia(self, distancia, percurso, infra):
        # w=d*2^r*2^s
        w = distancia * (2 ** percurso) * (2 ** infra)
        return w

    def get_gompertz(self, mu, beta, gamma, total_time):
        h = np.zeros(total_time)
        for t in range(total_time):
            temp = -beta * np.exp(-gamma * t)
            h[t] = mu * np.exp(temp)
        return h

    def densidade_demografica_total(self, numero_habitantes, area_km):
        return numero_habitantes/area_km

    def populacao_economicamente_ativa(self, numero_habitantes, porcentagem):
        return numero_habitantes * porcentagem

    def get_total_bs(self,lista_bs):
        total_macro = 0.0
        total_femto = 0.0
        for bs in lista_bs:
            if bs.tipo_BS.tipo == 'Macro':
                total_macro += 1
            else:
                total_femto += 1

    def get_distancia_euclidiana(self, bs_a, bs_b):
        pass

    def get_distancia_manhattan(self, bs_a, bs_b, obj, g):
        distancia, caminho = g.calcula_distancia_pontos(g.gera_arestas(obj), bs_a.id_rua, bs_b.id_rua)
        return distancia

    def get_ponto_aleatorio(self):
        x = random.uniform(200.0, 500.0)
        y = random.uniform(200.0, 500.0)
        return Ponto(x, y)

    def busca_bs_nao_hub(self, lista_bs, ano):
        result = list()
        for bs in lista_bs:
            if bs.hub_bs is False and bs.ano == ano:
                result.append(bs)
        return result

    def busca_bs_hub(self, lista_bs):
        b = None
        for bs in lista_bs:
            if bs.hub_bs is True:
                b = bs
        return b

    def convert_kWh(self, potencia):
        return (potencia * 24)/1000

    def atualizacao_linear(self, preco_inicial, taxa, periodo):
        preco_atualizado = np.zeros(periodo)
        for ano in range(len(preco_atualizado)):
            if ano == 0:
                preco_atualizado[ano] = preco_inicial
            else:
                preco_atualizado[ano] = preco_inicial + (taxa * preco_atualizado[ano - 1])
        return preco_atualizado

    def get_cenarios_alternativos(self, municipios):
        chave_primeiro = list(municipios)[0]
        total_aglomerados = len(municipios[chave_primeiro].agrupamentos_territoriais)
        cenarios = list()
        for i in range(total_aglomerados):
            cenarios.append(dict())
        for i in range(total_aglomerados):
            for key in municipios:
                cenarios[i][key] = municipios[key].agrupamentos_territoriais[i]
        return cenarios

    def subtracao_lista(self, valor_1, valor_2):
      return list(np.array(valor_1) - np.array(valor_2))
